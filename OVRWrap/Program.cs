﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System;

namespace Penrose.OVRWrap
{
    public class MainForm : GameWindow
    {
        Session session;

        EyeRenderDesc[] eyeRenderDesc;
        TextureSwapChain[] textureChain;
        int[] depthBuffer;
        int[] frameBuffer;

        Int64 frameIndex;

        public MainForm(DisplayDevice device)
            : base(1080, 600, GraphicsMode.Default, "OVR Wrap Test", GameWindowFlags.Default, device)
        {
            session = OVR.CreateSession();
            HmdDesc hmdDesc = OVR.GetHmdDesc(session);

            Width = hmdDesc.Resolution.W / 2;
            Height = hmdDesc.Resolution.H / 2;

            eyeRenderDesc = new EyeRenderDesc[2];
            eyeRenderDesc[0] = OVR.GetRenderDesc(session, EyeType.Left, hmdDesc.DefaultEyeFov[0]);
            eyeRenderDesc[1] = OVR.GetRenderDesc(session, EyeType.Right, hmdDesc.DefaultEyeFov[1]);

            textureChain = new TextureSwapChain[2];
            textureChain[0] = OVR.CreateTextureSwapChain(session);
            textureChain[1] = OVR.CreateTextureSwapChain(session);

            depthBuffer = new int[2];
            for (int eye = 0; eye < 2; eye++)
            {
                int tex = GL.GenTexture();

                GL.BindTexture(TextureTarget.Texture2D, tex);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
                
                SizeI size = OVR.GetFovTextureSize(session, eye, hmdDesc.DefaultEyeFov[eye]);
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.DepthComponent24,
                   size.W, size.H, 0, PixelFormat.DepthComponent, PixelType.UnsignedInt, IntPtr.Zero);

                depthBuffer[eye] = tex;

                GL.BindTexture(TextureTarget.Texture2D, 0);
            }

            frameBuffer = new int[2];
            frameBuffer[0] = GL.GenFramebuffer();
            frameBuffer[1] = GL.GenFramebuffer();
        }

        protected override void OnLoad(EventArgs e)
        {
            Console.WriteLine(OVR.GetHmdDesc(session));
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            double sampleTime;

            HmdDesc hmdDesc = OVR.GetHmdDesc(session);
            Posef[] eyeRenderPose = OVR.GetEyePoses(session, frameIndex, eyeRenderDesc, out sampleTime);

            for (int eye = 0; eye < 2; eye++)
            {
                int curIndex = OVR.GetTextureSwapChainCurrentIndex(session, textureChain[eye]);
                uint curTex = OVR.GetTextureSwapChainBufferGL(session, textureChain[eye], curIndex);

                GL.BindFramebuffer(FramebufferTarget.Framebuffer, frameBuffer[eye]);
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, curTex, 0);
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2D, depthBuffer[eye], 0);

                GL.Viewport(0, 0, Width, Height);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
                GL.ClearColor(0.0f, 0.0f, 0.0f, 1.0f);

                Matrix4 finalRollPitchYaw = Matrix4.CreateFromQuaternion(eyeRenderPose[eye].Orientation);
                Vector3 finalUp = Vector3.TransformPerspective(Vector3.UnitY, finalRollPitchYaw);
                Vector3 finalForward = Vector3.TransformPerspective(-Vector3.UnitZ, finalRollPitchYaw);
                Vector3 shiftedEyePos = eyeRenderPose[eye].Position;
                
                Matrix4 view = Matrix4.LookAt(shiftedEyePos, shiftedEyePos + finalForward, finalUp);
                Matrix4 proj = Matrix4.CreateOrthographic(10, 10, 0.3f, 100f);
                
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, 0, 0);
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2D, 0, 0);

                OVR.CommitTextureSwapChain(session, textureChain[eye]);
            }

            LayerEyeFov ld = new LayerEyeFov();
            ld.ColorTexture = new TextureSwapChain[2];
            ld.Viewport = new RectI[2];
            ld.Fov = new FovPort[2];
            ld.RenderPose = new Posef[2];

            ld.Header.Type = LayerType.EyeFov;
            ld.Header.Flags = LayerFlags.TextureOriginAtBottomLeft;
            ld.SensorSampleTime = sampleTime;
            for (int eye = 0; eye < 2; eye++)
            {
                SizeI size = OVR.GetFovTextureSize(session, eye, hmdDesc.DefaultEyeFov[eye]);

                ld.ColorTexture[eye] = textureChain[eye];
                ld.Viewport[eye] = new RectI(0, 0, size.W, size.H / 2);
                ld.Fov[eye] = hmdDesc.DefaultEyeFov[eye];
                ld.RenderPose[eye] = eyeRenderPose[eye];
            }

            OVR.SubmitFrame(session, frameIndex, ld);
            frameIndex++;
        }
    }
    public static class Program
    {
        static void Main(string[] args)
        {
            OVR.Initialize();

            MainForm f = new MainForm(DisplayDevice.Default);
            f.Run();
        }
    }
}
