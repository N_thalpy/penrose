﻿using System;
using System.Runtime.InteropServices;

namespace Penrose.OVRWrap
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Session
    {
        public IntPtr HmdStruct;
    }
}
