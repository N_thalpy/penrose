﻿using System;
using System.Runtime.InteropServices;

namespace Penrose.OVRWrap
{
    [StructLayout(LayoutKind.Sequential)]
    public struct TextureSwapChain
    {
        public IntPtr TextureSwapChainData;
    }
}
