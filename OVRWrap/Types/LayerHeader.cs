﻿using System;
using System.Runtime.InteropServices;

namespace Penrose.OVRWrap
{
    public enum LayerType
    {
        Disabled = 0,
        EyeFov = 1,
        Quad = 3,
        EyeMatrix = 5,
        EnumSize = 0x7FFFFFFF,
    }
    [Flags]
    public enum LayerFlags
    {
        HighQuality = 0x01,
        TextureOriginAtBottomLeft = 0x02,
        HeadLocked = 0x04,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct LayerHeader
    {
        public LayerType Type;
        public LayerFlags Flags;
    }
}
