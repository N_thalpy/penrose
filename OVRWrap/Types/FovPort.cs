﻿using System.Runtime.InteropServices;

namespace Penrose.OVRWrap
{
    [StructLayout(LayoutKind.Sequential)]
    public struct FovPort
    {
        public float UpTan;
        public float DownTan;
        public float LeftTan;
        public float RightTan;
    }
}
