﻿using System.Runtime.InteropServices;

namespace Penrose.OVRWrap
{
    [StructLayout(LayoutKind.Sequential)]
    public struct SizeI
    {
        public int W;
        public int H;
    }
}
