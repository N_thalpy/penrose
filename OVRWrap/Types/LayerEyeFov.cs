﻿using System.Runtime.InteropServices;

namespace Penrose.OVRWrap
{
    [StructLayout(LayoutKind.Sequential)]
    public struct LayerEyeFov
    {
        public LayerHeader Header;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public TextureSwapChain[] ColorTexture;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public RectI[] Viewport;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public FovPort[] Fov;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public Posef[] RenderPose;

        public double SensorSampleTime;
    }
}
