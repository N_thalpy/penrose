﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System;
using System.Collections.Generic;

namespace BezierHelper
{
    class MainForm : GameWindow
    {
        public List<float> Vertices;

        public Vector2 Min;
        public Vector2 Max;

        public float WindowWidth
        {
            get
            {
                return Max.X - Min.X;
            }
        }
        public float WindowHeight
        {
            get
            {
                return Max.Y - Min.Y;
            }
        }

        public int Click;

        private float sx
        {
            get
            {
                return 25f / Width;
            }
        }
        private float sy
        {
            get
            {
                return 25f / Height;
            }
        }

        public MainForm()
            : base(1000, 1000)
        {

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Vertices = new List<float>()
            {
                0f, 0.2f, 0.3f, 0.4f, 1f
            };

            Min = new Vector2(-0.2f, -1.2f);
            Max = new Vector2(1.2f, 2.2f);

            Click = -1;
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);
            switch (e.KeyChar)
            {
                case 'p':
                    Console.WriteLine("Curve Setting:");
                    for (int i = 0; i < Vertices.Count; i++)
                        Console.WriteLine("Vertex {0}: {1}", i + 1, Vertices[i]);
                    break;


            }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            float x = 2 * (float)e.Position.X / Width - 1;
            float y = 2 * (float)(Height - e.Position.Y) / Height - 1;

            Click = -1;
            for (int i = 0; i < Vertices.Count; i++)
            {
                float vx = 2 * (0 - Min.X) / WindowWidth - 1;
                float vy = 2 * (Vertices[i] - Min.Y) / WindowHeight - 1;

                if (x - sx < vx && vx < x + sx &&
                    y - sx < vy && vy < y + sy)
                    Click = i;
            }
        }
        protected override void OnMouseMove(MouseMoveEventArgs e)
        {
            base.OnMouseMove(e);
            float x = 2 * (float)e.Position.X / Width - 1;
            float y = 2 * (float)(Height - e.Position.Y) / Height - 1;

            if (Click != -1)
            {
                Vertices[Click] = (y + 1) * WindowHeight / 2 + Min.Y;
            }
        }
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            Click = -1;
        }
        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            Click = -1;
        }
        
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.ClearColor(Color4.Black);

            // Draw Snaps
            GL.Begin(PrimitiveType.Lines);
            for (int x = (int)Min.X; x < Max.X; x++)
            {
                if (x == 0)
                    GL.Color3(1.0, 0.0, 0.0);
                else
                    GL.Color3(1.0, 1.0, 1.0);

                GL.Vertex2(2 * (x - Min.X) / WindowWidth - 1, -1);
                GL.Vertex2(2 * (x - Min.X) / WindowWidth - 1, 1);
            }
            for (int y = (int)Min.Y; y < Max.Y; y++)
            {
                if (y == 0)
                    GL.Color3(1.0, 0.0, 0.0);
                else
                    GL.Color3(1.0, 1.0, 1.0);

                GL.Vertex2(-1, 2 * (y - Min.Y) / WindowHeight - 1);
                GL.Vertex2(1, 2 * (y - Min.Y) / WindowHeight - 1);
            }
            GL.End();

            // Draw vertices
            GL.Begin(PrimitiveType.Quads);
            for (int i = 0; i < Vertices.Count; i++)
            {
                float x = 2 * (0 - Min.X) / WindowWidth - 1;
                float y = 2 * (Vertices[i] - Min.Y) / WindowHeight - 1;

                GL.Color3(1.0, 1.0 / (Vertices.Count - 1) * i, 0.0);
                GL.Vertex2(x + sx, y + sy);
                GL.Vertex2(x - sx, y + sy);
                GL.Vertex2(x - sx, y - sy);
                GL.Vertex2(x + sx, y - sy);
            }
            GL.End();

            int divide = 1000;

            GL.Begin(PrimitiveType.LineStrip);
            GL.Color3(0.0, 1.0, 1.0);
            for (int i = -divide / 10; i <= divide * 11 / 10; i++)
            {
                float t = (float)i / divide;

                float y = 0;
                for (int j = 0; j < Vertices.Count; j++)
                {
                    int n = Vertices.Count - 1;

                    double coeff = 1;
                    coeff *= Math.Pow(t, j);
                    coeff *= Math.Pow(1 - t, n - j);
                    coeff *= Factorial(n) / Factorial(j) / Factorial(n - j);

                    y += (float)coeff * Vertices[j];
                }

                float d = 0.01f;
                GL.Vertex2(2 * (t - Min.X) / WindowWidth - 1, 2 * (y - Min.Y) / WindowHeight - 1 + d);
                GL.Vertex2(2 * (t - Min.X) / WindowWidth - 1, 2 * (y - Min.Y) / WindowHeight - 1 - d);
            }
            GL.End();
            GL.Begin(PrimitiveType.LineStrip);
            GL.Color3(0.5, 1.0, 1.0);
            for (int i = -divide / 10; i <= divide * 11 / 10; i++)
            {
                float t = (float)i / divide;

                float y = 0;
                for (int j = 0; j < Vertices.Count; j++)
                {
                    int n = Vertices.Count - 1;

                    double coeff = 1;
                    coeff *= Math.Pow(t, j);
                    coeff *= Math.Pow(1 - t, n - j);
                    coeff *= Factorial(n) / Factorial(j) / Factorial(n - j);

                    y += (float)coeff * Vertices[j];
                }

                float d = 0.01f;
                GL.Vertex2(2 * (1 + t - Min.X) / WindowWidth - 1, 2 * (y - Min.Y) / WindowHeight - 1 + d);
                GL.Vertex2(2 * (1 + t - Min.X) / WindowWidth - 1, 2 * (y - Min.Y) / WindowHeight - 1 - d);
            }
            GL.End();

            SwapBuffers();
        }

        private double Factorial(int n)
        {
            double rv = 1;
            for (int i = 1; i < n; i++)
                rv *= i;
            return rv;
        }
    }
}
