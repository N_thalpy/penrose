#include "../LibOVR/Include/OVR_CAPI_GL.h"
#include "../LibOVR/Include/Extras/OVR_Math.h"

#include "../LibOVRKernel/Src/GL/CAPI_GLE.h"
#include "../LibOVRKernel/Src/GL/CAPI_GLE_GL.h"

#include <cassert>
#include <cstring>