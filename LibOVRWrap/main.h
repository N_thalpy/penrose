#pragma once

#include "main.cpp"

extern "C" {
    void __declspec(dllimport) OVRInitialize();

    void __declspec(dllexport) OVRInitialize()
    {
        ovrInitParams initParams = { ovrInit_RequestVersion, OVR_MINOR_VERSION, NULL, 0, 0 };
        ovrResult res = ovr_Initialize(&initParams);
        assert(OVR_SUCCESS(res));
    }
    void __declspec(dllexport) OVRCreateSession(char* sess)
    {
        ovrGraphicsLuid luid;
        ovrResult res = ovr_Create((ovrSession*)sess, &luid);

        ovr_SetTrackingOriginType(*(ovrSession*)sess, ovrTrackingOrigin_FloorLevel);

        assert(OVR_SUCCESS(res));
    }
    void __declspec(dllexport) OVRGetHmdDesc(char* desc, char* sess)
    {
        ovrHmdDesc hmdDesc = ovr_GetHmdDesc(*(ovrSession*)sess);
        memcpy(desc, &hmdDesc, sizeof(ovrHmdDesc));
    }
    void __declspec(dllexport) OVRGetSessionStatus(char* status, char* sess)
    {
        ovr_GetSessionStatus(*(ovrSession*)sess, (ovrSessionStatus*)status);
    }
    void __declspec(dllexport) OVRGetRenderDesc(char* eyeRenderDesc, char* sess, int eyeType, char* fov)
    {
        ovrEyeRenderDesc desc = ovr_GetRenderDesc(*(ovrSession*)sess, (ovrEyeType)eyeType, *(ovrFovPort*)fov);
        memcpy(eyeRenderDesc, &desc, sizeof(ovrEyeRenderDesc));
    }
    void __declspec(dllexport) OVRCreateTextureSwapChain(char* chain, char* sess, int eye)
    {
        wglSwapIntervalEXT(0);
        ovrHmdDesc hmdDesc = ovr_GetHmdDesc(*(ovrSession*)sess);
        ovrSizei idealTextureSize = ovr_GetFovTextureSize(*(ovrSession*)sess, ovrEyeType(eye), hmdDesc.DefaultEyeFov[eye], 1);

        ovrTextureSwapChain* castedChain = (ovrTextureSwapChain*)chain;
        ovrTextureSwapChainDesc desc = {};
        desc.Type = ovrTexture_2D;
        desc.ArraySize = 1;
        desc.Width = idealTextureSize.w;
        desc.Height = idealTextureSize.h;
        desc.MipLevels = 1;
        desc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
        desc.SampleCount = 1;
        desc.StaticImage = ovrFalse;

        ovrResult result = ovr_CreateTextureSwapChainGL(*(ovrSession*)sess, &desc, castedChain);
        assert(OVR_SUCCESS(result));

        int length = 0;
        ovr_GetTextureSwapChainLength(*(ovrSession*)sess, *castedChain, &length);

        for (int i = 0; i < length; ++i) {
            GLuint chainTexId;
            ovr_GetTextureSwapChainBufferGL(*(ovrSession*)sess, *castedChain, i, &chainTexId);
            glBindTexture(GL_TEXTURE_2D, chainTexId);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        }
    }
    void __declspec(dllexport) OVRSubmitFrame(char* sess, long long frameIndex, char* ld)
    {
        ovrLayerHeader* layers = &((ovrLayerEyeFov*)ld)->Header;
        ovrResult result = ovr_SubmitFrame(*(ovrSession*)sess, frameIndex, nullptr, &layers, 1);
        assert(OVR_SUCCESS(result));
    }
    void __declspec(dllexport) OVRGetFovTextureSize(char* rv, char* sess, int eye, char* fov, float pixelsPerDisplayPixel)
    {
        ovrSizei size = ovr_GetFovTextureSize(*(ovrSession*)sess, (ovrEyeType)eye, *(ovrFovPort*)fov, pixelsPerDisplayPixel);
        memcpy(rv, &size, sizeof(ovrSizei));
    }
    void __declspec(dllexport) OVRGetEyePoses(char* rv, char* sess, long long frameIndex, char* eyeRenderDesc, double* sensorSampleTime)
    {
        ovrVector3f hmdToEyeOffset[2] = {
            ((ovrEyeRenderDesc*)eyeRenderDesc)[0].HmdToEyeOffset,
            ((ovrEyeRenderDesc*)eyeRenderDesc)[1].HmdToEyeOffset
        };
        ovr_GetEyePoses(*(ovrSession*)sess, frameIndex, ovrTrue, hmdToEyeOffset, (ovrPosef*)rv, sensorSampleTime);
    }

    void __declspec(dllexport) OVRCommitTextureSwapChain(char* sess, char* chain)
    {
        ovr_CommitTextureSwapChain(*(ovrSession*)sess, *(ovrTextureSwapChain*)chain);
    }

    void __declspec(dllexport) OVRGetTextureSwapChainCurrentIndex(int* index, char* sess, char* chain)
    {
        ovr_GetTextureSwapChainCurrentIndex(*(ovrSession*)sess, *(ovrTextureSwapChain*)chain, index);
    }
    void __declspec(dllexport) OVRGetTextureSwapChainBufferGL(unsigned int* texId, char* sess, char* chain, int index)
    {
        ovr_GetTextureSwapChainBufferGL(*(ovrSession*)sess, *(ovrTextureSwapChain*)chain, index, texId);
    }

    void __declspec(dllexport) OVRCreateMirrorTextureGL(char* rv, char* sess, int width, int height)
    {
        ovrMirrorTextureDesc desc;
        memset(&desc, 0, sizeof(desc));
        desc.Width = width;
        desc.Height = height;
        desc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;

        ovrResult res = ovr_CreateMirrorTextureGL(*(ovrSession*)sess, &desc, (ovrMirrorTexture*)rv);
        assert(OVR_SUCCESS(res));
    }
    void __declspec(dllexport) OVRGetMirrorTextureBufferGL(char* rv, char* sess, char* mirrorTexture)
    {
        ovr_GetMirrorTextureBufferGL(*(ovrSession*)sess, *(ovrMirrorTexture*)mirrorTexture, (unsigned int*)rv);
    }
}
