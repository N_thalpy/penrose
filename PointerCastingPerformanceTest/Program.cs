﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace PointerCastingPerformanceTest
{
    public unsafe static class Program
    {
        private struct Vector3
        {
            public Single X, Y, Z;
        }
        private struct Color4
        {
            public Single A, R, G, B;
        }

        private struct VertexPositionColorNormal
        {
            public Vector3 Position;
            public Color4 Color;
            public Vector3 Normal;
        }

        private readonly static int sampleCount;
        private readonly static int byteBasedStride;
        private readonly static int floatBasedStride;

        private static Random rd;

        static Program()
        {
            Debug.Assert(Marshal.SizeOf<VertexPositionColorNormal>() % Marshal.SizeOf<Single>() == 0);

            sampleCount = 1024 * 512;
            byteBasedStride = Marshal.SizeOf<VertexPositionColorNormal>();
            floatBasedStride = byteBasedStride / Marshal.SizeOf<Single>();
        }

        private static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            Single* ptr= (Single*)Marshal.AllocHGlobal(sampleCount * byteBasedStride).ToPointer();
            
            sw.Restart();
            rd = new Random(25252);
            for (int idx = 0; idx < 16; idx++)
            {
                Test1(ref ptr);
                ptr -= sampleCount * floatBasedStride;
            }
            WriteLog(1, sw.ElapsedTicks);

            sw.Restart();
            rd = new Random(25252);
            for (int idx = 0; idx < 16; idx++)
            {
                Test2(ref ptr);
                ptr -= sampleCount * floatBasedStride;
            }
            WriteLog(2, sw.ElapsedTicks);

            sw.Restart();
            rd = new Random(25252);
            for (int idx = 0; idx < 16; idx++)
            {
                Test3(ref ptr);
                ptr -= sampleCount * floatBasedStride;
            }
            WriteLog(3, sw.ElapsedTicks);

            Marshal.FreeHGlobal(new IntPtr(ptr));
        }

        private static void CreateVertexPositionColorNormal(out VertexPositionColorNormal vpcn)
        {
            vpcn.Position.X = rd.Next(-128, 128);
            vpcn.Position.Y = rd.Next(-128, 128);
            vpcn.Position.Z = rd.Next(-128, 128);

            vpcn.Color.A = rd.Next(-128, 128);
            vpcn.Color.R = rd.Next(-128, 128);
            vpcn.Color.G = rd.Next(-128, 128);
            vpcn.Color.B = rd.Next(-128, 128);

            vpcn.Normal.X = rd.Next(-128, 128);
            vpcn.Normal.Y = rd.Next(-128, 128);
            vpcn.Normal.Z = rd.Next(-128, 128);
        }

        private static void Test1(ref Single* ptr)
        {
            // Make 2 pointer
            VertexPositionColorNormal* pVPCN = (VertexPositionColorNormal*)ptr;
            for (int idx = 0; idx < sampleCount; idx++)
                CreateVertexPositionColorNormal(out *(pVPCN++));

            ptr += sampleCount * floatBasedStride;
        }
        private static void Test2(ref Single* ptr)
        {
            // Use lots of cast
            VertexPositionColorNormal vpcn;

            for (int idx = 0; idx < sampleCount; idx++)
            {
                CreateVertexPositionColorNormal(out vpcn);
                *((VertexPositionColorNormal*)ptr) = vpcn;
                ptr += floatBasedStride;
            }
        }
        public static void Test3(ref Single* ptr)
        {
            // Use Marshal.StructureToPtr
            VertexPositionColorNormal vpcn;

            for (int idx = 0; idx < sampleCount; idx++)
            {
                CreateVertexPositionColorNormal(out vpcn);
                Marshal.StructureToPtr<VertexPositionColorNormal>(vpcn, new IntPtr(ptr), false);
                ptr += floatBasedStride;
            }
        }

        private static void WriteLog(int testNumber, Int64 elapsedTicks)
        {
            Console.WriteLine("Test {0} has Ended: Elapsed time: {1}", testNumber, (float)elapsedTicks / Stopwatch.Frequency);
        }
    }
}
