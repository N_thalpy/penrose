﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using Penrose.GameLoop;
using Penrose.GameObject.Projectile.Impl;
using Penrose.GameObject.Projectile.Renderer;
using Penrose.OVR;
using Penrose.Physics;
using Penrose.Pool;
using Penrose.Rendering;
using Penrose.Rendering.Effect;
using Penrose.Rendering.Effect.Emitter.Impl;
using Penrose.Rendering.Effect.Particle.Impl;
using Penrose.Rendering.Renderer;
using Penrose.Rendering.Shader;
using Penrose.Rendering.VertexType;
using Penrose.SystemComponent;
using Penrose.UI;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Penrose
{
    public sealed class GameSystem : GameWindow
    {
        public static GameSystem Instance
        {
            get;
            private set;
        }
        public static bool IsVR
        {
            get;
            private set;
        }
        public static int TargetFPS
        {
            get;
            private set;
        }
        public static float TargetDeltaTime
        {
            get;
            private set;
        }

        public static event Action InvokeOnExit;

        public static void Initialize(bool isVR)
        {
            GameSystem.IsVR = isVR;
            TargetFPS = IsVR == true ? 90 : 60;
            TargetDeltaTime = 1f / TargetFPS;

            if (isVR == true)
            {
                Oculus.Initialize();
                Instance = new GameSystem(1920, 1067, "Engine:: w/ VR");
            }
            else
                Instance = new GameSystem(1600, 900, "Engine:: wo/ VR");

            FastMath.Initialize();

            ThreadHelper.Initialize();
            GLHelper.Initialize();

            ColliderRenderer.Initialize();
            PhysicsManager.Initialize();
            PoolManager.PoolObjects<SphericalCollider>(1500);

            WavefrontObjectProvider.Initialize();
            ShaderProvider.Initialize();
            BitmapFontProvider.Initialize();
            TextureProvider.Initialize();

            RenderQueue.Initialize();

            TrailEffectRenderer.Initialize();
            ColliderRenderer.Initialize();
            TurretProjectileRenderer.Initialize();
            MissileProjectileRenderer.Initialize();
            GenericObjectFlatRenderer.Initialize();

            PoolManager.PoolObjects<TurretProjectile>(1000);
            PoolManager.PoolObjects<MissileProjectile>(1000);

            ParticleRenderer<VertexPositionColor>.Initialize();

            PoolManager.PoolObjects<TrailEffect>(1000);
            PoolManager.PoolObjects<SimpleTraceEmitter>(1000);
            PoolManager.PoolObjects<ShitCannonTraceEmitter>(1000);
            PoolManager.PoolObjects<LinearMovingColoredCube>(100000);
        }
        public static void Start()
        {
            if (GameSystem.IsVR == true)
                Instance.Run(0.0, 0.0);
            else
                Instance.Run(60.0, 60.0);
        }

        public static void RunOnRenderThread(Action a, String debugName)
        {
            if (GameSystem.Instance.CurrentLoop == null)
                a.Invoke();
            else
                GameSystem.Instance.CurrentLoop.ReserveRender(a, debugName);
        }

        public GameLoopBase CurrentLoop
        {
            get
            {
                if (gameLoopStack == null || gameLoopStack.Count == 0)
                    return null;
                return gameLoopStack.Peek();
            }
        }

        public Vector2 Viewport;

        private Session session;
        private EyeRenderDesc[] eyeRenderDesc;
        private TextureSwapChain[] textureChain;
        private int[] depthBuffer;
        private int[] frameBuffer;
        private long frameIndex;

        private MirrorTexture mirrorTexture;
        private int mirrorTextureHandle;
        private int mirrorFrameBuffer;

        private Stack<GameLoopBase> gameLoopStack;

        private GameTime renderGameTime;
        private GameTime updateGameTime;

        private bool lockMouse;
        private bool togglingLock;

        private GameSystem(int width, int height, String name)
            : base(width, height, new GraphicsMode(32, 24, 0, 16), name, GameWindowFlags.Default)
        {
            Viewport = new Vector2(width, height);
            gameLoopStack = new Stack<GameLoopBase>();

            renderGameTime = new GameTime();
            updateGameTime = new GameTime();

            lockMouse = true;

            if (GameSystem.IsVR == true)
            {
                session = Oculus.CreateSession();
                HmdDesc hmdDesc = Oculus.GetHmdDesc(session);

                eyeRenderDesc = new EyeRenderDesc[2];

                textureChain = new TextureSwapChain[2];
                textureChain[0] = Oculus.CreateTextureSwapChain(session, 0);
                textureChain[1] = Oculus.CreateTextureSwapChain(session, 1);

                depthBuffer = new int[2];
                for (int eye = 0; eye < 2; eye++)
                {
                    SizeI size = Oculus.GetFovTextureSize(session, eye, hmdDesc.DefaultEyeFov[eye]);
                    int tex = GL.GenTexture();

                    GL.ActiveTexture(TextureUnit.Texture0);
                    GL.BindTexture(TextureTarget.Texture2D, tex);

                    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
                    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
                    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
                    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

                    GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.DepthComponent24,
                        size.W, size.H, 0, PixelFormat.DepthComponent, PixelType.UnsignedInt, IntPtr.Zero);

                    depthBuffer[eye] = tex;

                    GL.BindTexture(TextureTarget.Texture2D, 0);
                }

                mirrorTexture = Oculus.OVRCreateMirrorTextureGL(session, Width, Height);
                mirrorTextureHandle = Oculus.OVRGetMirrorTextureBufferGL(session, mirrorTexture);

                mirrorFrameBuffer = GL.GenFramebuffer();
                GLHelper.CheckGLError();

                GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, mirrorFrameBuffer);
                GL.FramebufferTexture2D(FramebufferTarget.ReadFramebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, mirrorTextureHandle, 0);
                GL.FramebufferRenderbuffer(FramebufferTarget.ReadFramebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, 0);
                GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, 0);

                frameBuffer = new int[2];
                frameBuffer[0] = GL.GenFramebuffer();
                frameBuffer[1] = GL.GenFramebuffer();
            }
            else
            {
                GL.Viewport(0, 0, width, height);
            }

            GLHelper.CheckGLError();
        }

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                if (gameLoopStack.Count >= 2)
                    PopGameLoop();
            }
        }
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            TimeHelper.Tic("MainLoop.Total");

            float dt = renderGameTime.DeltaTime;
            renderGameTime.Refresh();

            if (CurrentLoop.IsInitialized == false)
                CurrentLoop.Initialize();

            TimeHelper.Tic("MainLoop.Update");
            CurrentLoop.Update(dt);
            TimeHelper.Toc("MainLoop.Update");

            InputHelper.Update();

            if (GameSystem.IsVR == true)
                VRRender(dt);
            else
                NonVRRender(dt);

            TimeHelper.Flush();

            if (Keyboard.GetState().IsKeyDown(Key.ControlLeft) == true)
            {
                if (togglingLock == false)
                    lockMouse ^= true;
                togglingLock = true;
            }
            else
                togglingLock = false;


            if (lockMouse == true)
                OpenTK.Input.Mouse.SetPosition(
                    GameSystem.Instance.Location.X + GameSystem.Instance.Width / 2,
                    GameSystem.Instance.Location.Y + GameSystem.Instance.Height / 2);
            GLHelper.CheckGLError();
        }

        private void VRRender(float deltaTime)
        {
            double sampleTime;
            HmdDesc hmdDesc = Oculus.GetHmdDesc(session);

            eyeRenderDesc[0] = Oculus.GetRenderDesc(session, EyeType.Left, hmdDesc.DefaultEyeFov[0]);
            eyeRenderDesc[1] = Oculus.GetRenderDesc(session, EyeType.Right, hmdDesc.DefaultEyeFov[1]);

            Posef[] eyeRenderPose = Oculus.GetEyePoses(session, frameIndex, eyeRenderDesc, out sampleTime);

            TimeHelper.Tic("MainLoop.Render");

            RenderQueue.Instance.Clear();
            CurrentLoop.Render(deltaTime);

            for (int eye = 0; eye < 2; eye++)
            {
                int curIndex = Oculus.GetTextureSwapChainCurrentIndex(session, textureChain[eye]);
                uint curTex = Oculus.GetTextureSwapChainBufferGL(session, textureChain[eye], curIndex);

                FrameBuffer.PushFrameBuffer();
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, frameBuffer[eye]);
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, curTex, 0);
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2D, depthBuffer[eye], 0);

                SizeI size = Oculus.GetFovTextureSize(session, eye, hmdDesc.DefaultEyeFov[eye]);
                GL.Viewport(0, 0, size.W, size.H);

                GL.ClearColor(GLHelper.BackgroundColor);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

                Matrix4 finalRollPitchYaw = Matrix4.CreateFromQuaternion(eyeRenderPose[eye].Orientation);
                Vector3 finalUp = Vector3.TransformPerspective(Vector3.UnitY, finalRollPitchYaw);
                Vector3 finalForward = Vector3.TransformPerspective(-Vector3.UnitZ, finalRollPitchYaw);
                Vector3 shiftedEyePos = eyeRenderPose[eye].Position;

                Vector3 z = -finalForward.Normalized();
                Vector3 x = Vector3.Cross(finalUp, z).Normalized();
                Vector3 y = Vector3.Cross(z, x);

                Matrix4 view = new Matrix4(
                    x.X, x.Y, x.Z, -Vector3.Dot(x, shiftedEyePos),
                    y.X, y.Y, y.Z, -Vector3.Dot(y, shiftedEyePos),
                    z.X, z.Y, z.Z, -Vector3.Dot(z, shiftedEyePos),
                    0, 0, 0, 1
                );
                view.Transpose();

                float l = -hmdDesc.DefaultEyeFov[eye].LeftTan;
                float r = hmdDesc.DefaultEyeFov[eye].RightTan;
                float t = hmdDesc.DefaultEyeFov[eye].UpTan;
                float b = -hmdDesc.DefaultEyeFov[eye].DownTan;
                float n = GLHelper.NearPlane;
                float f = GLHelper.FarPlane;

                Matrix4 proj = new Matrix4(
                    2 / (r - l), 0, (r + l) / (r - l), 0,
                    0, 2 / (t - b), (t + b) / (t - b), 0,
                    0, 0, -(f + n) / (f - n), -2 * f * n / (f - n),
                    0, 0, -1, 0
                );
                proj.Transpose();

                Matrix4 mat = view * proj;

                GLHelper.CurrentProjection = mat;
                RenderQueue.Instance.Flush();

                GLHelper.CheckGLError();

                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, 0, 0);
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2D, 0, 0);

                int fbo;
                Rectangle viewport;
                FrameBuffer.PopFrameBuffer(out fbo, out viewport);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo);
                GL.Viewport(viewport);

                GLHelper.CheckGLError();

                Oculus.CommitTextureSwapChain(session, textureChain[eye]);
            }
            TimeHelper.Toc("MainLoop.Render");

            LayerEyeFov ld = new LayerEyeFov()
            {
                ColorTexture = new TextureSwapChain[2],
                Viewport = new RectI[2],
                Fov = new FovPort[2],
                RenderPose = new Posef[2],
                Header = new LayerHeader()
                {
                    Type = LayerType.EyeFov,
                    Flags = LayerFlags.TextureOriginAtBottomLeft,
                },
                SensorSampleTime = sampleTime,
            };

            for (int eye = 0; eye < 2; eye++)
            {
                SizeI size = Oculus.GetFovTextureSize(session, eye, hmdDesc.DefaultEyeFov[eye]);

                ld.ColorTexture[eye] = textureChain[eye];
                ld.Viewport[eye] = new RectI(0, 0, size.W, size.H);
                ld.Fov[eye] = hmdDesc.DefaultEyeFov[eye];
                ld.RenderPose[eye] = eyeRenderPose[eye];
            }

            GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, mirrorFrameBuffer);
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, 0);
            GL.BlitFramebuffer(0, Height, Width, 0, 0, 0, Width, Height, ClearBufferMask.ColorBufferBit, BlitFramebufferFilter.Nearest);
            GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, 0);

            SwapBuffers();

            TimeHelper.Tic("MainLoop.Submit");
            Oculus.SubmitFrame(session, frameIndex, ld);
            TimeHelper.Toc("MainLoop.Submit");

            frameIndex++;
            TimeHelper.Toc("MainLoop.Total");

        }
        private void NonVRRender(float deltaTime)
        {
            GL.ClearColor(GLHelper.BackgroundColor);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            Matrix4 proj = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver4, (float)Width / Height, GLHelper.NearPlane, GLHelper.FarPlane);

            GLHelper.CurrentProjection = proj;
            RenderQueue.Instance.Clear();
            CurrentLoop.Render(deltaTime);
            RenderQueue.Instance.Flush();

            SwapBuffers();
        }

        public void PushGameLoop(GameLoopBase loop)
        {
            gameLoopStack.Push(loop);
        }
        public void PopGameLoop()
        {
            gameLoopStack.Peek().Dispose();
            gameLoopStack.Pop();
        }

        public override void Exit()
        {
            InvokeOnExit.Invoke();
            base.Exit();
        }
    }
}
