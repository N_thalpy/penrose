﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Penrose.Pool
{
    public class PoolManager
    {
        public static class Pool<T> where T : IPoolable, new()
        {
            public static Queue<T> Garbages;
            public static int TotalObjects;

            static Pool()
            {
                Garbages = new Queue<T>();
                GameSystem.InvokeOnExit += Dispose;
            }

            public static T Create()
            {
                if (Garbages.Count == 0)
                {
                    TotalObjects++;
                    Console.WriteLine("Object Pool Not sufficient!! {0}", typeof(T).FullName);
                    return new T();
                }
                else
                {
                    T rv = Garbages.Dequeue();
                    rv.Clear();
                    rv.Activate();
                    return rv;
                }
            }
            public static void Destroy(T target)
            {
                CheckSelfDestroy(target);

                target.Deactivate();
                Garbages.Enqueue(target);
            }

            public static void Dispose()
            {
                while (Garbages.Count != 0)
                {
                    T target = Garbages.Dequeue();
                    if (target is IDisposable)
                        (target as IDisposable).Dispose();
                }
            }

            [Conditional("DEBUG")]
            private static void CheckSelfDestroy(T target)
            {
                StackTrace tr = new StackTrace();
                foreach (StackFrame frame in tr.GetFrames())
                    Debug.Assert(frame.GetMethod().DeclaringType != typeof(T));
            }
        }

        public static T Create<T>() where T : IPoolable, new()
        {
            return Pool<T>.Create();
        }
        public static void Destroy<T>(T target) where T : IPoolable, new()
        {
            Pool<T>.Destroy(target);
        }

        public static void PoolObjects<T>(int count) where T : IPoolable, new()
        {
            Pool<T>.TotalObjects += count;
            for (int idx = 0; idx < count; idx++)
                Pool<T>.Garbages.Enqueue(new T());
        }
    }
}
