﻿namespace Penrose.Pool
{
    public interface IPoolable
    {
        void Clear();

        void Activate();
        void Deactivate();
    }
}
