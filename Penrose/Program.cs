﻿using OpenTK;
using Penrose.GameLoop.SystemGameLoop;
using Penrose.GameLoop.TestImpl;
using Penrose.UnitTest;
using System;
using System.Runtime;
using System.Threading;

namespace Penrose
{
    public static class Program
    {
        [STAThread]
        public static void Main()
        {
            UnitTestManager.InvokeUnitTest();

            Thread.BeginThreadAffinity();

            Toolkit.Init();
            GameSystem.Initialize(false);

            GCSettings.LatencyMode = GCLatencyMode.LowLatency;

            GameSystem.Instance.PushGameLoop(new ExitGameLoop());
            GameSystem.Instance.PushGameLoop(new NDMEngageLoop());

            GameSystem.Instance.VSync = VSyncMode.Off;
            
            GameSystem.Start();

            Thread.EndThreadAffinity();

            Environment.Exit(0);
        }
    }
}
