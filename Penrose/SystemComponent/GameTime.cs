﻿using System.Diagnostics;

namespace Penrose.SystemComponent
{
    public sealed class GameTime
    {
        public float DeltaTime
        {
            get;
            private set;
        }
        public long Frame
        {
            get;
            private set;
        }
        
        private Stopwatch sw;

        private long currentTick;
        private long oldTick;

        static GameTime()
        {
        }
        public GameTime()
        {
            sw = new Stopwatch();
            
            Reset();
            sw.Start();
        }

        public void Reset()
        {
            currentTick = sw.ElapsedTicks;
            oldTick = currentTick;
            Frame = 0;
        }
        public void Refresh()
        {
            oldTick = currentTick;
            currentTick = sw.ElapsedTicks;

            Debug.Assert(oldTick < currentTick);
            DeltaTime = (currentTick - oldTick) / (float)Stopwatch.Frequency;
            Frame++;
        }
    }
}
