﻿using OpenTK;
using OpenTK.Graphics;
using Penrose.AI;
using Penrose.AI.Impl;
using Penrose.GameObject.Equipment.Engine.Impl;
using Penrose.GameObject.Projectile;
using Penrose.Physics;
using Penrose.Pool;
using Penrose.Rendering;

namespace Penrose.GameObject.Ship.Impl
{
    public sealed class OrionNebula : ShipBase
    {
        // temp
        private int hitCount;
        private Trapzeium[] trapeziumList;

        public OrionNebula(AIBase shipAI)
            : base("M42 Orion Nebula", Team.Enemy1)
        {
            model = WavefrontObjectProvider.OrionNebula;
            modelOffset = new Vector3();
            color = Color4.Turquoise;

            PrimaryEngine = new SlowEngine();
            PrimaryEngine.Owner = this;

            Collider = PoolManager.Create<SphericalCollider>();
            Collider.Initialize(Transform, 48, this, OnCollision);

            trapeziumList = new Trapzeium[4];
            for (int idx = 0; idx < 4; idx++)
                trapeziumList[idx] = new Trapzeium(new SimpleTrapeziumAI(shipAI.Target));

            AI = shipAI;
            if (AI != null)
                AI.Owner = this;
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);
            CanDispose = (Alive == false);
        }

        public void OnCollision(SphericalCollider opponent)
        {
            if (AI != null)
            {
                if (opponent.Owner is ProjectileBase)
                {
                    ProjectileBase proj = opponent.Owner as ProjectileBase;
                    if (proj.Owner != this)
                        hitCount++;
                    if (hitCount > 20)
                    {
                        Alive = false;

                        if (Collider != null)
                            PoolManager.Destroy(Collider);
                        Collider = null;
                    }
                }
            }
        }

        public void LaunchTrapezium()
        {

        }

        public override void Dispose()
        {
            base.Dispose();
            if (Collider != null)
                PoolManager.Destroy(Collider);
        }
    }
}
