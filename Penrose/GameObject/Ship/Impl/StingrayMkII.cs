﻿using OpenTK;
using OpenTK.Graphics;
using Penrose.AI;
using Penrose.GameObject.Equipment.Engine.Impl;
using Penrose.GameObject.Equipment.Weapon.Impl;
using Penrose.GameObject.Projectile;
using Penrose.Physics;
using Penrose.Pool;
using Penrose.Rendering;

namespace Penrose.GameObject.Ship.Impl
{
    public sealed class StingrayMkII : ShipBase
    {
        // temp
        private int hitCount;

        public StingrayMkII(AIBase shipAI)
            : base("Stingray Hen 3-1357", Team.Enemy1)
        {
            model = WavefrontObjectProvider.Stingray;
            modelOffset = new Vector3(0, 0, -19.7f);
            color = Color4.Crimson;

            PrimaryEngine = new SimpleEngine();

            PrimaryWeapon = new ShitCannon();
            SecondaryWeapon = new MissileLauncher();

            PrimaryEngine.Owner = this;
            PrimaryWeapon.Owner = this;
            SecondaryWeapon.Owner = this;

            Collider = PoolManager.Create<SphericalCollider>();
            Collider.Initialize(Transform, 30, this, OnCollision);

            AI = shipAI;
            if (AI != null)
                AI.Owner = this;
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);
            CanDispose = (Alive == false) && (PrimaryWeapon.CanDispose == true);
        }

        public void OnCollision(SphericalCollider opponent)
        {
            if (AI != null)
            {
                if (opponent.Owner is ProjectileBase)
                {
                    ProjectileBase proj = opponent.Owner as ProjectileBase;
                    if (proj.Owner != this)
                        hitCount++;
                    if (hitCount > 4)
                    {
                        Alive = false;

                        if (Collider != null)
                            PoolManager.Destroy(Collider);
                        Collider = null;
                    }
                }
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            if (Collider != null)
                PoolManager.Destroy(Collider);
        }
    }
}
