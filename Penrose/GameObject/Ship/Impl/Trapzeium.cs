﻿using Penrose.AI;

namespace Penrose.GameObject.Ship.Impl
{
    public sealed class Trapzeium : ShipBase
    {
        public Trapzeium(AIBase ai)
            : base("Trapezium Cluster", Team.Enemy1)
        {

        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
