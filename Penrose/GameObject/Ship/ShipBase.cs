﻿using OpenTK;
using OpenTK.Graphics;
using Penrose.AI;
using Penrose.GameObject.Equipment.Engine;
using Penrose.GameObject.Equipment.Weapon;
using Penrose.Physics;
using Penrose.Rendering;
using Penrose.Rendering.Renderer;
using System;

namespace Penrose.GameObject.Ship
{
    public abstract class ShipBase : IDisposable
    {
        protected AIBase AI;

        protected WavefrontObject model;
        protected Vector3 modelOffset;
        protected Color4 color;

        public readonly String Name;
        public readonly Team ShipTeam;

        public Transform Transform;

        public EngineBase PrimaryEngine;
        public WeaponBase PrimaryWeapon;
        public WeaponBase SecondaryWeapon;

        public SphericalCollider Collider;

        public bool Alive;
        public bool CanDispose;

        public Vector3 CurrentWorldVelocity
        {
            get
            {
                return PrimaryEngine.CurrentWorldVelocity;
            }
        }

        protected ShipBase(String name, Team team)
        {
            Name = name;
            this.ShipTeam = team;
            Transform = new Transform();

            Alive = true;
        }

        public virtual void Update(float deltaTime)
        {
            if (AI == null)
            {
                PrimaryEngine.Update(
                    deltaTime,
                    InputHelper.IsAccelerationPushed(),
                    InputHelper.IsBreakPushed(),
                    InputHelper.GetLeftRight(),
                    InputHelper.GetTwist(),
                    InputHelper.GetForwardBackward());

                PrimaryWeapon?.Update(deltaTime, InputHelper.IsPrimaryFired());
                SecondaryWeapon?.Update(deltaTime, InputHelper.IsSecondaryFired());
            }
            else
            {
                AI.Update(deltaTime);
            }
        }
        public virtual void Render(Transform eyeTransform)
        {
            PrimaryWeapon?.Render(eyeTransform);
            SecondaryWeapon?.Render(eyeTransform);

            if (AI != null && Alive == true)
            {
                GenericObjectFlatRenderer.Instance.Enqueue(Transform, modelOffset, model, color);
                ColliderRenderer.Instance.Enqueue(Collider);
            }
        }

        public virtual void Dispose()
        {
            PrimaryWeapon?.Dispose();
            SecondaryWeapon?.Dispose();
        }
    }
}
