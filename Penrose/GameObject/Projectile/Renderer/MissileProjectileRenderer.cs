﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Penrose.GameObject.Projectile.Impl;
using Penrose.Rendering;
using Penrose.Rendering.Shader;
using Penrose.Rendering.Shader.ShaderVar;
using Penrose.Rendering.VertexType;
using Penrose.Unsafe;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Penrose.GameObject.Projectile.Renderer
{
    public sealed class MissileProjectileRenderer : IDisposable
    {
        public static MissileProjectileRenderer Instance;

        static MissileProjectileRenderer()
        {
        }
        public static void Initialize()
        {
            MissileProjectileRenderer.Instance = new MissileProjectileRenderer(100);
            GameSystem.InvokeOnExit += MissileProjectileRenderer.Instance.Dispose;
        }

        private List<MissileProjectile> projectileList;

        private UnmanagedArray<VertexPositionColor> vertexArray;
        private UnmanagedArray<Int32> elementArray;

        private WavefrontObject model;
        private VertexBuffer vbo;
        private ElementBuffer ebo;
        private ShaderBase shader;

        private unsafe MissileProjectileRenderer(int maxCount)
        {
            projectileList = new List<MissileProjectile>();

            model = WavefrontObjectProvider.Cube;
            shader = ShaderProvider.ColorShader;

            vertexArray = new UnmanagedArray<VertexPositionColor>(model.Vertices.Length * maxCount);
            vbo = VertexBuffer.Create<VertexPositionColor>(vertexArray.Capacity);
            vbo.Seal();

            elementArray = new UnmanagedArray<Int32>(model.MeshElement[0].Length * maxCount);
            ebo = ElementBuffer.Create(elementArray.Capacity);

            Int32* ep = (Int32*)elementArray.Pointer;
            for (int idx = 0; idx < maxCount; idx++)
                for (int jdx = 0; jdx < model.MeshElement[0].Length; jdx++)
                    *(ep++) = model.Vertices.Length * idx + model.MeshElement[0][jdx];

            ebo.Upload(elementArray.Pointer, 0, ebo.Count);
            ebo.Seal();
        }

        public void Clear()
        {
            projectileList.Clear();
        }
        public void Enqueue(MissileProjectile proj)
        {
            projectileList.Add(proj);
        }
        public void Enqueue(IEnumerable<MissileProjectile> projectileList)
        {
            foreach (MissileProjectile proj in projectileList)
                this.projectileList.Add(proj);
        }
        public unsafe void Flush(Transform eyeTransform)
        {
            vbo.UnSeal();

            vertexArray.CheckCapacity(model.Vertices.Length * projectileList.Count);
            vbo.Resize<VertexPositionColor>(vertexArray.Capacity);

            bool eboResized = elementArray.CheckCapacity(model.MeshElement[0].Length * projectileList.Count);
            if (eboResized == true)
            {
                ebo.UnSeal();
                ebo.Resize(elementArray.Capacity);

                Int32* ep = (Int32*)elementArray.Pointer;
                for (int idx = 0; idx < elementArray.Capacity / model.MeshElement[0].Length; idx++)
                    for (int jdx = 0; jdx < model.MeshElement[0].Length; jdx++)
                        *(ep++) = model.Vertices.Length * idx + model.MeshElement[0][jdx];

                ebo.Upload(elementArray.Pointer, 0, elementArray.Capacity);
                ebo.Seal();
            }

            int count = projectileList.Count();

            VertexPositionColor* vp = (VertexPositionColor*)vertexArray.Pointer;
            foreach (MissileProjectile tp in projectileList)
                for (int jdx = 0; jdx < model.Vertices.Length; jdx++)
                {
                    vp->Position = Vector3.TransformPerspective(model.Vertices[jdx].Position, tp.transform.TransformMatrix);
                    vp->Color = new Color4(255, 8, 125, 255);
                    vp++;
                }

            vbo.Upload<VertexPositionColor>(vertexArray.Pointer, 0, model.Vertices.Length * count);
            vbo.Seal();

            RenderQueue.Instance.Enqueue(new RenderQueue.OpaqueRenderJob()
            {
                VertexBuffer = vbo,
                ElementBuffer = ebo,
                Shader = shader,

                ElementCount = model.MeshElement[0].Length * projectileList.Count(),
                PrimitiveType = PrimitiveType.Triangles,

                VariableManager = new ShaderVariableManager()
                {
                    UniformTransform = GLHelper.CreateTransformMatrix(eyeTransform),
                }
            });
        }

        public void Dispose()
        {
            vertexArray.Dispose();
            vbo.Dispose();

            elementArray.Dispose();
            ebo.Dispose();
        }
    }
}
