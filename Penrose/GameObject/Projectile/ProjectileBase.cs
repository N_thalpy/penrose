﻿using Penrose.GameObject.Ship;

namespace Penrose.GameObject.Projectile
{
    public abstract class ProjectileBase
    {
        public ShipBase Owner;
        public Transform transform;

        protected ProjectileBase()
        {
            transform = new Transform();
        }

        public abstract void Update(float deltaTime);
    }
}
