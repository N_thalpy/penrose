﻿using OpenTK;
using OpenTK.Graphics;
using Penrose.GameObject.Projectile.Renderer;
using Penrose.GameObject.Ship;
using Penrose.Physics;
using Penrose.Pool;
using Penrose.Rendering.Effect;
using Penrose.Rendering.Effect.AnimationCurve;
using Penrose.Rendering.Effect.Emitter.Impl;
using Penrose.Rendering.Renderer;
using System;

namespace Penrose.GameObject.Projectile.Impl
{
    public sealed class MissileProjectile : ProjectileBase, IPoolable
    {
        public bool Active;
        private bool hit;

        private float speed;
        private Vector3 currentAngularVelocity;
        private float maxYaw;
        private float maxPitch;
        private float maxRoll;

        public SphericalCollider Collider;
        
        public ShitCannonTraceEmitter Particle;
        public TrailEffect Trail;

        public MissileProjectile()
            : base()
        {
            Active = true;
            hit = false;

            maxYaw = MathHelper.Pi / 6;
            maxPitch = MathHelper.Pi / 6;
            maxRoll = MathHelper.Pi / 6;
        }

        public override void Update(float deltaTime)
        {
            if (hit == true)
            {
                Trail.Update(deltaTime, false);
                Particle.Update(deltaTime, false);
            }
            else
            {
                Transform targetTransform = null;

                RaycastHit[] hits = PhysicsManager.Instance.CastSphere(transform.Position, 300);
                foreach (RaycastHit hit in hits)
                {
                    if (hit.Collider.Owner is ShipBase shipBase && shipBase.ShipTeam != this.Owner.ShipTeam)
                    {
                        targetTransform = hit.Collider.Transform;
                        break;
                    }
                }

                // Maybe need some more condition checks
                // e.g. Raycast to target and check other colliders

                Vector3 targetAngularVelocity = Vector3.Zero;
                if (targetTransform != null)
                {
                    // Do some tracking related stuffs
                    Matrix4 lookAt = Matrix4.LookAt(transform.Position, targetTransform.Position, Vector3.UnitY);
                    Quaternion targetQuat = transform.Orientation.Inverted() * lookAt.ExtractRotation().Inverted();
                    targetQuat.ToEulerAngle(out float pitch, out float yaw, out float roll);

                    targetAngularVelocity = new Vector3(Math.Sign(pitch) * maxPitch, Math.Sign(yaw) * maxYaw, Math.Sign(roll) * maxRoll);
                }
                currentAngularVelocity = Vector3.Lerp(currentAngularVelocity, targetAngularVelocity, 1.1f);
                transform.Orientation *= Quaternion.FromEulerAngles(currentAngularVelocity.X * deltaTime, currentAngularVelocity.Y * deltaTime, currentAngularVelocity.Z * deltaTime);                

                transform.Position += transform.Orientation * (new Vector3(0, 0, -1) * speed * deltaTime);
                Particle.Update(deltaTime, true);
                Trail.Update(deltaTime, true);
            }

            if (hit == true && Trail.ValidLogCount == 0)
                Active = false;
        }
        public void Render()
        {
            if (hit == false)
            {
                MissileProjectileRenderer.Instance.Enqueue(this);
                ColliderRenderer.Instance.Enqueue(Collider);
            }

            Particle.Render();
            Trail.Render();
        }

        public void Initialize(ShipBase owner, Vector3 initPos, Quaternion initRot, float speed)
        {
            Active = true;
            hit = false;

            Owner = owner;

            transform.Position = initPos;
            transform.Orientation = initRot;
            this.speed = speed;

            Particle = PoolManager.Create<ShitCannonTraceEmitter>();
            Particle.Initialize(transform, 0.05f);

            Trail = PoolManager.Create<TrailEffect>();
            Trail.Initialize(5, GameSystem.TargetFPS * 3, 20, transform);

            Trail.RadiusCurve.AddKeyFrame(new KeyFrame<float>(1f, 0f));
            Trail.RadiusCurve.AddKeyFrame(new KeyFrame<float>(0.2f, 2.7f));
            Trail.RadiusCurve.AddKeyFrame(new KeyFrame<float>(0, 3f));
            Trail.RadiusCurve.PreCompute();

            Trail.ColorCurve.AddKeyFrame(new KeyFrame<Color4>(new Color4(8, 255, 125, 255), 0f));
            Trail.ColorCurve.AddKeyFrame(new KeyFrame<Color4>(new Color4(0, 255, 195, 255), 0.6f));
            Trail.ColorCurve.AddKeyFrame(new KeyFrame<Color4>(new Color4(0, 255, 164, 218), 1.4f));
            Trail.ColorCurve.AddKeyFrame(new KeyFrame<Color4>(new Color4(185, 255, 0, 60), 2.7f));
            Trail.ColorCurve.AddKeyFrame(new KeyFrame<Color4>(new Color4(Color4.Red.R, Color4.Red.G, Color4.Red.B, 0), 3f));
            Trail.ColorCurve.PreCompute();

            Collider = PoolManager.Create<SphericalCollider>();
            Collider.Initialize(transform, 1, this, OnCollision);
        }

        public void Clear()
        {
        }
        public void Activate()
        {
        }
        public void Deactivate()
        {
            PoolManager.Destroy(Collider);
            PoolManager.Destroy(Particle);
            PoolManager.Destroy(Trail);
        }

        private void OnCollision(SphericalCollider opponent)
        {
            if (opponent.Owner is ShipBase)
            {
                ShipBase ship = opponent.Owner as ShipBase;
                if (ship != this.Owner)
                    hit = true;
            }
        }
    }
}
