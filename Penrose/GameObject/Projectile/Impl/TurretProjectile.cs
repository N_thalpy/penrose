﻿using OpenTK;
using OpenTK.Graphics;
using Penrose.GameObject.Projectile.Renderer;
using Penrose.GameObject.Ship;
using Penrose.Physics;
using Penrose.Pool;
using Penrose.Rendering.Effect;
using Penrose.Rendering.Effect.AnimationCurve;
using Penrose.Rendering.Effect.Emitter.Impl;
using Penrose.Rendering.Renderer;

namespace Penrose.GameObject.Projectile.Impl
{
    public sealed class TurretProjectile : ProjectileBase, IPoolable
    {
        public bool Active;
        private bool hit;

        private Vector3 velocity;

        public SphericalCollider Collider;

        public ShitCannonTraceEmitter Particle;
        public TrailEffect Trail;

        private float timer;

        public TurretProjectile()
            : base()
        {
            Active = true;
            hit = false;
        }

        public override void Update(float deltaTime)
        {
            if (hit == true)
            {
                Trail.Update(deltaTime, false);
                Particle.Update(deltaTime, false);
            }
            else
            {
                transform.Position += velocity * deltaTime;
                Particle.Update(deltaTime, true);
                Trail.Update(deltaTime, true);
            }

            timer += deltaTime;
            if (timer > 4f)
                hit = true;
            if (hit == true && Trail.ValidLogCount == 0)
                Active = false;
        }
        public void Render()
        {
            if (hit == false)
            {
                TurretProjectileRenderer.Instance.Enqueue(this);
                ColliderRenderer.Instance.Enqueue(this.Collider);
            }
            
            Particle.Render();
            Trail.Render();
        }

        public void Initialize(ShipBase owner, Vector3 initPos, Quaternion initRot, Vector3 vel)
        {
            hit = false;
            Active = true;

            Owner = owner;

            transform.Position = initPos;
            transform.Orientation = initRot;
            velocity = vel;

            Particle = PoolManager.Create<ShitCannonTraceEmitter>();
            Particle.Initialize(transform, 0.05f);

            Trail = PoolManager.Create<TrailEffect>();
            Trail.Initialize(5, GameSystem.TargetFPS * 3, 20, transform);

            Trail.RadiusCurve.AddKeyFrame(new KeyFrame<float>(1f, 0f));
            Trail.RadiusCurve.AddKeyFrame(new KeyFrame<float>(0.2f, 2.7f));
            Trail.RadiusCurve.AddKeyFrame(new KeyFrame<float>(0, 3f));
            Trail.RadiusCurve.PreCompute();

            Trail.ColorCurve.AddKeyFrame(new KeyFrame<Color4>(new Color4(255, 8, 125, 255), 0f));
            Trail.ColorCurve.AddKeyFrame(new KeyFrame<Color4>(new Color4(255, 0, 195, 255), 0.6f));
            Trail.ColorCurve.AddKeyFrame(new KeyFrame<Color4>(new Color4(255, 0, 164, 218), 1.4f));
            Trail.ColorCurve.AddKeyFrame(new KeyFrame<Color4>(new Color4(255, 185, 0, 60), 2.7f));
            Trail.ColorCurve.AddKeyFrame(new KeyFrame<Color4>(new Color4(Color4.Red.R, Color4.Red.G, Color4.Red.B, 0), 3f));
            Trail.ColorCurve.PreCompute();

            timer = 0;
        }

        public void Clear()
        {
            Active = true;
        }
        public void Activate()
        {
            Collider = PoolManager.Create<SphericalCollider>();
            Collider.Initialize(transform, 1, this, OnCollision);
        }
        public void Deactivate()
        {
            PoolManager.Destroy(Collider);
            PoolManager.Destroy(Particle);
            PoolManager.Destroy(Trail);
        }

        private void OnCollision(SphericalCollider opponent)
        {
            if (opponent.Owner is ShipBase)
            {
                ShipBase ship = opponent.Owner as ShipBase;
                if (ship != this.Owner)
                    hit = true;
            }
        }
    }
}
