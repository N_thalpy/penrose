﻿using Penrose.GameObject.Ship;
using System;

namespace Penrose.GameObject.Equipment.Weapon
{
    public abstract class WeaponBase : IDisposable
    {
        public bool CanDispose;

        public ShipBase Owner;
        protected float coolTime;
        protected float projectileSpeed;

        public abstract void Update(float deltaTime, bool fired);
        public abstract void Render(Transform eyeTransform);

        public abstract void Dispose();
    }
}
