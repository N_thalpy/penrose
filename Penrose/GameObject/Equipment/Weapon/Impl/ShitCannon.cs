﻿using OpenTK;
using Penrose.GameObject.Projectile.Impl;
using Penrose.Pool;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Penrose.GameObject.Equipment.Weapon.Impl
{
    public sealed class ShitCannon : WeaponBase
    {
        private bool disposed;
        private float time;

        private List<TurretProjectile> projectileList;

        private Random rd;

        public ShitCannon()
            : base()
        {
            projectileList = new List<TurretProjectile>();
            projectileSpeed = 200f;

            disposed = false;
            time = 0f;

            rd = new Random();
        }

        public override void Update(float deltaTime, bool fired)
        {
            CanDispose = projectileList.Count == 0;

            Debug.Assert(disposed == false);
            if (fired == true && coolTime <= 0)
            {
                coolTime = 1f;
                TurretProjectile pb;

                pb = PoolManager.Create<TurretProjectile>();
                pb.Initialize(
                    Owner,
                    Owner.Transform.Position + Owner.Transform.Orientation * new Vector3(3, 0, 0),
                    Owner.Transform.Orientation,
                    Owner.Transform.Orientation * (projectileSpeed * -Vector3.UnitZ) + Owner.CurrentWorldVelocity);
                projectileList.Add(pb);

                pb = PoolManager.Create<TurretProjectile>();
                pb.Initialize(
                    Owner,
                    Owner.Transform.Position + Owner.Transform.Orientation * new Vector3(-3, 0, 0),
                    Owner.Transform.Orientation,
                    Owner.Transform.Orientation * (projectileSpeed * -Vector3.UnitZ) + Owner.CurrentWorldVelocity);
                projectileList.Add(pb);
            }

            coolTime -= deltaTime;
            time += deltaTime;

            foreach (TurretProjectile p in projectileList)
                if (p.Active == false)
                    PoolManager.Destroy(p);

            projectileList.RemoveAll(_ => _.Active == false);

            foreach (TurretProjectile pb in projectileList)
                pb.Update(deltaTime);
        }

        public override void Render(Transform eyeTransform)
        {
            Debug.Assert(disposed == false);
            foreach (TurretProjectile proj in projectileList)
                proj.Render();
        }

        public override void Dispose()
        {
            Debug.Assert(disposed == false);

            foreach (TurretProjectile p in projectileList)
                PoolManager.Destroy(p);
            projectileList.Clear();
        }
    }
}
