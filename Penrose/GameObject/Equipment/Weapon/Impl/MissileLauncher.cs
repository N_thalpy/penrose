﻿using OpenTK;
using Penrose.GameObject.Projectile.Impl;
using Penrose.Pool;
using System.Collections.Generic;

namespace Penrose.GameObject.Equipment.Weapon.Impl
{
    public sealed class MissileLauncher : WeaponBase
    {
        private List<MissileProjectile> projectileList;

        public MissileLauncher()
            : base()
        {
            projectileList = new List<MissileProjectile>();
            projectileSpeed = 180f;
        }

        public override void Update(float deltaTime, bool fired)
        {
            coolTime -= deltaTime;
            if (fired == true && coolTime <= 0)
            {
                coolTime = 2f;

                MissileProjectile pb;
                pb = PoolManager.Create<MissileProjectile>();
                pb.Initialize(
                    Owner,
                    Owner.Transform.Position + Owner.Transform.Orientation * new Vector3(0, -10, 0),
                    Owner.Transform.Orientation,
                    (Owner.Transform.Orientation * (projectileSpeed * -Vector3.UnitZ) + Owner.CurrentWorldVelocity).Length);

                projectileList.Add(pb);
            }

            foreach (MissileProjectile p in projectileList)
                if (p.Active == false)
                    PoolManager.Destroy(p);

            projectileList.RemoveAll(_ => _.Active == false);

            foreach (MissileProjectile pb in projectileList)
                pb.Update(deltaTime);
        }

        public override void Render(Transform eyeTransform)
        {
            foreach (MissileProjectile proj in projectileList)
                proj.Render();
        }

        public override void Dispose()
        {
            foreach (MissileProjectile p in projectileList)
                PoolManager.Destroy(p);
            projectileList.Clear();
        }
    }
}
