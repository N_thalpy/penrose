﻿using OpenTK;
using Penrose.GameObject.Ship;

namespace Penrose.GameObject.Equipment.Engine
{
    public abstract class EngineBase
    {
        public ShipBase Owner;

        public float MaxYaw;
        public float MaxPitch;
        public float MaxRoll;
        public float MaxThrust;
        public float MaxReverseThrust;
        public float FrictionCoefficient;

        protected float currentThrust;
        public Vector3 CurrentWorldVelocity
        {
            get;
            protected set;
        }
        protected Vector3 currentRotateSpeed;

        public abstract void Update(float deltaTime, bool accel, bool breakdown, float pitch, float yaw, float roll);
    }
}
