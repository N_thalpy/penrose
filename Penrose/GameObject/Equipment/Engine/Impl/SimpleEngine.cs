﻿using OpenTK;
using System.Diagnostics;

namespace Penrose.GameObject.Equipment.Engine.Impl
{
    public sealed class SimpleEngine : EngineBase
    {
        public SimpleEngine()
        {
            MaxYaw = MathHelper.PiOver2;
            MaxPitch = MathHelper.PiOver2;
            MaxRoll = MathHelper.PiOver2;

            MaxThrust = 400;
            MaxReverseThrust = -30;

            FrictionCoefficient = 1f;
        }
        public override void Update(float deltaTime, bool accel, bool breakdown, float pitch, float yaw, float roll)
        {
            Debug.Assert(MaxYaw > 0);
            Debug.Assert(MaxPitch > 0);
            Debug.Assert(MaxRoll > 0);

            Debug.Assert(MaxThrust > 0);
            Debug.Assert(MaxReverseThrust < 0);
            Debug.Assert(0 < FrictionCoefficient);

            // dv = -v_max * fric*dt + thrust * dt = 0
            // v_max = thrust/fric

            float thrustDest = 0;
            thrustDest = accel ? MaxThrust : thrustDest;
            thrustDest = breakdown ? MaxReverseThrust : thrustDest;
            currentThrust += 0.01f * (thrustDest - currentThrust);

            CurrentWorldVelocity -= CurrentWorldVelocity * FrictionCoefficient * deltaTime;
            CurrentWorldVelocity += Owner.Transform.Orientation * -Vector3.UnitZ * currentThrust * deltaTime;

            Vector3 rotateSpeedDest = new Vector3(pitch * MaxYaw, yaw * MaxPitch, roll * MaxRoll);
            currentRotateSpeed = Vector3.Lerp(currentRotateSpeed, rotateSpeedDest, 0.01f);

            Owner.Transform.Position += CurrentWorldVelocity * deltaTime;
            Owner.Transform.Orientation *= Quaternion.FromEulerAngles(currentRotateSpeed.X * deltaTime, currentRotateSpeed.Y * deltaTime, currentRotateSpeed.Z * deltaTime);
        }
    }
}
