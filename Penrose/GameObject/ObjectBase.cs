﻿using OpenTK.Graphics;
using Penrose.Rendering;

namespace Penrose.GameObject
{
    public sealed class ObjectBase
    {
        public WavefrontObject Model;
        public Transform Transform;
        public Color4 Color;
        
        public ObjectBase(WavefrontObject obj)
        {
            this.Model = obj;
            Transform = new Transform();
        }
        
        public void Clear()
        {
            Transform.Clear();
        }
    }
}
