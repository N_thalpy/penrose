﻿using OpenTK;

namespace Penrose.GameObject
{
    public sealed class Transform
    {
        private Vector3 position;
        private Quaternion orientation;
        private Vector3 scale;

        private Matrix4 rotMat;

        private bool isTransformMatrixDirty;
        private Matrix4 transformMatrix;
        public Matrix4 TransformMatrix
        {
            get
            {
                if (isTransformMatrixDirty == true)
                {
                    Matrix4.CreateFromQuaternion(ref orientation, out rotMat);
                    transformMatrix = new Matrix4(
                        rotMat[0, 0] * scale.X, rotMat[0, 1] * scale.X, rotMat[0, 2] * scale.X, 0,
                        rotMat[1, 0] * scale.Y, rotMat[1, 1] * scale.Y, rotMat[1, 2] * scale.Y, 0,
                        rotMat[2, 0] * scale.Z, rotMat[2, 1] * scale.Z, rotMat[2, 2] * scale.Z, 0,
                        position.X, position.Y, position.Z, 1);
                    isTransformMatrixDirty = false;
                }

                return transformMatrix;
            }
        }

        private bool isInvertedTransformMatrixDirty;
        private Matrix4 invertedTransformMatrix;
        public Matrix4 InvertedTransformMatrix
        {
            get
            {
                if (isInvertedTransformMatrixDirty == true)
                {
                    // Can be optimized
                    invertedTransformMatrix = TransformMatrix.Inverted();
                    isInvertedTransformMatrixDirty = false;
                }

                return invertedTransformMatrix;
            }
        }

        public Vector3 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                isTransformMatrixDirty = true;
                isInvertedTransformMatrixDirty = true;
            }
        }
        public Quaternion Orientation
        {
            get
            {
                return orientation;
            }
            set
            {
                orientation = value.Normalized();
                isTransformMatrixDirty = true;
                isInvertedTransformMatrixDirty = true;
            }
        }
        public Vector3 Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
                isTransformMatrixDirty = true;
                isInvertedTransformMatrixDirty = true;
            }
        }

        public Transform()
        {
            Clear();
        }

        public void Clear()
        {
            position = Vector3.Zero;
            orientation = Quaternion.Identity;
            scale = Vector3.One;

            transformMatrix = Matrix4.Identity;
            invertedTransformMatrix = Matrix4.Identity;

            isTransformMatrixDirty = true;
            isInvertedTransformMatrixDirty = true;
        }
    }
}
