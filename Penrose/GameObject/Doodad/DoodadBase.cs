﻿using System;

namespace Penrose.GameObject.Doodad
{
    public abstract class DoodadBase : IDisposable
    {
        protected Transform Transform;

        protected DoodadBase()
        {
            Transform = new Transform();
        }

        public abstract void Update(float deltaTime);
        public abstract void Render(Transform eyeTransform);

        public abstract void Dispose();
    }
}
