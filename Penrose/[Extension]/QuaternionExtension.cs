﻿using OpenTK;
using Penrose.UnitTest;
using System;
using System.Diagnostics;

namespace Penrose
{
    public static class QuaternionExtension
    {
        // Reference: http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/Quaternions.pdf
        public static void ToEulerAngle(this Quaternion q, out float pitch, out float yaw, out float roll)
        {
            float sinPitch = 2 * (q.W * q.Z + q.Y * q.X);
            
            if (sinPitch <= -1 || sinPitch >= 1)
            {
                roll = 0;
                pitch = Math.Sign(sinPitch) * MathHelper.PiOver2;
                yaw = (float)Math.Atan2(q.Y, q.W);
            }
            else
            {
                float zz = q.Z * q.Z;

                roll = (float)Math.Atan2(2 * (q.W * q.X - q.Y * q.Z), 1 - 2 * (zz + q.X * q.X));
                pitch = (float)Math.Asin(sinPitch);
                yaw = (float)Math.Atan2(2 * (q.W * q.Y - q.Z * q.X), 1 - 2 * (q.Y * q.Y + zz));
            }
        }

        [UnitTest]
        public static void UnitTest()
        {
            Random rd = new Random(271828182);

            for (int idx = 0; idx < 1000000; idx++)
            {
                Quaternion quat = new Quaternion(
                    10 * (rd.NextFloat() - 0.5f),
                    10 * (rd.NextFloat() - 0.5f),
                    10 * (rd.NextFloat() - 0.5f),
                    10 * (rd.NextFloat() - 0.5f));
                quat.Normalize();

                quat.ToEulerAngle(out float pitch, out float yaw, out float roll);
                Quaternion evaluated = Quaternion.FromEulerAngles(pitch, yaw, roll);

                float eps = 0.01f;
                Debug.Assert(Math.Abs((quat.Inverted() * evaluated).W) > 1 - eps);
            }
        }
    }
}
