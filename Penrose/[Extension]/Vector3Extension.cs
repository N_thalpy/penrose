﻿using OpenTK;

namespace Penrose
{
    public static class Vector3Extension
    {
        public static Vector3 ElementwiseProduct(this Vector3 v, float x, float y, float z) => new Vector3(v.X * x, v.Y * y, v.Z * z);

        public static Vector2 SwizzleXX(this Vector3 v) => new Vector2(v.X, v.X);
        public static Vector2 SwizzleXY(this Vector3 v) => new Vector2(v.X, v.Y);
        public static Vector2 SwizzleXZ(this Vector3 v) => new Vector2(v.X, v.Z);
        public static Vector2 SwizzleX0(this Vector3 v) => new Vector2(v.X, 0);
        public static Vector2 SwizzleYX(this Vector3 v) => new Vector2(v.Y, v.X);
        public static Vector2 SwizzleYY(this Vector3 v) => new Vector2(v.Y, v.Y);
        public static Vector2 SwizzleYZ(this Vector3 v) => new Vector2(v.Y, v.Z);
        public static Vector2 SwizzleY0(this Vector3 v) => new Vector2(v.Y, 0);
        public static Vector2 SwizzleZX(this Vector3 v) => new Vector2(v.Z, v.X);
        public static Vector2 SwizzleZY(this Vector3 v) => new Vector2(v.Z, v.Y);
        public static Vector2 SwizzleZZ(this Vector3 v) => new Vector2(v.Z, v.Z);
        public static Vector2 SwizzleZ0(this Vector3 v) => new Vector2(v.Z, 0);
        public static Vector2 Swizzle0X(this Vector3 v) => new Vector2(0, v.X);
        public static Vector2 Swizzle0Y(this Vector3 v) => new Vector2(0, v.Y);
        public static Vector2 Swizzle0Z(this Vector3 v) => new Vector2(0, v.Z);
        public static Vector2 Swizzle00(this Vector3 v) => new Vector2(0, 0);

        public static Vector3 SwizzleXXX(this Vector3 v) => new Vector3(v.X, v.X, v.X);
        public static Vector3 SwizzleXXY(this Vector3 v) => new Vector3(v.X, v.X, v.Y);
        public static Vector3 SwizzleXXZ(this Vector3 v) => new Vector3(v.X, v.X, v.Z);
        public static Vector3 SwizzleXX0(this Vector3 v) => new Vector3(v.X, v.X, 0);
        public static Vector3 SwizzleXYX(this Vector3 v) => new Vector3(v.X, v.Y, v.X);
        public static Vector3 SwizzleXYY(this Vector3 v) => new Vector3(v.X, v.Y, v.Y);
        public static Vector3 SwizzleXYZ(this Vector3 v) => new Vector3(v.X, v.Y, v.Z);
        public static Vector3 SwizzleXY0(this Vector3 v) => new Vector3(v.X, v.Y, 0);
        public static Vector3 SwizzleXZX(this Vector3 v) => new Vector3(v.X, v.Z, v.X);
        public static Vector3 SwizzleXZY(this Vector3 v) => new Vector3(v.X, v.Z, v.Y);
        public static Vector3 SwizzleXZZ(this Vector3 v) => new Vector3(v.X, v.Z, v.Z);
        public static Vector3 SwizzleXZ0(this Vector3 v) => new Vector3(v.X, v.Z, 0);
        public static Vector3 SwizzleX0X(this Vector3 v) => new Vector3(v.X, 0, v.X);
        public static Vector3 SwizzleX0Y(this Vector3 v) => new Vector3(v.X, 0, v.Y);
        public static Vector3 SwizzleX0Z(this Vector3 v) => new Vector3(v.X, 0, v.Z);
        public static Vector3 SwizzleX00(this Vector3 v) => new Vector3(v.X, 0, 0);
        public static Vector3 SwizzleYXX(this Vector3 v) => new Vector3(v.Y, v.X, v.X);
        public static Vector3 SwizzleYXY(this Vector3 v) => new Vector3(v.Y, v.X, v.Y);
        public static Vector3 SwizzleYXZ(this Vector3 v) => new Vector3(v.Y, v.X, v.Z);
        public static Vector3 SwizzleYX0(this Vector3 v) => new Vector3(v.Y, v.X, 0);
        public static Vector3 SwizzleYYX(this Vector3 v) => new Vector3(v.Y, v.Y, v.X);
        public static Vector3 SwizzleYYY(this Vector3 v) => new Vector3(v.Y, v.Y, v.Y);
        public static Vector3 SwizzleYYZ(this Vector3 v) => new Vector3(v.Y, v.Y, v.Z);
        public static Vector3 SwizzleYY0(this Vector3 v) => new Vector3(v.Y, v.Y, 0);
        public static Vector3 SwizzleYZX(this Vector3 v) => new Vector3(v.Y, v.Z, v.X);
        public static Vector3 SwizzleYZY(this Vector3 v) => new Vector3(v.Y, v.Z, v.Y);
        public static Vector3 SwizzleYZZ(this Vector3 v) => new Vector3(v.Y, v.Z, v.Z);
        public static Vector3 SwizzleYZ0(this Vector3 v) => new Vector3(v.Y, v.Z, 0);
        public static Vector3 SwizzleY0X(this Vector3 v) => new Vector3(v.Y, 0, v.X);
        public static Vector3 SwizzleY0Y(this Vector3 v) => new Vector3(v.Y, 0, v.Y);
        public static Vector3 SwizzleY0Z(this Vector3 v) => new Vector3(v.Y, 0, v.Z);
        public static Vector3 SwizzleY00(this Vector3 v) => new Vector3(v.Y, 0, 0);
        public static Vector3 SwizzleZXX(this Vector3 v) => new Vector3(v.Z, v.X, v.X);
        public static Vector3 SwizzleZXY(this Vector3 v) => new Vector3(v.Z, v.X, v.Y);
        public static Vector3 SwizzleZXZ(this Vector3 v) => new Vector3(v.Z, v.X, v.Z);
        public static Vector3 SwizzleZX0(this Vector3 v) => new Vector3(v.Z, v.X, 0);
        public static Vector3 SwizzleZYX(this Vector3 v) => new Vector3(v.Z, v.Y, v.X);
        public static Vector3 SwizzleZYY(this Vector3 v) => new Vector3(v.Z, v.Y, v.Y);
        public static Vector3 SwizzleZYZ(this Vector3 v) => new Vector3(v.Z, v.Y, v.Z);
        public static Vector3 SwizzleZY0(this Vector3 v) => new Vector3(v.Z, v.Y, 0);
        public static Vector3 SwizzleZZX(this Vector3 v) => new Vector3(v.Z, v.Z, v.X);
        public static Vector3 SwizzleZZY(this Vector3 v) => new Vector3(v.Z, v.Z, v.Y);
        public static Vector3 SwizzleZZZ(this Vector3 v) => new Vector3(v.Z, v.Z, v.Z);
        public static Vector3 SwizzleZZ0(this Vector3 v) => new Vector3(v.Z, v.Z, 0);
        public static Vector3 SwizzleZ0X(this Vector3 v) => new Vector3(v.Z, 0, v.X);
        public static Vector3 SwizzleZ0Y(this Vector3 v) => new Vector3(v.Z, 0, v.Y);
        public static Vector3 SwizzleZ0Z(this Vector3 v) => new Vector3(v.Z, 0, v.Z);
        public static Vector3 SwizzleZ00(this Vector3 v) => new Vector3(v.Z, 0, 0);
        public static Vector3 Swizzle0XX(this Vector3 v) => new Vector3(0, v.X, v.X);
        public static Vector3 Swizzle0XY(this Vector3 v) => new Vector3(0, v.X, v.Y);
        public static Vector3 Swizzle0XZ(this Vector3 v) => new Vector3(0, v.X, v.Z);
        public static Vector3 Swizzle0X0(this Vector3 v) => new Vector3(0, v.X, 0);
        public static Vector3 Swizzle0YX(this Vector3 v) => new Vector3(0, v.Y, v.X);
        public static Vector3 Swizzle0YY(this Vector3 v) => new Vector3(0, v.Y, v.Y);
        public static Vector3 Swizzle0YZ(this Vector3 v) => new Vector3(0, v.Y, v.Z);
        public static Vector3 Swizzle0Y0(this Vector3 v) => new Vector3(0, v.Y, 0);
        public static Vector3 Swizzle0ZX(this Vector3 v) => new Vector3(0, v.Z, v.X);
        public static Vector3 Swizzle0ZY(this Vector3 v) => new Vector3(0, v.Z, v.Y);
        public static Vector3 Swizzle0ZZ(this Vector3 v) => new Vector3(0, v.Z, v.Z);
        public static Vector3 Swizzle0Z0(this Vector3 v) => new Vector3(0, v.Z, 0);
        public static Vector3 Swizzle00X(this Vector3 v) => new Vector3(0, 0, v.X);
        public static Vector3 Swizzle00Y(this Vector3 v) => new Vector3(0, 0, v.Y);
        public static Vector3 Swizzle00Z(this Vector3 v) => new Vector3(0, 0, v.Z);
        public static Vector3 Swizzle000(this Vector3 v) => Vector3.Zero;
    }
}