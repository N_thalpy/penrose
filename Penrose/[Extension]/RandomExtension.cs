﻿using OpenTK;
using System;

namespace Penrose
{
    public static class RandomExtension
    {
        public static float NextFloat(this Random rd)
        {
            return (float)rd.NextDouble();
        }

        public static Vector3 NextUnit3DVector(this Random rd)
        {
            float theta = 2 * MathHelper.Pi * rd.NextFloat();
            float phi = MathHelper.Pi * rd.NextFloat();

            return new Vector3(
                (float)(Math.Sin(theta) * Math.Cos(phi)),
                (float)(Math.Sin(theta) * Math.Sin(phi)),
                (float)(Math.Cos(theta)));
        }

        public static Vector2 NextUnit2DVector(this Random rd)
        {
            float theta = 2 * MathHelper.Pi * rd.NextFloat();

            return new Vector2((float)(Math.Cos(theta)), (float)(Math.Sin(theta)));
        }
    }
}
