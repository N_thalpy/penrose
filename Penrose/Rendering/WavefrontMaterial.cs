﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Penrose.Rendering
{
    public sealed class WavefrontMaterial
    {
        public String Name;

        private List<Material> materialList;
        private Material currentMaterial
        {
            get
            {
                return materialList[materialList.Count - 1];
            }
        }
        
        public Material this[String name]
        {
            get
            {
                return materialList.Find(_ => _.Name == name);
            }
        }

        public WavefrontMaterial(String fname)
        {
            Name = fname;
            materialList = new List<Material>();

            using (StreamReader sr = new StreamReader(fname))
                Parse(sr);
        }

        private void Parse(StreamReader sr)
        {
            int lineNumber = 0;
            while (sr.EndOfStream == false)
            {
                String trimmed = sr.ReadLine().Trim();
                String[] split = trimmed.Split(' ').Where(_ => String.IsNullOrWhiteSpace(_) == false).ToArray();
                lineNumber++;

                if (split.Length == 0)
                    continue;

                switch (split[0])
                {
                    case "#":
                        break;

                    case "newmtl":
                        materialList.Add(new Material());
                        currentMaterial.Name = split[1];
                        break;

                    case "map_Kd":
                        currentMaterial.Texture = Texture.CreateFromBitmapPath(Path.Combine(Path.GetDirectoryName(Name), split[1]));
                        break;
                        
                    case "illum":
                    case "Ka":
                    case "Kd":
                    case "Ks":
                    case "Ns":
                        break;
                }
            }
        }
    }
}
