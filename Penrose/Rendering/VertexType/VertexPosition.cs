﻿using OpenTK;
using Penrose.Rendering.Shader.ShaderVar;
using System.Runtime.InteropServices;

namespace Penrose.Rendering.VertexType
{
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexPosition
    {
        public Vector3 Position;

        public VertexPosition(float x, float y, float z)
        {
            Position = new Vector3(x, y, z);
        }
        public static void Setup(ShaderBase shader)
        {
            shader.StartBindLocation<VertexPosition>();

            shader.BindLocation(SV.VertInPosition, 3);

            shader.EndBindLocation();
            
            GLHelper.CheckGLError();
        }
    }
}
