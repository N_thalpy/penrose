﻿using OpenTK;
using OpenTK.Graphics;
using Penrose.Rendering.Shader.ShaderVar;
using System.Runtime.InteropServices;

namespace Penrose.Rendering.VertexType
{
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexPositionColor
    {
        public Vector3 Position;
        public Color4 Color;

        public VertexPositionColor(Vector3 position, Color4 color)
        {
            Position = position;
            Color = color;
        }

        public static void Setup(ShaderBase shader)
        {
            shader.StartBindLocation<VertexPositionColor>();

            shader.BindLocation(SV.VertInPosition, 3);
            shader.BindLocation(SV.VertInColor, 4);

            shader.EndBindLocation();
        }
    }
}
