﻿using OpenTK;
using OpenTK.Graphics;
using Penrose.Rendering.Shader.ShaderVar;
using System.Runtime.InteropServices;

namespace Penrose.Rendering.VertexType
{
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexPositionTextureColor
    {
        public Vector3 Position;
        public Vector2 TexCoord;
        public Color4 Color;

        public VertexPositionTextureColor(Vector3 p, Vector2 t, Color4 c)
        {
            Position = p;
            TexCoord = t;
            Color = c;
        }
        public VertexPositionTextureColor(Vector3 p, Vector2 t)
            : this(p, t, Color4.White)
        {
        }

        public VertexPositionTextureColor(float x, float y, float z, float tx, float ty, float r, float g, float b, float a)
        {
            Position.X = x;
            Position.Y = y;
            Position.Z = z;

            TexCoord.X = tx;
            TexCoord.Y = ty;

            Color.A = a;
            Color.R = r;
            Color.G = g;
            Color.B = b;
        }
        public VertexPositionTextureColor(float x, float y, float z, float tx, float ty)
            : this (x, y, z, tx, ty, 1, 1, 1, 1)
        {
        }

        public static void Setup(ShaderBase shader)
        {
            shader.StartBindLocation<VertexPositionTextureColor>();

            shader.BindLocation(SV.VertInPosition, 3);
            shader.BindLocation(SV.VertInTextureCoord, 2);
            shader.BindLocation(SV.VertInColor, 4);

            shader.EndBindLocation();
        }
    }
}
