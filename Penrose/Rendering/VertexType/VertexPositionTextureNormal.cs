﻿using OpenTK;
using Penrose.Rendering.Shader.ShaderVar;
using System.Runtime.InteropServices;

namespace Penrose.Rendering.VertexType
{
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexPositionTextureNormal
    {
        public Vector3 Position;
        public Vector2 TexCoord;
        public Vector3 Normal;
        
        public VertexPositionTextureNormal(Vector3 position, Vector2 texCoord, Vector3 normal)
        {
            Position = position;
            TexCoord = texCoord;
            Normal = normal;
        }

        public static void Setup(ShaderBase shader)
        {
            shader.StartBindLocation<VertexPositionTextureColor>();

            shader.BindLocation(SV.VertInPosition, 3);
            shader.BindLocation(SV.VertInTextureCoord, 2);
            shader.BindLocation(SV.VertInNormal, 4);

            shader.EndBindLocation();
        }
    }
}
