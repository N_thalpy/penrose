﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace Penrose.Rendering.VertexType
{
    public delegate void VertexSetup(ShaderBase shader);

    public static class VertexTypes
    {
        static class VertexSetupHolder<T>
        {
            public static VertexSetup VertexSetup;
            static VertexSetupHolder()
            {
                MethodInfo methodInfo = typeof(T).GetMethod("Setup");
                Debug.Assert(methodInfo != null, "Setup Not Found");

                VertexSetup = Delegate.CreateDelegate(typeof(VertexSetup), methodInfo) as VertexSetup;
            }
        }

        public static VertexSetup Setup<T>()
        {
            return VertexSetupHolder<T>.VertexSetup;
        }
    }
}
