﻿using OpenTK;
using OpenTK.Graphics;
using Penrose.Rendering.Shader.ShaderVar;
using System.Runtime.InteropServices;

namespace Penrose.Rendering.VertexType
{
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexPositionColorNormal
    {
        public Vector3 Position;
        public Color4 Color;
        public Vector3 Normal;

        public VertexPositionColorNormal(Vector3 position, Color4 color, Vector3 normal)
        {
            Position = position;
            Color = color;
            Normal = normal;
        }

        public static void Setup(ShaderBase shader)
        {
            shader.StartBindLocation<VertexPositionColorNormal>();

            shader.BindLocation(SV.VertInPosition, 3);
            shader.BindLocation(SV.VertInColor, 4);
            shader.BindLocation(SV.VertInNormal, 3);

            shader.EndBindLocation();
        }
    }
}
