﻿using OpenTK;
using Penrose.Rendering.Shader.ShaderVar;
using System.Runtime.InteropServices;

namespace Penrose.Rendering.VertexType
{
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexPositionTexture
    {
        public Vector3 Position;
        public Vector2 TexCoord;

        public VertexPositionTexture(Vector3 v, Vector2 vt)
        {
            Position = v;
            TexCoord = vt;
        }
        public VertexPositionTexture(float x, float y, float z, Vector2 t)
        {
            Position = new Vector3(x, y, z);
            TexCoord = t;
        }
        public VertexPositionTexture(float x, float y, float z, float tx, float ty)
        {
            Position = new Vector3(x, y, z);
            TexCoord = new Vector2(tx, ty);
        }

        public static void Setup(ShaderBase shader)
        {
            shader.StartBindLocation<VertexPositionTexture>();

            shader.BindLocation(SV.VertInPosition, 3);
            shader.BindLocation(SV.VertInTextureCoord, 2);

            shader.EndBindLocation();
        }
    }
}
