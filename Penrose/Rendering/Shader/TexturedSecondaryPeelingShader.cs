﻿using Penrose.Rendering.Shader.ShaderVar;
using System;

namespace Penrose.Rendering.Shader
{
    public sealed class TexturedSecondaryPeelingShader : ShaderBase
    {
        private static String vSource = $@"
            #version 430 core
                
            layout (location = 0) uniform mat4 {SV.UniformTransform};
                
            layout (location = 0) in vec3 {SV.VertInPosition};
            layout (location = 1) in vec2 {SV.VertInTextureCoord};   
            layout (location = 2) in vec4 {SV.VertInColor};

            out vec4 {SV.FragInColor};
            out vec2 {SV.FragInTextureCoord};
                
            void main()
            {{
                {SV.FragInColor} = {SV.VertInColor};
                {SV.FragInTextureCoord} = {SV.VertInTextureCoord};

                gl_Position = {SV.UniformTransform} * vec4({SV.VertInPosition}, 1);
            }}
        ";

        private static String fSource = $@"
            #version 430 core

            layout (binding = 0) uniform sampler2D {SV.UniformTexture};
            layout (binding = 1) uniform sampler2D {SV.UniformMaxDepthTexture};

            in vec4 {SV.FragInColor};
            in vec2 {SV.FragInTextureCoord};

            out vec4 {SV.FragOutColor};

            void main()
            {{
                if (gl_FragCoord.z <= texelFetch({SV.UniformMaxDepthTexture}, ivec2(gl_FragCoord.xy), 0).r)
                    discard;

                {SV.FragOutColor} = texture({SV.UniformTexture}, {SV.FragInTextureCoord}) * {SV.FragInColor};
                if ({SV.FragOutColor}.w == 0)
                    discard;
            }}
        ";

        public TexturedSecondaryPeelingShader()
            : base(vSource, fSource,
                  new String[] { SV.UniformTransform, SV.UniformTexture },
                  new String[] { SV.VertInPosition, SV.VertInTextureCoord, SV.VertInColor },
                  new String[] { SV.UniformTexture, SV.UniformMaxDepthTexture })
        {
        }
    }
}
