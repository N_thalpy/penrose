﻿using Penrose.Rendering.Shader.ShaderVar;
using System;

namespace Penrose.Rendering.Shader
{
    public sealed class ColorSecondaryPeelingShader : ShaderBase
    {
        private static String vSource = $@"
            #version 430 core
                
            layout (location = 0) uniform mat4 {SV.UniformTransform};

            layout (location = 0) in vec3 {SV.VertInPosition}; 
            layout (location = 1) in vec4 {SV.VertInColor};

            out vec4 {SV.FragInColor};
                
            void main()
            {{
                gl_Position = {SV.UniformTransform} * vec4({SV.VertInPosition}, 1);
                {SV.FragInColor} = {SV.VertInColor};  
            }}
            ";

        private static String fSource = $@"
            #version 430 core
            
            layout (binding = 0) uniform sampler2D {SV.UniformMinDepthTexture};
            layout (binding = 1) uniform sampler2D {SV.UniformMaxDepthTexture};

            in vec4 {SV.FragInColor};
            out vec4 {SV.FragOutColor};

            void main()
            {{
                if (gl_FragCoord.z >= texelFetch({SV.UniformMinDepthTexture}, ivec2(gl_FragCoord.xy), 0).r)
                    discard;
                if (gl_FragCoord.z <= texelFetch({SV.UniformMaxDepthTexture}, ivec2(gl_FragCoord.xy), 0).r)
                    discard;

                {SV.FragOutColor} = {SV.FragInColor};
            }}
            ";

        public ColorSecondaryPeelingShader()
            : base(vSource, fSource,
                  new String[] { SV.UniformTransform },
                  new String[] { SV.VertInPosition, SV.VertInColor },
                  new String[] { SV.UniformMinDepthTexture, SV.UniformMaxDepthTexture })
        {
        }
    }
}