﻿using Penrose.Rendering.Shader.ShaderVar;
using System;

namespace Penrose.Rendering.Shader
{
    public sealed class ColorShader : ShaderBase
    {
        private static String vSource = $@"
            #version 430 core
                
            layout (location = 0) uniform mat4 {SV.UniformTransform};

            layout (location = 0) in vec3 {SV.VertInPosition}; 
            layout (location = 1) in vec4 {SV.VertInColor};

            out vec4 {SV.FragInColor};
                
            {SV.LinearlizeDepth.Body}

            void main()
            {{
                gl_Position = {SV.UniformTransform} * vec4({SV.VertInPosition}, 1);
                {SV.FragInColor} = {SV.VertInColor};  
            }}
            ";

        private static String fSource = $@"
            #version 430 core

            in vec4 {SV.FragInColor};

            out vec4 {SV.FragOutColor};

            void main()
            {{
                {SV.FragOutColor} = {SV.FragInColor};
            }}
            ";

        public ColorShader()
            : base(vSource, fSource,
                  new String[] { SV.UniformTransform },
                  new String[] { SV.VertInPosition, SV.VertInColor },
                  new String[] { })
        {
        }
    }
}
