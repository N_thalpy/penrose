﻿using Penrose.Rendering.Shader.ShaderVar;
using System;

namespace Penrose.Rendering.Shader
{
    public sealed class BitmapFontShader : ShaderBase
    {
        private static String vSource = $@"
                #version 430 core
                
                layout (location = 0) uniform mat4 {SV.UniformTransform};
                
                layout (location = 0) in vec3 {SV.VertInPosition};
                layout (location = 1) in vec2 {SV.VertInTextureCoord};   

                out vec2 {SV.FragInTextureCoord};
                
                void main()
                {{
                    {SV.FragInTextureCoord} = {SV.VertInTextureCoord};

                    gl_Position = {SV.UniformTransform} * vec4({SV.VertInPosition}, 1);
                }}
            ";

        private static String fSource = $@"
                #version 430 core

                layout (binding = 0) uniform sampler2D {SV.UniformTexture};

                layout (location = 1) uniform vec4 {SV.UniformColor};

                in vec2 {SV.FragInTextureCoord};

                out vec4 {SV.FragOutColor};

                void main()
                {{
                    vec4 texColor = texture({SV.UniformTexture}, {SV.FragInTextureCoord});
                    {SV.FragOutColor} = texColor.xxxx * {SV.UniformColor};
                }}
            ";

        public BitmapFontShader()
            : base(vSource, fSource,
                  new String[] { SV.UniformTransform, SV.UniformColor },
                  new String[] { SV.VertInPosition, SV.VertInTextureCoord },
                  new String[] { SV.UniformTexture })
        {
        }
    }
}
