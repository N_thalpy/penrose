﻿using Penrose.Rendering.Shader.ShaderVar;
using System;

namespace Penrose.Rendering.Shader
{
    public sealed class BloomShader : ShaderBase
    {
        private static String vSource = $@"
            #version 430 core

            layout (location = 0) uniform mat4 {SV.UniformTransform};

            layout (location = 0) in vec3 {SV.VertInPosition};
            layout (location = 1) in vec2 {SV.VertInTextureCoord};

            out vec2 {SV.FragInTextureCoord};

            void main()
            {{
                gl_Position = vec4({SV.VertInPosition}, 1);
                {SV.FragInTextureCoord} = {SV.VertInTextureCoord};
            }}
            ";
        private static String fSource = $@"
            #version 430 core

            layout (binding = 0) uniform sampler2D {SV.UniformTexture}; 

            in vec2 {SV.FragInTextureCoord};

            out vec4 {SV.FragOutColor};

            void main()
            {{
                vec2 uv = {SV.FragInTextureCoord};
                vec2 dir = vec2(1, 1) * 0;

                vec4 orig = texture2D({SV.UniformTexture}, uv);
                vec4 color = vec4(0.0);

                color += texture2D({SV.UniformTexture}, uv + 0 * dir) * {SV.Gaussian.GaussianWeight(0, 7)};

                color += texture2D({SV.UniformTexture}, uv + 1 * dir) * {SV.Gaussian.GaussianWeight(1, 7)};
                color += texture2D({SV.UniformTexture}, uv + 2 * dir) * {SV.Gaussian.GaussianWeight(2, 7)};
                color += texture2D({SV.UniformTexture}, uv + 3 * dir) * {SV.Gaussian.GaussianWeight(3, 7)};
                color += texture2D({SV.UniformTexture}, uv + 4 * dir) * {SV.Gaussian.GaussianWeight(4, 7)};
                color += texture2D({SV.UniformTexture}, uv + 5 * dir) * {SV.Gaussian.GaussianWeight(5, 7)};
                color += texture2D({SV.UniformTexture}, uv + 6 * dir) * {SV.Gaussian.GaussianWeight(6, 7)}; 

                color += texture2D({SV.UniformTexture}, uv - 1 * dir) * {SV.Gaussian.GaussianWeight(1, 7)};
                color += texture2D({SV.UniformTexture}, uv - 2 * dir) * {SV.Gaussian.GaussianWeight(2, 7)};
                color += texture2D({SV.UniformTexture}, uv - 3 * dir) * {SV.Gaussian.GaussianWeight(3, 7)};
                color += texture2D({SV.UniformTexture}, uv - 4 * dir) * {SV.Gaussian.GaussianWeight(4, 7)};
                color += texture2D({SV.UniformTexture}, uv - 5 * dir) * {SV.Gaussian.GaussianWeight(5, 7)};
                color += texture2D({SV.UniformTexture}, uv - 6 * dir) * {SV.Gaussian.GaussianWeight(6, 7)};                

                {SV.FragOutColor} = color * (vec4(1) - orig) + orig;
            }}
            ";

        public BloomShader()
            : base(vSource, fSource, 
                  new String[] { SV.UniformTransform },
                  new String[] { SV.VertInPosition, SV.VertInTextureCoord},
                  new String[] { SV.UniformTexture })
        {
        }
    }
}
