﻿using Penrose.Rendering.Shader.ShaderVar;
using System;

namespace Penrose.Rendering.Shader
{
    public sealed class TexturedShader : ShaderBase
    {
        private static String vSource = $@"
                #version 430 core
                
                layout (location = 0) uniform mat4 {SV.UniformTransform};
                
                layout (location = 0) in vec3 {SV.VertInPosition};
                layout (location = 1) in vec2 {SV.VertInTextureCoord};   
                layout (location = 2) in vec4 {SV.VertInColor};

                out vec4 {SV.FragInColor};
                out vec2 {SV.FragInTextureCoord};
                
                void main()
                {{
                    {SV.FragInColor} = {SV.VertInColor};
                    {SV.FragInTextureCoord} = {SV.VertInTextureCoord};

                    gl_Position = {SV.UniformTransform} * vec4({SV.VertInPosition}, 1);
                }}
            ";

        private static String fSource = $@"
                #version 430 core

                layout (binding = 0) uniform sampler2D {SV.UniformTexture};

                in vec4 {SV.FragInColor};
                in vec2 {SV.FragInTextureCoord};

                out vec4 {SV.FragOutColor};

                void main()
                {{
                    {SV.FragOutColor} = texture({SV.UniformTexture}, {SV.FragInTextureCoord}) * {SV.FragInColor};
                }}
            ";

        public TexturedShader()
            : base(vSource, fSource,
                  new String[] { SV.UniformTransform },
                  new String[] { SV.VertInPosition, SV.VertInTextureCoord, SV.VertInColor },
                  new String[] { SV.UniformTexture })
        {
        }
    }
}
