﻿namespace Penrose.Rendering.Shader
{
    public static class ShaderProvider
    {
        public static ColorShader ColorShader;
        public static TexturedShader TexturedShader;
        public static BloomShader BloomShader;
        public static BitmapFontShader BitmapFontShader;
        public static FlatColorShader FlatColorShader;

        public static ColorPrimaryPeelingShader ColorPrimaryPeelingShader;
        public static ColorSecondaryPeelingShader ColorSecondaryPeelingShader;

        public static TexturedPrimaryPeelingShader TexturedPrimaryPeelingShader;
        public static TexturedSecondaryPeelingShader TexturedSecondaryPeelingShader;

        static ShaderProvider()
        {
        }

        public static void Initialize()
        {
            ColorShader = new ColorShader();
            TexturedShader = new TexturedShader();
            BloomShader = new BloomShader();
            BitmapFontShader = new BitmapFontShader();
            FlatColorShader = new FlatColorShader();

            ColorPrimaryPeelingShader = new ColorPrimaryPeelingShader();
            ColorSecondaryPeelingShader = new ColorSecondaryPeelingShader();

            TexturedPrimaryPeelingShader = new TexturedPrimaryPeelingShader();
            TexturedSecondaryPeelingShader = new TexturedSecondaryPeelingShader();

            GameSystem.InvokeOnExit += Dispose;
        }

        private static void Dispose()
        {
            ColorShader.Dispose();
            TexturedShader.Dispose();
            BloomShader.Dispose();
            BitmapFontShader.Dispose();
            FlatColorShader.Dispose();

            ColorPrimaryPeelingShader.Dispose();
            ColorSecondaryPeelingShader.Dispose();
            TexturedPrimaryPeelingShader.Dispose();
            TexturedSecondaryPeelingShader.Dispose();
        }
    }
}
