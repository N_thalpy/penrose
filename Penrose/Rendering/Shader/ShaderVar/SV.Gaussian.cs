﻿using System;
using System.Linq;

namespace Penrose.Rendering.Shader.ShaderVar
{
    public static partial class SV
    {
        public sealed class GaussianHelper
        {
            public double GaussianWeight(int i, int kernelSize)
            {
                return Math.Exp(-i * i / 2) / (1 + 2 * Enumerable.Range(1, kernelSize).Sum(_ => Math.Exp(-_ * _ / 2)));
            }
        }

        public static GaussianHelper Gaussian = new GaussianHelper();
    }
}
