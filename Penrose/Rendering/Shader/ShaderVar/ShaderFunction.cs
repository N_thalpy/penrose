﻿using System;

namespace Penrose.Rendering.Shader.ShaderVar
{
    public sealed class ShaderFunction
    {
        public String Name;
        public String Body;

        public ShaderFunction(String name, String body)
        {
            Name = name;
            Body = body;
        }
    }
}
