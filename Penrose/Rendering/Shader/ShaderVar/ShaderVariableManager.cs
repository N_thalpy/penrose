﻿using OpenTK;
using OpenTK.Graphics;

namespace Penrose.Rendering.Shader.ShaderVar
{
    public struct ShaderVariableManager
    {
        public Matrix4 UniformTransform;
        public Color4 UniformColor;
        public int UniformTexture;

        public void Bind(ShaderBase shader)
        {
            Matrix4 mvp = UniformTransform * GLHelper.CurrentProjection;

            shader.BindUniformMatrix4(SV.UniformTransform, ref mvp);
            shader.BindUniform4(SV.UniformColor, UniformColor);
            shader.BindTexture(SV.UniformTexture, UniformTexture);
        }
    }
}
