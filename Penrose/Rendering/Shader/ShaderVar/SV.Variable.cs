﻿using System;

namespace Penrose.Rendering.Shader.ShaderVar
{
    public static partial class SV
    {
        public const String UniformTransform = "uTrans";
        public const String UniformColor = "uColor";
        public const String UniformOutlineColor = "uOutlineColor";
        public const String UniformTexture = "uTex";
        public const String UniformAngle = "uTheta";
        public const String UniformTime = "uTime";
        public const String UniformSeed = "uSeed";
        public const String UniformPositionLog = "uPositionLog";
        public const String UniformTimeCoefficient = "uTimeCoefficient";
        public const String UniformCameraPosition = "uCameraPosition";
        public const String UniformMaxDepthTexture = "uMaxDepthTexture";
        public const String UniformMinDepthTexture = "uMinDepthTexture";

        public const String VertInPosition = "viPos";
        public const String VertInTextureCoord = "viTex";
        public const String VertInColor = "viColor";
        public const String VertInRadius = "viRadius";
        public const String VertInPhase = "viPhase";
        public const String VertInAxis = "viAxis";
        public const String VertInVelocity = "viVelocity";
        public const String VertInSeed = "viSeed";
        public const String VertInNormal = "viNormal";

        public const String GeoInColor = "giColor";

        public const String FragInColor = "fiColor";
        public const String FragInTextureCoord = "fiTex";
        public const String FragInDiscard = "fiDiscard";
        public const String FragInNormal = "fiNormal";

        public const String FragOutColor = "foColor";  
    }
}
