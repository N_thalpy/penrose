﻿using System;

namespace Penrose.Rendering.Shader.ShaderVar
{
    public static partial class SV<T>
    {
        private static String typeName = SVHelper.GenTypeToString<T>();

        public static ShaderFunction Bezier3 = new ShaderFunction(
            "bezier3",
            $@"
            {typeName} bezier3 (float t, {typeName} p0, {typeName} p1, {typeName} p2)
            {{
                float u = 1.0 - t;
                return (p0 * u * u) + 2.0 * (p1 * u * t) + (p2 * t * t);
            }}
            ");

        public static ShaderFunction Bezier4 = new ShaderFunction(
            "bezier4",
            $@"
            {typeName} bezier4 (float t, {typeName} p0, {typeName} p1, {typeName} p2, {typeName} p3)
            {{
                float t2 = t * t;
                float u = 1.0 - t;
                float u2 = u * u;
                return (p0 * u2 * u) + 3.0 * (p1 * u2 * t) + 3.0 * (p2 * t2 * u) + (p3 * t2 * t);
            }}
            ");

        public static ShaderFunction Bezier5 = new ShaderFunction(
            "bezier5",
            $@"
            {typeName} bezier5 (float t, {typeName} p0, {typeName} p1, {typeName} p2, {typeName} p3, {typeName} p4)
            {{
                float t2 = t * t;
                float u = 1.0 - t;
                float u2 = u * u;
                float ut = u * t;

                return (p0 * u2 * u2) + 4.0 * (p1 * u2 * ut) + 6.0 * (p2 * ut * ut) + 4.0 * (p3 * t2 * ut) + (p4 * t2 * t2);
            }}
            ");
    }
}

