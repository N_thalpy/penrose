﻿using OpenTK;
using System;

namespace Penrose.Rendering.Shader.ShaderVar
{
    public sealed class SVHelper
    {
        public static String GenTypeToString<T>()
        {
            if (typeof(T) == typeof(float))
                return "float";
            if (typeof(T) == typeof(Vector2))
                return "vec2";
            if (typeof(T) == typeof(Vector3))
                return "vec3";
            if (typeof(T) == typeof(Vector4))
                return "vec4";
            throw new ArgumentException();
        }
    }
}
