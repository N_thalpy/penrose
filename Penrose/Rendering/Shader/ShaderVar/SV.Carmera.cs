﻿namespace Penrose.Rendering.Shader.ShaderVar
{
    public static partial class SV
    {
        public static ShaderFunction LinearlizeDepth = new ShaderFunction(
            "linearlizeDepth",
			$@"
			vec4 linearlizeDepth (vec4 v)
			{{
				float newz = {2 / (GLHelper.FarPlane - GLHelper.NearPlane)} * v.w - {(GLHelper.FarPlane + GLHelper.NearPlane) / (GLHelper.FarPlane - GLHelper.NearPlane)};
				return vec4(v.x, v.y, newz * v.w, v.w);
			}}
			");
    }
}
