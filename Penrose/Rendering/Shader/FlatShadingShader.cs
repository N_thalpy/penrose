﻿using Penrose.Rendering.Shader.ShaderVar;
using System;

namespace Penrose.Rendering.Shader
{
    public sealed class FlatColorShader : ShaderBase
    {
        private static String vSource = $@"
            #version 430 core
                
            layout (location = 0) uniform mat4 {SV.UniformTransform};

            layout (location = 0) in vec3 {SV.VertInPosition}; 
            layout (location = 1) in vec4 {SV.VertInColor};
            layout (location = 2) in vec3 {SV.VertInNormal};

            out vec4 {SV.FragInColor};
            flat out vec3 {SV.FragInNormal};
                
            void main()
            {{
                gl_Position = {SV.UniformTransform} * vec4({SV.VertInPosition}, 1);
                {SV.FragInColor} = {SV.VertInColor};  
                {SV.FragInNormal} = {SV.VertInNormal};
            }}
            ";

        private static String fSource = $@"
            #version 430 core

            in vec4 {SV.FragInColor};
            flat in vec3 {SV.FragInNormal};

            out vec4 {SV.FragOutColor};

            void main()
            {{
                {SV.FragOutColor} = {SV.FragInColor} * (0.8 + 0.2 * max(0, {SV.FragInNormal}.y));
            }}
            ";

        public FlatColorShader()
            : base(vSource, fSource, 
                  new String[] { SV.UniformTransform },
                  new String[] { SV.VertInPosition, SV.VertInColor, SV.VertInNormal },
                  new String[] { })
        {
        }
    }
}
