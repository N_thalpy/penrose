﻿using OpenTK.Graphics.OpenGL;

namespace Penrose.Rendering
{
    public static class TextureProvider
    {
        public static Texture BeamTexture;

        public static Texture MinimapPlayerIcon;
        public static Texture MinimapStingrayIcon;

        static TextureProvider()
        {
        }
        public static void Initialize()
        {
            BeamTexture = Texture.CreateFromBitmapPath(
                "Resource/Effect/NonAlphaBeam.png", 
                TextureMinFilter.Linear,
                TextureMagFilter.Linear,
                TextureWrapMode.Repeat,
                TextureWrapMode.Repeat);

            MinimapPlayerIcon = Texture.CreateFromBitmapPath("Resource/HUD/Minimap/Player.png");
            MinimapStingrayIcon = Texture.CreateFromBitmapPath("Resource/HUD/Minimap/Stingray.png");

            GameSystem.InvokeOnExit += Dispose;
        }

        private static void Dispose()
        {
            BeamTexture.Dispose();

            MinimapPlayerIcon.Dispose();
            MinimapStingrayIcon.Dispose();
        }
    }
}
