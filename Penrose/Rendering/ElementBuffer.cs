﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Diagnostics;

namespace Penrose.Rendering
{
    public sealed class ElementBuffer : IDisposable
    {
        private int ebo;
        public int Count
        {
            get;
            private set;
        }

        public bool Loaded
        {
            get;
            private set;
        }

        [CanRunOn(ThreadType.All)]
        private ElementBuffer(int ebo, int count)
        {
            this.ebo = ebo;
            this.Count = count;
        }
        ~ElementBuffer()
        {
            Debug.Assert(ebo == 0);
        }

        [CanRunOn(ThreadType.All)]
        public static ElementBuffer Create(int count)
        {
            int ebo = 0;

            GameSystem.RunOnRenderThread(() =>
            {
                GLHelper.EnterGLSection();

                ebo = GL.GenBuffer();

                GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);
                GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(sizeof(Int32) * count), IntPtr.Zero, BufferUsageHint.StaticDraw);
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

                GLHelper.ExitGLSection();
            }, "EBO Generation");

            ElementBuffer eb = new ElementBuffer(ebo, count);
            return eb;
        }
        [CanRunOn(ThreadType.All)]
        public static ElementBuffer Create(int[] elements)
        {
            int ebo = 0;
            int count = 0;

            GameSystem.RunOnRenderThread(() =>
            {
                GLHelper.EnterGLSection();

                count = elements.Length;
                ebo = GL.GenBuffer();

                GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);
                GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(sizeof(int) * count), elements, BufferUsageHint.StaticDraw);
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

                GLHelper.ExitGLSection();
            }, "EBO Generation");

            ElementBuffer eb = new ElementBuffer(ebo, count);
            eb.Loaded = true;
            return eb;
        }

        public unsafe void Upload(void* indices, long offset, long count)
        {
            Upload(indices, (int)offset, (int)count);
        }
        [CanRunOn(ThreadType.All)]
        public unsafe void Upload(void* indices, int offset, int count)
        {
            Upload((int*)indices, offset, count);
        }
        [CanRunOn(ThreadType.All)]
        public unsafe void Upload(int* indices, int offset, int count)
        {
            GameSystem.RunOnRenderThread(() =>
            {
                GLHelper.EnterGLSection();

                GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);
                GL.BufferSubData(BufferTarget.ElementArrayBuffer, (IntPtr)offset, (IntPtr)(sizeof(int) * count), new IntPtr(indices));
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

                GLHelper.ExitGLSection();
            }, "EBO Uplaod");
        }

        [CanRunOn(ThreadType.All)]
        public void Seal()
        {
            Debug.Assert(Loaded == false);
            Loaded = true;
        }
        [CanRunOn(ThreadType.All)]
        public void UnSeal()
        {
            Debug.Assert(Loaded == true);
            Loaded = false;
        }
        [CanRunOn(ThreadType.MainThread)]
        public void Resize(int count)
        {
            if (count == Count)
                return;

            ThreadHelper.CheckCallerThread();
            Debug.Assert(Loaded == false);

            Count = count;

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(sizeof(Int32) * count), IntPtr.Zero, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }

        [CanRunOn(ThreadType.MainThread)]
        public void Bind()
        {
            ThreadHelper.CheckCallerThread();
            if (Loaded == false)
                return;

            Debug.Assert(ebo != 0);

            GLHelper.EnterGLSection();

            Debug.Assert(GL.GetInteger(GetPName.ElementArrayBufferBinding) == 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);

            GLHelper.ExitGLSection();
        }
        [CanRunOn(ThreadType.MainThread)]
        [Conditional("DEBUG")]
        public void Unbind()
        {
            ThreadHelper.CheckCallerThread();
            if (Loaded == false)
                return;

            Debug.Assert(ebo != 0);

            GLHelper.EnterGLSection();

            Debug.Assert(GL.GetInteger(GetPName.ElementArrayBufferBinding) == ebo);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            GLHelper.ExitGLSection();
        }

        [CanRunOn(ThreadType.MainThread)]
        public void Dispose()
        {
            ThreadHelper.CheckCallerThread();

            Debug.Assert(ebo != 0);

            GLHelper.EnterGLSection();
            GL.DeleteBuffer(ebo);
            GLHelper.ExitGLSection();

            ebo = 0;
        }
    }
}
