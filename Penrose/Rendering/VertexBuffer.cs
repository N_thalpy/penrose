﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Penrose.Rendering.VertexType;
using System;
using System.Diagnostics;

namespace Penrose.Rendering
{
    public sealed class VertexBuffer
    {
        public int vbo;
        public int Count
        {
            get;
            private set;
        }

        public bool Loaded
        {
            get;
            private set;
        }

        private VertexSetup setup;

        [CanRunOn(ThreadType.All)]
        private VertexBuffer(int vbo, int count, VertexSetup setup)
        {
            this.vbo = vbo;
            this.Count = count;
            this.setup = setup;
        }

        [CanRunOn(ThreadType.All)]
        public static VertexBuffer Create<T>(int count) where T : struct
        {
            VertexBuffer vb = null;
            int stride = BlittableValueType.StrideOf<T>(new T());

            GameSystem.RunOnRenderThread(() =>
            {
                GLHelper.EnterGLSection();
                int vbo = GL.GenBuffer();

                vb = new VertexBuffer(vbo, count, VertexTypes.Setup<T>());

                GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(stride * count), IntPtr.Zero, BufferUsageHint.StaticDraw);
                GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

                GLHelper.CheckGLError();
                GLHelper.ExitGLSection();
            }, "VBO Allocation");

            return vb;
        }
        [CanRunOn(ThreadType.All)]
        public static VertexBuffer Create<T>(T[] vertices) where T : struct
        {
            VertexBuffer vb = null;
            GameSystem.RunOnRenderThread(() =>
            {
                GLHelper.EnterGLSection();
                int stride = BlittableValueType<T>.Stride;
                int count = vertices.Length;

                int vbo = GL.GenBuffer();

                vb = new VertexBuffer(vbo, count, VertexTypes.Setup<T>());

                GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(stride * count), vertices, BufferUsageHint.StaticDraw);
                GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

                GLHelper.CheckGLError();
                GLHelper.ExitGLSection();
            }, "VBO Allocation");

            vb.Loaded = true;
            return vb;
        }
        [CanRunOn(ThreadType.All)]
        public unsafe static VertexBuffer Create<T>(void* vertices, int count) where T : struct
        {
            VertexBuffer vb = null;
            GameSystem.RunOnRenderThread(() =>
            {
                GLHelper.EnterGLSection();
                int stride = BlittableValueType<T>.Stride;

                int vbo = GL.GenBuffer();

                vb = new VertexBuffer(vbo, count, VertexTypes.Setup<T>());

                GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(stride * count), new IntPtr(vertices), BufferUsageHint.StaticDraw);
                GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

                GLHelper.CheckGLError();
                GLHelper.ExitGLSection();
            }, "VBO Allocation");

            vb.Loaded = true;
            return vb;
        }

        public unsafe void Upload<T>(void* vertices, long offset, long count) where T : struct
        {
            Upload<T>(vertices, (int)offset, (int)count);
        }
        [CanRunOn(ThreadType.All)]
        public unsafe void Upload<T>(void* vertices, int offset, int count) where T : struct
        {
            Debug.Assert(Loaded == false);
            int stride = BlittableValueType<T>.Stride;

            GameSystem.RunOnRenderThread(() =>
            {
                GLHelper.EnterGLSection();

                GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
                GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr)offset, (IntPtr)(count * stride), new IntPtr(vertices));
                GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

                GLHelper.CheckGLError();
                GLHelper.ExitGLSection();
            }, String.Format("VBO Uploading (vbo: {0}", vbo));
        }

        [CanRunOn(ThreadType.All)]
        public void Seal()
        {
            Debug.Assert(Loaded == false);
            Loaded = true;
        }
        // For synchronization, only main thread can invoke unseal
        [CanRunOn(ThreadType.MainThread)]
        public void UnSeal()
        {
            ThreadHelper.CheckCallerThread();

            Debug.Assert(Loaded == true);
            Loaded = false;
        }
        [CanRunOn(ThreadType.MainThread)]
        public void Resize<T>(int count)
        {
            if (Count == count)
                return;

            ThreadHelper.CheckCallerThread();
            Debug.Assert(Loaded == false);

            Count = count;
            int stride = BlittableValueType<T>.Stride;

            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(stride * count), IntPtr.Zero, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        [CanRunOn(ThreadType.MainThread)]
        public void Bind(ShaderBase shader)
        {
            ThreadHelper.CheckCallerThread();
            if (Loaded == false)
                return;

            Debug.Assert(vbo != 0);

            GLHelper.EnterGLSection();

            Debug.Assert(GL.GetInteger(GetPName.ArrayBufferBinding) == 0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);

            GLHelper.ExitGLSection();

            setup.Invoke(shader);
        }
        [CanRunOn(ThreadType.MainThread)]
        [Conditional("DEBUG")]
        public void Unbind()
        {
            ThreadHelper.CheckCallerThread();
            if (Loaded == false)
                return;

            Debug.Assert(vbo != 0);

            GLHelper.EnterGLSection();

            Debug.Assert(GL.GetInteger(GetPName.ArrayBufferBinding) == vbo);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            GLHelper.ExitGLSection();
        }

        [CanRunOn(ThreadType.MainThread)]
        public void Dispose()
        {
            ThreadHelper.CheckCallerThread();
            Debug.Assert(vbo != 0);

            GLHelper.EnterGLSection();
            GL.DeleteBuffer(vbo);
            GLHelper.ExitGLSection();

            vbo = 0;
        }

        [Conditional("DEBUG")]
        [CanRunOn(ThreadType.MainThread)]
        private static void CheckUploadState(int count, int stride)
        {
            int size;
            GL.GetBufferParameter(BufferTarget.ArrayBuffer, BufferParameterName.BufferSize, out size);
            if (count * stride != size)
                throw new ApplicationException("Vertex data not uploaded correctly");
        }
    }
}
