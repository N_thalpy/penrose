﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Penrose.Rendering.Shader;
using Penrose.Rendering.Shader.ShaderVar;
using Penrose.Rendering.VertexType;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Penrose.Rendering
{
    public sealed class RenderQueue : IDisposable
    {
        // Want to allocate at stack, to prevent calling garvage collector
        public struct OpaqueRenderJob
        {
            public VertexBuffer VertexBuffer;
            public ElementBuffer ElementBuffer;
            public ShaderBase Shader;

            public Int64 ElementCount;
            public PrimitiveType PrimitiveType;

            public ShaderVariableManager VariableManager;
        }
        public struct TransparentRenderJob
        {
            public VertexBuffer VertexBuffer;
            public ElementBuffer ElementBuffer;

            public ShaderBase PrimaryPeelingShader;
            public ShaderBase SecondaryPeelingShader;

            public Int64 ElementCount;
            public PrimitiveType PrimitiveType;

            public ShaderVariableManager VariableManager;
        }
        public struct DepthIndependentRenderJob
        {
            public VertexBuffer VertexBuffer;
            public ElementBuffer ElementBuffer;
            public ShaderBase Shader;

            public Int64 ElementCount;
            public PrimitiveType PrimitiveType;

            public ShaderVariableManager VariableManager;
        }

        public static RenderQueue Instance;
        private static List<PrimitiveType> validPrimitiveType;

        static RenderQueue()
        {
            validPrimitiveType = new List<PrimitiveType>();

            validPrimitiveType.Add(PrimitiveType.Lines);
            validPrimitiveType.Add(PrimitiveType.Triangles);
            validPrimitiveType.Add(PrimitiveType.Quads);
        }
        public static void Initialize()
        {
            RenderQueue.Instance = new RenderQueue();
            GameSystem.InvokeOnExit += RenderQueue.Instance.Dispose;
        }

        private const int FrameBufferCount = 3;

        private bool disposed;

        private List<OpaqueRenderJob> opaqueJobList;
        private List<TransparentRenderJob> transparentJobList;
        private List<DepthIndependentRenderJob> depthIndependentJobList;

        private FrameBuffer opaqueFrameBuffer;
        private FrameBuffer[] transparentFrameBuffers;
        private TexturedShader textureShader;

        private VertexBuffer frameVertexBuffer;
        private ElementBuffer frameElementBuffer;

        private RenderQueue()
        {
            disposed = false;

            opaqueJobList = new List<OpaqueRenderJob>();
            transparentJobList = new List<TransparentRenderJob>();
            depthIndependentJobList = new List<DepthIndependentRenderJob>();

            opaqueFrameBuffer = FrameBuffer.Create();
            transparentFrameBuffers = new FrameBuffer[FrameBufferCount];
            for (int idx = 0; idx < FrameBufferCount; idx++)
                transparentFrameBuffers[idx] = FrameBuffer.Create();

            frameVertexBuffer = VertexBuffer.Create(new VertexPositionTextureColor[4]
            {
                new VertexPositionTextureColor(-1, 1, 0, 0, 1),
                new VertexPositionTextureColor(-1, -1, 0, 0, 0),
                new VertexPositionTextureColor(1, -1, 0, 1, 0),
                new VertexPositionTextureColor(1, 1, 0, 1, 1),
            });
            frameElementBuffer = ElementBuffer.Create(new int[4]
            {
                0, 1, 2, 3,
            });

            textureShader = new TexturedShader();
        }
        ~RenderQueue()
        {
            Debug.Assert(disposed == true);
        }

        public void Clear()
        {
            opaqueJobList.Clear();
            transparentJobList.Clear();
            depthIndependentJobList.Clear();
        }

        public void Enqueue(OpaqueRenderJob renderJob)
        {
            if (renderJob.ElementCount != 0)
                opaqueJobList.Add(renderJob);
        }
        public void Enqueue(TransparentRenderJob renderJob)
        {
            if (renderJob.ElementCount != 0)
                transparentJobList.Add(renderJob);
        }
        public void Enqueue(DepthIndependentRenderJob renderJob)
        {
            if (renderJob.ElementCount != 0)
                depthIndependentJobList.Add(renderJob);
        }

        [CanRunOn(ThreadType.MainThread)]
        public void Flush()
        {
            if (disposed == true)
                return;

            Debug.Assert(opaqueJobList.TrueForAll(_ => validPrimitiveType.Contains(_.PrimitiveType)));
            Debug.Assert(transparentJobList.TrueForAll(_ => validPrimitiveType.Contains(_.PrimitiveType)));
            Debug.Assert(depthIndependentJobList.TrueForAll(_ => validPrimitiveType.Contains(_.PrimitiveType)));

            GL.FrontFace(FrontFaceDirection.Ccw);
            GL.Enable(EnableCap.CullFace);

            Matrix4 mvp;

            ThreadHelper.CheckCallerThread();

            GL.ClearColor(new Color4(245, 245, 255, 0));
            GL.ClearDepth(1);
            GL.Enable(EnableCap.DepthTest);

            // Opaque Render
            opaqueFrameBuffer.BindFrameBuffer();
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            foreach (OpaqueRenderJob job in opaqueJobList)
            {
                job.VertexBuffer.Bind(job.Shader);
                job.ElementBuffer.Bind();
                job.Shader.Bind();

                job.VariableManager.Bind(job.Shader);
                GLHelper.DrawPrimitive(job.PrimitiveType, job.ElementCount);

                job.VertexBuffer.Unbind();
                job.ElementBuffer.Unbind();
                job.Shader.Unbind();
            }

            opaqueFrameBuffer.UnbindFrameBuffer();

            // Primary Transparent Render
            transparentFrameBuffers[0].BindFrameBuffer();
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            foreach (TransparentRenderJob job in transparentJobList)
            {
                job.VertexBuffer.Bind(job.PrimaryPeelingShader);
                job.ElementBuffer.Bind();
                job.PrimaryPeelingShader.Bind();

                job.VariableManager.Bind(job.PrimaryPeelingShader);
                job.PrimaryPeelingShader.BindTexture(SV.UniformMinDepthTexture, opaqueFrameBuffer.DepthBuffer);

                GLHelper.DrawPrimitive(job.PrimitiveType, job.ElementCount);

                job.VertexBuffer.Unbind();
                job.ElementBuffer.Unbind();
                job.PrimaryPeelingShader.Unbind();
            }

            transparentFrameBuffers[0].UnbindFrameBuffer();

            // Secondary Transparent Render
            for (int idx = 1; idx < FrameBufferCount; idx++)
            {
                transparentFrameBuffers[idx].BindFrameBuffer();
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

                foreach (TransparentRenderJob job in transparentJobList)
                {
                    job.VertexBuffer.Bind(job.SecondaryPeelingShader);
                    job.ElementBuffer.Bind();
                    job.SecondaryPeelingShader.Bind();

                    job.VariableManager.Bind(job.SecondaryPeelingShader);
                    job.SecondaryPeelingShader.BindTexture(SV.UniformMinDepthTexture, opaqueFrameBuffer.DepthBuffer);
                    job.SecondaryPeelingShader.BindTexture(SV.UniformMaxDepthTexture, transparentFrameBuffers[idx - 1].DepthBuffer);

                    GLHelper.DrawPrimitive(job.PrimitiveType, job.ElementCount);

                    job.VertexBuffer.Unbind();
                    job.ElementBuffer.Unbind();
                    job.SecondaryPeelingShader.Unbind();
                }

                transparentFrameBuffers[idx].UnbindFrameBuffer();
            }

            // Flush OIT
            GL.Disable(EnableCap.DepthTest);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            frameVertexBuffer.Bind(textureShader);
            frameElementBuffer.Bind();
            textureShader.Bind();

            mvp = Matrix4.Identity;
            textureShader.BindUniformMatrix4(SV.UniformTransform, ref mvp);
            textureShader.BindTexture(SV.UniformTexture, opaqueFrameBuffer.ColorBuffer);

            GLHelper.DrawPrimitive(PrimitiveType.Quads, 4);

            for (int idx = transparentFrameBuffers.Length - 1; idx >= 0; idx--)
            {
                textureShader.BindTexture(SV.UniformTexture, transparentFrameBuffers[idx].ColorBuffer);
                GLHelper.DrawPrimitive(PrimitiveType.Quads, 4);
            }

            frameVertexBuffer.Unbind();
            frameElementBuffer.Unbind();
            textureShader.Unbind();
            
            // Depth Independent Render
            foreach (DepthIndependentRenderJob job in depthIndependentJobList)
            {
                job.VertexBuffer.Bind(job.Shader);
                job.ElementBuffer.Bind();
                job.Shader.Bind();

                job.VariableManager.Bind(job.Shader);
                GLHelper.DrawPrimitive(job.PrimitiveType, job.ElementCount);

                job.VertexBuffer.Unbind();
                job.ElementBuffer.Unbind();
                job.Shader.Unbind();
            }

            GL.Disable(EnableCap.Blend);
            GL.Disable(EnableCap.CullFace);
        }

        public void Dispose()
        {
            opaqueFrameBuffer.Dispose();
            foreach (FrameBuffer frameBuffer in transparentFrameBuffers)
                frameBuffer.Dispose();

            textureShader.Dispose();
            frameVertexBuffer.Dispose();
            frameElementBuffer.Dispose();

            disposed = true;
        }
    }
}
