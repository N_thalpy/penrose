﻿namespace Penrose.Rendering
{
    public static class WavefrontObjectProvider
    {
        public static WavefrontObject Cube;
        public static WavefrontObject Sphere;

        public static WavefrontObject Stingray;
        public static WavefrontObject OrionNebula;
        public static WavefrontObject Trapezium;

        static WavefrontObjectProvider()
        {
        }

        public static void Initialize()
        {
            Cube = new WavefrontObject("Resource/Model/Cube.obj");
            Sphere = new WavefrontObject("Resource/Model/Sphere.obj");
            
            Stingray = new WavefrontObject("Resource/Model/Stingray.obj");

            OrionNebula = new WavefrontObject("Resource/Model/OrionNebula.obj");
            Trapezium = new WavefrontObject("Resource/Model/Trapezium.obj");
        }
    }
}
