﻿using OpenTK;
using Penrose.Rendering.VertexType;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Penrose.Rendering
{
    public sealed class WavefrontObject
    {
        public enum RenderMode
        {
            Shaded,
            Wireframe,
        }

        private class FaceMaterialPair
        {
            public Material Material;
            public List<Face> Face;

            public FaceMaterialPair()
            {
                Material = null;
                Face = null;
            }
        }
        private struct Face
        {
            public int[] Position;
            public int[] TexCoord;
            public int[] Normal;
        }
        private struct Line
        {
            public int Start;
            public int End;

            public override bool Equals(object obj)
            {
                if (obj is Line)
                {
                    Line l = (Line)obj;
                    return (l.Start == this.Start && l.End == this.End) || (l.Start == this.End && l.End == this.Start);
                }

                return false;
            }
            public override int GetHashCode()
            {
                return String.Format("{0} --> {1}", Math.Min(Start, End), Math.Max(Start, End)).GetHashCode();
            }
            public override string ToString()
            {
                return String.Format("{0} --> {1}", Start, End);
            }
        }

        public String Name;

        public VertexPositionTextureNormal[] Vertices;
        public List<int[]> MeshElement;
        public List<int[]> WireframeElement;
        public List<Material> MaterialList;

        private List<Vector3> v;
        private List<Vector2> vt;
        private List<Vector3> vn;

        private WavefrontMaterial material;
        private List<FaceMaterialPair> f;
        private FaceMaterialPair currentFaceMaterialPair;

        public WavefrontObject(String fname)
        {
            Name = fname;

            v = new List<Vector3>();
            vt = new List<Vector2>();
            vn = new List<Vector3>();
            f = new List<FaceMaterialPair>();

            using (StreamReader sr = new StreamReader(fname))
                Parse(sr);

            List<VertexPositionTextureNormal> vptnList = new List<VertexPositionTextureNormal>();

            MeshElement = new List<int[]>();
            WireframeElement = new List<int[]>();
            MaterialList = new List<Material>();

            for (int matCount = 0; matCount < f.Count; matCount++)
            {
                MaterialList.Add(f[matCount].Material);
                List<Face> currentFaceList = f[matCount].Face;
                int[] meshIndices = new int[currentFaceList.Sum(_ => 3 * (_.Position.Length - 2))];
                List<Line> wireframeLines = new List<Line>();
                int meshIndexCount = 0;
                int wireframeIndexCount = 0;

                foreach (Face f in currentFaceList)
                {
#warning TODO: MAKE Face slicing works to non-convex faces

                    int[] meshTemp = new int[f.Position.Length];
                    int[] wireframeTemp = new int[f.Position.Length];

                    for (int i = 0; i < f.Position.Length; i++)
                    {
                        int meshIndex = vptnList.FindIndex(_ =>
                        {
                            bool vDup = (_.Position == v[f.Position[i]]);
                            bool vtDup = (f.TexCoord == null) || (_.TexCoord == vt[f.TexCoord[i]]);
                            bool vnDup = (f.Normal == null) || (_.Normal == vn[f.Normal[i]]);
                            return vDup && vtDup && vnDup;
                        });
                        if (meshIndex == -1)
                        {
                            meshIndex = vptnList.Count;
                            vptnList.Add(new VertexPositionTextureNormal(
                                v[f.Position[i]],
                                f.TexCoord == null ? Vector2.Zero : vt[f.TexCoord[i]],
                                f.Normal == null ? Vector3.Zero : vn[f.Normal[i]]));
                        }

                        meshTemp[i] = meshIndex;
                    }

                    for (int i = 0; i < meshTemp.Length - 2; i++)
                    {
                        meshIndices[meshIndexCount++] = meshTemp[0];
                        meshIndices[meshIndexCount++] = meshTemp[1 + i];
                        meshIndices[meshIndexCount++] = meshTemp[2 + i];
                    }

                    for (int i = 0; i < meshTemp.Length; i++)
                    {
                        wireframeLines.Add(new Line()
                        {
                            Start = meshTemp[i],
                            End = meshTemp[(i + 1) % meshTemp.Length],
                        });
                    }
                }

                wireframeLines = wireframeLines.Distinct().ToList();
                int[] wireframeIndices = new int[wireframeLines.Count * 2];
                for (int idx = 0; idx < wireframeLines.Count; idx++)
                {
                    wireframeIndices[idx * 2 + 0] = wireframeLines[idx].Start;
                    wireframeIndices[idx * 2 + 1] = wireframeLines[idx].End;
                }

                MeshElement.Add(meshIndices);
                WireframeElement.Add(wireframeIndices);
            }
            Vertices = vptnList.ToArray();
        }

        private Face ParseFace(IEnumerable<String> v1)
        {
            Face f = new Face();
            List<String[]> splitted = v1.Select(_ => _.Split('/').ToArray()).ToList();
            int split = splitted[0].Length;

            Debug.Assert(v1.ToList().TrueForAll(_ => _.Split('/').Length == split));

            switch (split)
            {
                case 1:
                    f.Position = splitted.Select(_ => Int32.Parse(_[0]) - 1).ToArray();
                    return f;

                case 2:
                    f.Position = splitted.Select(_ => Int32.Parse(_[0]) - 1).ToArray();
                    f.TexCoord = splitted.Select(_ => Int32.Parse(_[1]) - 1).ToArray();
                    return f;

                case 3:
                    f.Position = splitted.Select(_ => Int32.Parse(_[0]) - 1).ToArray();
                    f.Normal = splitted.Select(_ => Int32.Parse(_[2]) - 1).ToArray();

                    if (String.IsNullOrWhiteSpace(splitted[0][1]))
                        f.TexCoord = null;
                    else
                        f.TexCoord = splitted.Select(_ => Int32.Parse(_[1]) - 1).ToArray();

                    return f;

                default:
                    throw new NotImplementedException();
            }
        }
        private void Parse(StreamReader sr)
        {
            int lineNumber = 0;
            while (sr.EndOfStream == false)
            {
                String trimmed = sr.ReadLine().Trim();
                String[] split = trimmed.Split(' ').Where(_ => String.IsNullOrWhiteSpace(_) == false).ToArray();
                lineNumber++;

                if (split.Length == 0 || split[0].StartsWith("#"))
                    continue;

                switch (split[0])
                {
                    case "":
                        break;

                    case "v":
                        v.Add(new Vector3(Single.Parse(split[1]), Single.Parse(split[2]), Single.Parse(split[3])));
                        break;

                    case "vt":
                        vt.Add(new Vector2(Single.Parse(split[1]), Single.Parse(split[2])));
                        break;

                    case "vn":
                        vn.Add(new Vector3(Single.Parse(split[1]), Single.Parse(split[2]), Single.Parse(split[3])));
                        break;

                    case "f":
                        if (currentFaceMaterialPair == null)
                        {
                            Debug.Assert(material == null);
                            currentFaceMaterialPair = new FaceMaterialPair();
                            currentFaceMaterialPair.Face = new List<Face>();
                            f.Add(currentFaceMaterialPair);
                        }

                        currentFaceMaterialPair.Face.Add(ParseFace(split.Skip(1)));
                        break;

                    case "mtllib":
                        material = new WavefrontMaterial(Path.Combine(Path.GetDirectoryName(Name), split[1]));
                        break;

                    case "usemtl":
                        if (f.Exists(_ => _.Material.Name == split[1]))
                            currentFaceMaterialPair = f.Find(_ => _.Material.Name == split[1]);
                        else
                        {
                            FaceMaterialPair fmp = new FaceMaterialPair();
                            fmp.Face = new List<Face>();
                            fmp.Material = material[split[1]];

                            f.Add(fmp);
                            currentFaceMaterialPair = fmp;
                        }
                        break;

                    case "g":
                    case "s":
                    case "o":
                    case "l":
                        break;

                    default:
                        throw new NotImplementedException();
                }
            }
        }
    }
}
