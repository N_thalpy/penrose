﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Penrose.Rendering
{
    public sealed class Texture : IDisposable
    {
        public int TextureHandle
        {
            get;
            private set;
        }
        public readonly int Width;
        public readonly int Height;

        private TextureMinFilter minFilter;
        private TextureMagFilter magFilter;
        private TextureWrapMode sWrapMode;
        private TextureWrapMode tWrapMode;

        public TextureMinFilter MinFilter
        {
            get
            {
                return minFilter;
            }
        }
        public TextureMagFilter MagFilter
        {
            get
            {
                return magFilter;
            }
        }
        public TextureWrapMode SWrapMode
        {
            get
            {
                return sWrapMode;
            }
        }
        public TextureWrapMode TWrapMode
        {
            get
            {
                return tWrapMode;
            }
        }

        [CanRunOn(ThreadType.All)]
        private Texture(int handle, int width, int height, TextureMinFilter minFilter, TextureMagFilter magFilter, TextureWrapMode sWrapMode, TextureWrapMode tWrapMode)
        {
            TextureHandle = handle;
            Width = width;
            Height = height;

            this.minFilter = minFilter;
            this.magFilter = magFilter;
            this.sWrapMode = sWrapMode;
            this.tWrapMode = tWrapMode;
        }
        ~Texture()
        {
            Debug.Assert(TextureHandle == 0);
        }

        [CanRunOn(ThreadType.MainThread)]
        public static Texture CreateFromTextureHandle(int handle, int width, int height, TextureMinFilter minFilter, TextureMagFilter magFilter, TextureWrapMode sWrapMode, TextureWrapMode tWrapMode)
        {
            ThreadHelper.CheckCallerThread();
            return new Texture(handle, width, height, minFilter, magFilter, sWrapMode, tWrapMode);
        }

        [CanRunOn(ThreadType.MainThread)]
        public static Texture CreateFromBitmap(Bitmap bitmap, TextureMinFilter minFilter, TextureMagFilter magFilter, TextureWrapMode sWrapMode, TextureWrapMode tWrapMode)
        {
            ThreadHelper.CheckCallerThread();
            GLHelper.EnterGLSection();

            int id = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, id);
                       
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)minFilter);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)magFilter);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)sWrapMode);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)tWrapMode);

            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bitmapData.Width, bitmapData.Height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bitmapData.Scan0);
            bitmap.UnlockBits(bitmapData);

            GL.BindTexture(TextureTarget.Texture2D, 0);

            GLHelper.ExitGLSection();

            return Texture.CreateFromTextureHandle(id, bitmap.Width, bitmap.Height, minFilter, magFilter, sWrapMode, tWrapMode);
        }

        public static Texture CreateFromBitmapPath(String path)
        {
            return Texture.CreateFromBitmapPath(path, TextureMinFilter.Nearest, TextureMagFilter.Nearest, TextureWrapMode.Clamp, TextureWrapMode.Clamp);
        }
        [CanRunOn(ThreadType.MainThread)]
        public static Texture CreateFromBitmapPath(String path, TextureMinFilter minFilter, TextureMagFilter magFilter, TextureWrapMode sWrapMode, TextureWrapMode tWrapMode)
        {
            ThreadHelper.CheckCallerThread();

            if (File.Exists(path) == false)
                throw new FileNotFoundException(String.Format("File {0} has not found.", path));

            using (Bitmap bitmap = new Bitmap(path))
                return Texture.CreateFromBitmap(new Bitmap(path), minFilter, magFilter, sWrapMode, tWrapMode);
        }

        [CanRunOn(ThreadType.MainThread)]
        public void Dispose()
        {
            ThreadHelper.CheckCallerThread();
            Debug.Assert(TextureHandle != 0);

            GLHelper.EnterGLSection();
            GL.DeleteTexture(TextureHandle);
            GLHelper.ExitGLSection();

            TextureHandle = 0;
        }
    }
}
