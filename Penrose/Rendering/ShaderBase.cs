﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Penrose.Rendering.Shader.ShaderVar;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Penrose.Rendering
{
    public abstract class ShaderBase : IDisposable
    {
        public int ShaderProgram
        {
            get;
            private set;
        }

        private int geometricShader;
        private int vertexShader;
        private int fragmentShader;

        private int offset;
        private int stride;

        private Dictionary<String, int> uniformLocationDict;
        private Dictionary<String, int> vertexLocationDict;
        private Dictionary<String, int> textureLocationDict;

        [CanRunOn(ThreadType.MainThread)]
        public ShaderBase(String vSource, String fSource, IList<String> uniformLocation, IList<String> vertexLocation, IList<String> textureLocation)
            : this(vSource, "", fSource, uniformLocation, vertexLocation, textureLocation)
        {
        }
        [CanRunOn(ThreadType.MainThread)]
        public ShaderBase(String vSource, String gSource, String fSource, IList<String> uniformLocation, IList<String> vertexLocation, IList<String> textureLocation)
        {
            ThreadHelper.CheckCallerThread();

            if (String.IsNullOrWhiteSpace(gSource) == false)
                CreateGeometricShader(gSource);

            CreateVertexShader(vSource);
            CreateFregmentShader(fSource);
            LinkShader();

            uniformLocationDict = new Dictionary<String, int>();
            for (int i = 0; i < uniformLocation.Count(); i++)
                uniformLocationDict.Add(uniformLocation[i], i);

            vertexLocationDict = new Dictionary<String, int>();
            for (int i = 0; i < vertexLocation.Count(); i++)
                vertexLocationDict.Add(vertexLocation[i], i);

            textureLocationDict = new Dictionary<String, int>();
            for (int i = 0; i < textureLocation.Count(); i++)
                textureLocationDict.Add(textureLocation[i], i);
        }
        ~ShaderBase()
        {
            Debug.Assert(geometricShader == 0);
            Debug.Assert(vertexShader == 0);
            Debug.Assert(fragmentShader == 0);
        }

        [CanRunOn(ThreadType.MainThread)]
        private void CreateGeometricShader(String source)
        {
            ThreadHelper.CheckCallerThread();
            GLHelper.EnterGLSection();
            geometricShader = GL.CreateShader(ShaderType.GeometryShader);
            GLHelper.ExitGLSection();

            CompileShader(geometricShader, source);
        }
        [CanRunOn(ThreadType.MainThread)]
        private void CreateVertexShader(String source)
        {
            ThreadHelper.CheckCallerThread();
            GLHelper.EnterGLSection();
            vertexShader = GL.CreateShader(ShaderType.VertexShader);
            GLHelper.ExitGLSection();

            CompileShader(vertexShader, source);
        }
        [CanRunOn(ThreadType.MainThread)]
        private void CreateFregmentShader(String source)
        {
            ThreadHelper.CheckCallerThread();
            GLHelper.EnterGLSection();
            fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
            GLHelper.ExitGLSection();

            CompileShader(fragmentShader, source);
        }

        [CanRunOn(ThreadType.MainThread)]
        private void CompileShader(int shader, String source)
        {
            ThreadHelper.CheckCallerThread();

            String shaderLog;
            int shaderStatusCode;

            GLHelper.EnterGLSection();

            GL.ShaderSource(shader, source);
            GL.CompileShader(shader);
            GL.GetShaderInfoLog(shader, out shaderLog);
            GL.GetShader(shader, ShaderParameter.CompileStatus, out shaderStatusCode);

            GLHelper.ExitGLSection();

            if (shaderStatusCode == 0)
                throw new InvalidOperationException(String.Format("Shader Compilation Failed: {0}", shaderLog));
        }
        [CanRunOn(ThreadType.MainThread)]
        private void LinkShader()
        {
            ThreadHelper.CheckCallerThread();
            GLHelper.EnterGLSection();

            ShaderProgram = GL.CreateProgram();

            if (geometricShader != 0)
                GL.AttachShader(ShaderProgram, geometricShader);
            GL.AttachShader(ShaderProgram, vertexShader);
            GL.AttachShader(ShaderProgram, fragmentShader);
            GL.BindFragDataLocation(ShaderProgram, 0, SV.FragOutColor);
            GL.LinkProgram(ShaderProgram);

            GLHelper.CheckGLError();
            GLHelper.ExitGLSection();
        }

        [CanRunOn(ThreadType.MainThread)]
        public void Bind()
        {
            ThreadHelper.CheckCallerThread();
            GLHelper.EnterGLSection();

            Debug.Assert(GL.GetInteger(GetPName.CurrentProgram) == 0);
            GL.UseProgram(ShaderProgram);

            GLHelper.ExitGLSection();
        }
        [CanRunOn(ThreadType.MainThread)]
        public void Unbind()
        {
            ThreadHelper.CheckCallerThread();
            GLHelper.EnterGLSection();

            Debug.Assert(GL.GetInteger(GetPName.CurrentProgram) == ShaderProgram);
            GL.UseProgram(0);

            GLHelper.ExitGLSection();
        }

        [CanRunOn(ThreadType.MainThread)]
        public void Dispose()
        {
            ThreadHelper.CheckCallerThread();

            Debug.Assert(vertexShader != 0);
            Debug.Assert(fragmentShader != 0);
            Debug.Assert(ShaderProgram != 0);

            GLHelper.EnterGLSection();

            GL.DeleteShader(vertexShader);
            GL.DeleteShader(fragmentShader);
            if (geometricShader != 0)
                GL.DeleteShader(geometricShader);
            GL.DeleteProgram(ShaderProgram);

            GLHelper.ExitGLSection();

            vertexShader = 0;
            fragmentShader = 0;
            geometricShader = 0;
            ShaderProgram = 0;
        }

        public int GetUniformLocation(String name)
        {
            if (uniformLocationDict.ContainsKey(name) == true)
                return uniformLocationDict[name];
            return -1;
        }
        public int GetTextureLocation(String name)
        {
            if (textureLocationDict.ContainsKey(name) == true)
                return textureLocationDict[name];
            return -1;
        }
        public int GetVertexLocation(String name)
        {
            if (vertexLocationDict.ContainsKey(name) == true)
                return vertexLocationDict[name];
            return -1;
        }

        public void StartBindLocation<T>()
        {
            Debug.Assert(offset == 0);
            Debug.Assert(stride == 0);
            stride = BlittableValueType<T>.Stride;
        }
        [CanRunOn(ThreadType.MainThread)]
        public void BindLocation(String varName, int size)
        {
            ThreadHelper.CheckCallerThread();
            int loc = GetVertexLocation(varName);

            if (loc != -1)
            {
                GLHelper.EnterGLSection();

                GL.EnableVertexAttribArray(loc);
                GL.VertexAttribPointer(loc, size, VertexAttribPointerType.Float, false, stride, offset);

                GLHelper.ExitGLSection();
            }

            offset += size * sizeof(float);
        }
        public void EndBindLocation()
        {
            Debug.Assert(stride == offset);
            offset = 0;
            stride = 0;
        }

        [CanRunOn(ThreadType.MainThread)]
        public void BindUniform1(String name, ref float v)
        {
            ThreadHelper.CheckCallerThread();
            int attr = GetUniformLocation(name);
            if (attr != -1)
            {
                GLHelper.EnterGLSection();
                GL.Uniform1(attr, 1, ref v);
                GLHelper.ExitGLSection();
            }
        }
        [CanRunOn(ThreadType.MainThread)]
        public void BindUniform1(String name, ref int v)
        {
            ThreadHelper.CheckCallerThread();
            int attr = GetUniformLocation(name);
            if (attr != -1)
            {
                GLHelper.EnterGLSection();
                GL.Uniform1(attr, 1, ref v);
                GLHelper.ExitGLSection();
            }
        }
        [CanRunOn(ThreadType.MainThread)]
        public void BindUniform3(String name, Vector3[] v)
        {
            ThreadHelper.CheckCallerThread();
            int attr = GetUniformLocation(name);
            if (attr != -1)
            {
                GLHelper.EnterGLSection();
                unsafe
                {
                    fixed (Vector3* vp = v)
                        GL.Uniform3(attr, v.Length, (float*)vp);
                }
                GLHelper.ExitGLSection();
            }
        }
        [CanRunOn(ThreadType.MainThread)]
        public void BindUniform4(String name, Vector4 v)
        {
            ThreadHelper.CheckCallerThread();
            int attr = GetUniformLocation(name);
            if (attr != -1)
            {
                GLHelper.EnterGLSection();
                GL.Uniform4(attr, v.X, v.Y, v.Z, v.W);
                GLHelper.ExitGLSection();
            }
        }
        [CanRunOn(ThreadType.MainThread)]
        public void BindUniform4(String name, Color4 v)
        {
            ThreadHelper.CheckCallerThread();
            int attr = GetUniformLocation(name);
            if (attr != -1)
            {
                GLHelper.EnterGLSection();
                GL.Uniform4(attr, v.R, v.G, v.B, v.A);
                GLHelper.ExitGLSection();
            }
        }
        [CanRunOn(ThreadType.MainThread)]
        public void BindUniformMatrix4(String name, ref Matrix4 v)
        {
            ThreadHelper.CheckCallerThread();
            int attr = GetUniformLocation(name);
            if (attr != -1)
            {
                GLHelper.EnterGLSection();
                GL.UniformMatrix4(attr, false, ref v);
                GLHelper.ExitGLSection();
            }
        }

        public void BindTexture(String name, Texture texture)
        {
            BindTexture(name, texture.TextureHandle);
        }
        [CanRunOn(ThreadType.MainThread)]
        public void BindTexture(String name, int textureHandle)
        {
            ThreadHelper.CheckCallerThread();
            int attr = GetTextureLocation(name);
            if (attr != -1)
            {
                GL.ActiveTexture(TextureUnit.Texture0 + attr);
                GL.BindTexture(TextureTarget.Texture2D, textureHandle);
            }
        }
    }
}
