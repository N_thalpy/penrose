﻿using System;

namespace Penrose.Rendering
{
    public sealed class Material
    {
        public String Name;
        public Texture Texture;

        public override string ToString()
        {
            return Name;
        }
    }
}
