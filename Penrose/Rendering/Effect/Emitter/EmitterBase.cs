﻿using Penrose.GameObject;

namespace Penrose.Rendering.Effect.Emitter
{
    public abstract class EmitterBase
    {
        public abstract void Render();
    }
}
