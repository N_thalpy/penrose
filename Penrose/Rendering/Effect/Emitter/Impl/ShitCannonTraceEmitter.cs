﻿using OpenTK;
using Penrose.GameObject;
using Penrose.Rendering.Effect.Particle.Impl;
using Penrose.Pool;
using Penrose.Rendering.VertexType;
using System;
using System.Collections.Generic;
using OpenTK.Graphics;

namespace Penrose.Rendering.Effect.Emitter.Impl
{
    public sealed class ShitCannonTraceEmitter : EmitterBase, IPoolable
    {
        private static Random rd;

        static ShitCannonTraceEmitter()
        {
            rd = new Random();
        }

        private Transform currentTransform;
        private Transform targetTransform;

        private List<LinearMovingColoredCube> particleList;

        private Vector3 prevPosition;
        private float genTimer;

        private float genCooldown;

        public ShitCannonTraceEmitter()
            : base()
        {
            currentTransform = new Transform();
            particleList = new List<LinearMovingColoredCube>();
        }
        public void Initialize(Transform targetTransform, float genCooldown)
        {
            this.targetTransform = targetTransform;
            this.genCooldown = genCooldown;

            currentTransform.Position = targetTransform.Position;
            currentTransform.Orientation = targetTransform.Orientation;

            genTimer = 0;
        }

        public void Update(float deltaTime, bool generating)
        {
            prevPosition = currentTransform.Position;

            currentTransform.Position = targetTransform.Position;
            currentTransform.Orientation = targetTransform.Orientation;

            genTimer -= deltaTime;
            if (generating == true && genTimer < 0)
            {
                genTimer += genCooldown;

                LinearMovingColoredCube cc = PoolManager.Create<LinearMovingColoredCube>();

                Vector2 unit = rd.NextUnit2DVector();
                Vector3 vel = currentTransform.Orientation * new Vector3(unit.X, unit.Y, 0) * 10000;
                cc.Initialize(currentTransform.Position, Vector3.One, currentTransform.Orientation, vel, 1f);
                cc.Color = new Color4(255, 0, 79, 255);
                particleList.Add(cc);
            }

            foreach (LinearMovingColoredCube cc in particleList)
                if (cc.IsAlive == true)
                    cc.Update(deltaTime);

            foreach (LinearMovingColoredCube cc in particleList)
                if (cc.IsAlive == false)
                    PoolManager.Destroy(cc);
            particleList.RemoveAll(_ => _.IsAlive == false);
        }
        public override void Render()
        {
            ParticleRenderer<VertexPositionColor>.Instance.Enqueue(particleList);
        }

        public void Activate()
        {
        }
        public void Clear()
        {
            currentTransform.Clear();
            particleList.Clear();
            targetTransform = null;
        }
        public void Deactivate()
        {
            foreach (LinearMovingColoredCube cc in particleList)
                PoolManager.Destroy(cc);
        }
    }
}
