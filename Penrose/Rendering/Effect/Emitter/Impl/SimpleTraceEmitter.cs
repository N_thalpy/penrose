﻿using OpenTK;
using Penrose.GameObject;
using Penrose.Rendering.Effect.Particle.Impl;
using Penrose.Pool;
using Penrose.Rendering.VertexType;
using System;
using System.Collections.Generic;

namespace Penrose.Rendering.Effect.Emitter.Impl
{
    public sealed class SimpleTraceEmitter : EmitterBase, IPoolable
    {
        private static Random rd;

        static SimpleTraceEmitter()
        {
            rd = new Random();
        }

        private Transform currentTransform;
        private Transform targetTransform;

        private List<LinearMovingColoredCube> particleList;

        private Vector3 prevPosition;
        private float genTimer;

        private float genCooldown;

        public SimpleTraceEmitter()
            : base()
        {
            currentTransform = new Transform();
            particleList = new List<LinearMovingColoredCube>();
        }
        public void Initialize(Transform targetTransform, float genCooldown)
        {
            this.targetTransform = targetTransform;
            this.genCooldown = genCooldown;

            currentTransform.Position = targetTransform.Position;
            currentTransform.Orientation = targetTransform.Orientation;

            genTimer = 0;
        }

        public void Update(float deltaTime)
        {
            prevPosition = currentTransform.Position;

            currentTransform.Position = targetTransform.Position;
            currentTransform.Orientation = targetTransform.Orientation;

            genTimer -= deltaTime;
            if (genTimer < 0)
            {
                genTimer += genCooldown;

                LinearMovingColoredCube cc = PoolManager.Create<LinearMovingColoredCube>();

                Vector2 unit = rd.NextUnit2DVector();
                  Vector3 vel = currentTransform.Orientation * new Vector3(unit.X, unit.Y, 0) * 10000;
                cc.Initialize(currentTransform.Position, Vector3.One * 1000, currentTransform.Orientation, vel, 1f);
                particleList.Add(cc);
            }

            foreach (LinearMovingColoredCube cc in particleList)
            {
                if (cc.IsAlive == true)
                    cc.Update(deltaTime);
                else
                    PoolManager.Destroy(cc);
            }

            particleList.RemoveAll(_ => _.IsAlive == false);
        }
        public override void Render()
        {
            ParticleRenderer<VertexPositionColor>.Instance.Enqueue(particleList);
        }

        public void Activate()
        {
        }
        public void Clear()
        {
            currentTransform.Clear();
            particleList.Clear();
            targetTransform = null;
        }
        public void Deactivate()
        {
            foreach (LinearMovingColoredCube cc in particleList)
                PoolManager.Destroy(cc);
        }
    }
}
