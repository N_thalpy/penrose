﻿using OpenTK;
using Penrose.GameObject;
using Penrose.Pool;
using Penrose.Rendering.Effect.AnimationCurve;
using System;
using System.Collections.Generic;

namespace Penrose.Rendering.Effect
{
    public sealed class TrailEffect : IDisposable, IPoolable
    {
        private Transform target;
        
        public int PolygonVertex;
        public int MaxLogCount;
        public int ValidLogCount;
        public int SampleInterval;

        public List<Vector3> PositionLog;
        public List<Quaternion> OrientationLog;
        public List<bool> ValidLog;

        public FloatAnimationCurve RadiusCurve;
        public Color4AnimationCurve ColorCurve;

        public TrailEffect()
        {
            PositionLog = new List<Vector3>();
            OrientationLog = new List<Quaternion>();
            ValidLog = new List<bool>();

            RadiusCurve = new FloatAnimationCurve();
            ColorCurve = new Color4AnimationCurve();
        }
        public void Initialize(int polygonVertex, int maxLogCount, int sampleInterval, Transform targetTransform)
        {
            PolygonVertex = polygonVertex;
            target = targetTransform;
            MaxLogCount = maxLogCount;
            SampleInterval = sampleInterval;

            PositionLog.Clear();
            OrientationLog.Clear();
            ValidLog.Clear();

            ValidLogCount = 0;
        }

        public void Update(float deltaTime, bool isValid)
        {
            if (isValid == true)
                ValidLogCount++;

            PositionLog.Add(target.Position);
            OrientationLog.Add(target.Orientation);
            ValidLog.Add(isValid);
            
            if (PositionLog.Count > MaxLogCount)
            {
                if (ValidLog[0] == true)
                    ValidLogCount -= 1;

                PositionLog.RemoveAt(0);
                OrientationLog.RemoveAt(0);
                ValidLog.RemoveAt(0);
            }
        }
        public void Render()
        {
            TrailEffectRenderer.Instance.Enqueue(this);
        }

        public void Clear()
        {
            PositionLog.Clear();
            OrientationLog.Clear();
            ValidLog.Clear();

            target = null;
        }
        public void Activate()
        {
        }
        public void Deactivate()
        {
        }

        public void Dispose()
        {
            RadiusCurve.Dispose();
            ColorCurve.Dispose();
        }
    }
}
