﻿using OpenTK.Graphics.OpenGL;
using Penrose.GameObject;
using Penrose.Rendering.Effect.Particle;
using Penrose.Rendering.Shader;
using Penrose.Rendering.Shader.ShaderVar;
using Penrose.Rendering.VertexType;
using Penrose.Unsafe;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

namespace Penrose.Rendering.Effect
{
    public unsafe sealed class ParticleRenderer<T> : IDisposable where T : struct
    {
        public static ParticleRenderer<T> Instance;

        static ParticleRenderer()
        {
        }
        public static void Initialize()
        {
            // Capacity: 10000 Cubes
            Instance = new ParticleRenderer<T>(8 * 10000, 36 * 10000);
            GameSystem.InvokeOnExit += Instance.Dispose;
        }

        private UnmanagedArray<T> vertexArray;
        private UnmanagedArray<Int32> elementArray;

        private VertexBuffer vbo;
        private ElementBuffer ebo;

        private Queue<ParticleBase> particleQueue;

        private readonly int strideByFloat;
        private bool disposed;

        private ParticleRenderer(int initVertexCapacity, int initElementCapacity)
        {
            Debug.Assert(Marshal.SizeOf<T>() % Marshal.SizeOf<Single>() == 0);

            vertexArray = new UnmanagedArray<T>(initVertexCapacity);
            vbo = VertexBuffer.Create<T>(vertexArray.Capacity);
            vbo.Seal();

            elementArray = new UnmanagedArray<Int32>(initElementCapacity);
            ebo = ElementBuffer.Create(elementArray.Capacity);
            ebo.Seal();

            particleQueue = new Queue<ParticleBase>();

            strideByFloat = Marshal.SizeOf<T>() / Marshal.SizeOf<Single>();
            disposed = false;
        }
        ~ParticleRenderer()
        {
            Debug.Assert(disposed == true);
        }

        public void Clear()
        {
            particleQueue.Clear();
        }
        public void Enqueue(ParticleBase pb)
        {
            particleQueue.Enqueue(pb);
        }
        public void Enqueue(IEnumerable<ParticleBase> pbList)
        {
            foreach (ParticleBase pb in pbList)
                particleQueue.Enqueue(pb);
        }
        public void Flush(Transform eyeTransform)
        {
            int totalVertexCount = 0;
            int totalElementCount = 0;

            foreach (ParticleBase pb in particleQueue)
                if (pb.Importable<T>() == true)
                {
                    totalVertexCount += pb.VertexPerParticle;
                    totalElementCount += pb.ElementPerParticle;
                }

            vbo.UnSeal();
            ebo.UnSeal();

            vertexArray.CheckCapacity(totalVertexCount);
            vbo.Resize<T>(vertexArray.Capacity);

            elementArray.CheckCapacity(totalElementCount);
            ebo.Resize(elementArray.Capacity);

            // Fetch Vertex datas
            Single* vp = (Single*)vertexArray.Pointer;
            Int32* ep = (Int32*)elementArray.Pointer;

            long vertexCount = 0;
            foreach (ParticleBase pb in particleQueue)
                if (pb.Importable<T>() == true)
                {
                    pb.Import<T>(ref vp, ref ep, vertexCount);
                    vertexCount = (vp - (Single*)vertexArray.Pointer) / strideByFloat;
                }
            
            // 4 = Marshal.SizeOf<Int32>()
            long elementCount = ep - (Int32*)elementArray.Pointer;

            vbo.Upload<T>(vertexArray.Pointer, 0, vertexCount);
            ebo.Upload(elementArray.Pointer, 0, elementCount);
            vbo.Seal();
            ebo.Seal();

            RenderQueue.Instance.Enqueue(new RenderQueue.TransparentRenderJob()
            {
                VertexBuffer = vbo,
                ElementBuffer = ebo,
                PrimaryPeelingShader = ShaderProvider.ColorPrimaryPeelingShader,
                SecondaryPeelingShader = ShaderProvider.ColorSecondaryPeelingShader,

                ElementCount = elementCount,
                PrimitiveType = PrimitiveType.Triangles,

                VariableManager = new ShaderVariableManager()
                {
                    UniformTransform = GLHelper.CreateTransformMatrix(eyeTransform),
                }
            });
        }

        public void Dispose()
        {
            vertexArray.Dispose();
            elementArray.Dispose();

            vbo.Dispose();
            ebo.Dispose();
            disposed = true;
        }
    }
}
