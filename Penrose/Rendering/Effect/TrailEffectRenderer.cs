﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Penrose.GameObject;
using Penrose.Rendering.Shader;
using Penrose.Rendering.Shader.ShaderVar;
using Penrose.Rendering.VertexType;
using Penrose.Unsafe;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Penrose.Rendering.Effect
{
    public unsafe sealed class TrailEffectRenderer : IDisposable
    {
        public static TrailEffectRenderer Instance;

        static TrailEffectRenderer()
        {
        }
        public static void Initialize()
        {
            TrailEffectRenderer.Instance = new TrailEffectRenderer(10000);
            GameSystem.InvokeOnExit += TrailEffectRenderer.Instance.Dispose;
        }

        private VertexBuffer vbo;
        private ElementBuffer ebo;
        private ShaderBase primaryPeelingShader;
        private ShaderBase secondaryPeelingShader;

        private UnmanagedArray<VertexPositionColor> vertexArray;
        private UnmanagedArray<Int32> elementArray;

        private List<TrailEffect> renderTarget;

        private TrailEffectRenderer(int maxTriangle)
        {
            renderTarget = new List<TrailEffect>();

            vertexArray = new UnmanagedArray<VertexPositionColor>(maxTriangle * 3 / 2);
            elementArray = new UnmanagedArray<Int32>(maxTriangle * 3);
            vbo = VertexBuffer.Create<VertexPositionColor>(vertexArray.Capacity);
            ebo = ElementBuffer.Create(elementArray.Capacity);

            primaryPeelingShader = ShaderProvider.ColorPrimaryPeelingShader;
            secondaryPeelingShader = ShaderProvider.ColorSecondaryPeelingShader;

            vbo.Seal();
            ebo.Seal();
        }

        public void Clear()
        {
            renderTarget.Clear();
        }
        public void Enqueue(TrailEffect eff)
        {
            renderTarget.Add(eff);
        }
        public unsafe void Flush(Transform eyeTransform)
        {
            Debug.Assert(renderTarget.TrueForAll(_ => _.PolygonVertex >= 3));

            // TODO: SHRINK THIS
            int totalVertexCount = renderTarget.Select(_ =>
            {
                if (_.ValidLogCount <= 1)
                    return 0;
                return _.ValidLogCount * _.PolygonVertex;
            }).Sum();
            int totalElementCount = renderTarget.Select(_ =>
            {
                if (_.ValidLogCount <= 1)
                    return 0;
                return 6 * (_.ValidLogCount - 1) * _.PolygonVertex + 6 * (_.PolygonVertex - 2);
            }).Sum();

            if (totalElementCount == 0)
                return;

            vbo.UnSeal();
            ebo.UnSeal();

            vertexArray.CheckCapacity(totalVertexCount);
            elementArray.CheckCapacity(totalElementCount);
            vbo.Resize<VertexPositionColor>(vertexArray.Capacity);
            ebo.Resize(elementArray.Capacity);

            long vertexCount = 0;
            VertexPositionColor* vp = (VertexPositionColor*)vertexArray.Pointer;
            Int32* ep = (Int32*)elementArray.Pointer;

            int lastValid = 0;

            foreach (TrailEffect eff in renderTarget)
            {
                if (eff.ValidLogCount <= 1)
                    continue;

                for (int idx = 0; idx < eff.PositionLog.Count; idx++)
                    if (eff.ValidLog[idx] == true)
                        lastValid = idx;

                int prev = 0;
                AppendVertices(eff, 0, ref vp);

                for (int logIndex = 0; logIndex < eff.PositionLog.Count; logIndex++)
                {
                    if (logIndex == 0 || logIndex == lastValid)
                        continue;
                    if ((eff.PositionLog.Count - logIndex) % eff.SampleInterval != 0)
                        continue;
                    if (eff.ValidLog[logIndex] == false)
                        continue;

                    AppendVertices(eff, logIndex, ref vp);
                    AppendCylinderElements(eff, vertexCount, prev, ref ep);

                    prev++;
                }

                AppendVertices(eff, lastValid, ref vp);
                AppendCylinderElements(eff, vertexCount, prev, ref ep);

                AppendPolygonElementsCCW(eff, vertexCount, 0, ref ep);
                AppendPolygonElementsCW(eff, vertexCount, prev + 1, ref ep);

                // 28 = Marshal.SizeOf<VertexPositionColor>()
                vertexCount = vp - (VertexPositionColor*)vertexArray.Pointer;
            }

            vbo.Upload<VertexPositionColor>(vertexArray.Pointer, 0, vertexCount);
            ebo.Upload(elementArray.Pointer, 0, ep - (Int32*)elementArray.Pointer);

            vbo.Seal();
            ebo.Seal();

            RenderQueue.Instance.Enqueue(new RenderQueue.TransparentRenderJob()
            {
                VertexBuffer = vbo,
                ElementBuffer = ebo,
                PrimaryPeelingShader = primaryPeelingShader,
                SecondaryPeelingShader = secondaryPeelingShader,

                ElementCount = (Int32)(ep - (Int32*)elementArray.Pointer),
                PrimitiveType = PrimitiveType.Triangles,

                VariableManager = new ShaderVariableManager()
                {
                    UniformTransform = GLHelper.CreateTransformMatrix(eyeTransform),
                },
            });
        }

        private void AppendVertices(TrailEffect eff, int index, ref VertexPositionColor* vp)
        {
            // Vertex positioning order:
            // Counter-clockwise
            //  0------------- 2pi > theta
            //  |
            //  |  1->2->3->4->1
            //  |  v  v  v  v  v 
            //  |  5->6->7->8->5
            //  |
            //  v  -z

            Vector3 unitX, unitY, basePosition;

            eff.RadiusCurve.GetValue(eff.PositionLog.Count - index, out float radius);
            eff.ColorCurve.GetValue(eff.PositionLog.Count - index, out Color4 color);

            // Maybe can do some optimization because we multiply quats with x hat and y hat
            unitX = eff.OrientationLog[index] * (radius * Vector3.UnitX);
            unitY = eff.OrientationLog[index] * (radius * Vector3.UnitY);
            basePosition = eff.PositionLog[index];

            for (int polygonIndex = 0; polygonIndex < eff.PolygonVertex; polygonIndex++)
            {
                FastMath.GetNPiOverM(2 * polygonIndex, eff.PolygonVertex, out float sin, out float cos);

                Vector3.Multiply(ref unitX, cos, out Vector3 multipliedUnitX);
                Vector3.Multiply(ref unitY, sin, out Vector3 multipliedUnitY);
                Vector3.Add(ref basePosition, ref multipliedUnitX, out Vector3 tempVector);
                Vector3.Add(ref tempVector, ref multipliedUnitY, out vp->Position);

                vp->Color = color;

                vp++;
            }
        }
        private void AppendCylinderElements(TrailEffect eff, long indexBase, int from, ref Int32* ep)
        {
            // a -- b
            // |    |
            // c -- d
            // Mesh winding order: a->c->d, b->a->d

            int temp, a, b, c, d;
            temp = (int)indexBase + from * eff.PolygonVertex;

            for (int polygonIndex = 0; polygonIndex < eff.PolygonVertex; polygonIndex++)
            {
                a = temp + polygonIndex;
                b = temp + ((polygonIndex + 1) % eff.PolygonVertex);
                c = a + eff.PolygonVertex;
                d = b + eff.PolygonVertex;

                *(ep++) = a;
                *(ep++) = c;
                *(ep++) = d;

                *(ep++) = b;
                *(ep++) = a;
                *(ep++) = d;
            }
        }
        private void AppendPolygonElementsCCW(TrailEffect eff, long indexBase, int from, ref Int32* ep)
        {
            int b = (int)indexBase + from * eff.PolygonVertex;

            for (int polygonIndex = 2; polygonIndex < eff.PolygonVertex; polygonIndex++)
            {
                *(ep++) = b + 0;
                *(ep++) = b + polygonIndex - 1;
                *(ep++) = b + polygonIndex;
            }
        }
        private void AppendPolygonElementsCW(TrailEffect eff, long indexBase, int from, ref Int32* ep)
        {
            int b = (int)indexBase + from * eff.PolygonVertex;

            for (int polygonIndex = 2; polygonIndex < eff.PolygonVertex; polygonIndex++)
            {
                *(ep++) = b + 0;
                *(ep++) = b + polygonIndex;
                *(ep++) = b + polygonIndex - 1;
            }
        }

        public void Dispose()
        {
            vertexArray.Dispose();
            elementArray.Dispose();

            vbo.Dispose();
            ebo.Dispose();
        }
    }
}
