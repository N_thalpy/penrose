﻿using OpenTK;
using Penrose.Pool;

namespace Penrose.Rendering.Effect.Particle.Impl
{
    public sealed class LinearMovingColoredCube : ColoredCube, IPoolable
    {
        private Vector3 velocity;
        private float lifeTime;

        public LinearMovingColoredCube()
        {
        }
        public void Initialize(Vector3 pos, Vector3 scale, Quaternion rot, Vector3 velocity, float lifeTime)
        {
            transform.Position = pos;
            transform.Scale = scale;
            transform.Orientation = rot;

            IsAlive = true;
            this.velocity = velocity;
            this.lifeTime = lifeTime;
        }

        public override void Update(float deltaTime)
        {
            lifeTime -= deltaTime;
            transform.Position += velocity * deltaTime;

            if (lifeTime <= 0)
                IsAlive = false;
        }

        public void Activate()
        {
            transform.Clear();
            IsAlive = true;
        }
        public void Clear()
        {
        }
        public void Deactivate()
        {
        }
    }
}
