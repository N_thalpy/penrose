﻿using OpenTK;
using OpenTK.Graphics;
using Penrose.GameObject;
using Penrose.Rendering;
using Penrose.Rendering.VertexType;
using System;

namespace Penrose.Rendering.Effect.Particle
{
    public abstract class ColoredCube : ParticleBase
    {
        private static WavefrontObject model;

        static ColoredCube()
        {
            model = WavefrontObjectProvider.Cube;
        }

        protected Transform transform;
        public Color4 Color;

        protected ColoredCube()
            : base(ColoredCube.model.Vertices.Length, ColoredCube.model.MeshElement[0].Length)
        {
            transform = new Transform();

            // Traditionally, magenta means unallocated color
            Color = Color4.Magenta;
        }

        public override bool Importable<T>()
        {
            return typeof(T) == typeof(VertexPositionColor);
        }
        public unsafe override void Import<T>(ref Single* vertex, ref Int32* index, long indexOffset)
        {
            if (typeof(T) == typeof(VertexPositionColor))
            {
                VertexPositionColor* vpcPointer = (VertexPositionColor*)vertex;
                Matrix4 mat = transform.TransformMatrix;

                for (int idx = 0; idx < ColoredCube.model.Vertices.Length; idx++)
                {
                    VertexPositionTextureNormal vptn = model.Vertices[idx];
                    
                    Vector3.TransformPerspective(ref vptn.Position, ref mat, out vpcPointer->Position);
                    vpcPointer->Color = Color;
                    vpcPointer++;
                }
                for (int idx = 0; idx < ColoredCube.model.MeshElement[0].Length; idx++)
                    *(index++) = (int)indexOffset + ColoredCube.model.MeshElement[0][idx];

                // 7 = Marshal.SizeOf<VertexPositionColor>() / Marshal.SizeOf<Int32>()
                vertex += ColoredCube.model.Vertices.Length * 7;

                return;
            }

            throw new ArgumentException();
        }
    }
}
