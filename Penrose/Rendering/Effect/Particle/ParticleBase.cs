﻿using System;

namespace Penrose.Rendering.Effect.Particle
{
    public abstract class ParticleBase
    {
        public readonly int VertexPerParticle;
        public readonly int ElementPerParticle;

        public bool IsAlive
        {
            get;
            protected set;
        }
        
        protected ParticleBase(int vertexPerParticle, int elementPerParticle)
        {
            VertexPerParticle = vertexPerParticle;
            ElementPerParticle = elementPerParticle;
        }

        public abstract void Update(float deltaTime);

        public abstract bool Importable<T>();
        public unsafe abstract void Import<T>(ref Single* vertex, ref Int32* index, long indexOffset);
    }
}
