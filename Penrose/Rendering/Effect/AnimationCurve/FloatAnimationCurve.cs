﻿using Penrose.Unsafe;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Penrose.Rendering.Effect.AnimationCurve
{
    public sealed class FloatAnimationCurve : IDisposable
    {
        private BlendDelegate<float> blend;
        private List<KeyFrame<float>> frames;

        private UnmanagedArray<float> precomputedTable;

        public FloatAnimationCurve()
            : this(BlendFactory<float>.LinearBlend)
        {

        }
        public FloatAnimationCurve(BlendDelegate<float> blend)
        {
            this.blend = blend;
            this.frames = new List<KeyFrame<float>>();
        }

        public void Clear()
        {
            frames.Clear();
        }
        public void AddKeyFrame(KeyFrame<float> keyFrame)
        {
            frames.Add(keyFrame);
            frames = frames.OrderBy(_ => _.Time).ToList();
        }

        public unsafe void PreCompute()
        {
            if (precomputedTable != null)
                precomputedTable.Dispose();
            precomputedTable = new UnmanagedArray<float>(1 + (int)(GameSystem.TargetFPS * frames[frames.Count - 1].Time));

            float time = 0;
            Single* p = (Single*)precomputedTable.Pointer;
            for (int idx = 0; idx < precomputedTable.Capacity; idx++)
            {
                EvaluateValue(time, out *(p++));
                time += GameSystem.TargetDeltaTime;
            }
        }

        private void EvaluateValue(float time, out float value)
        {
            Debug.Assert(frames.Count != 0);

            if (time <= frames[0].Time)
            {
                value = frames[0].Data;
                return;
            }

            if (time >= frames[frames.Count - 1].Time)
            {
                value = frames[frames.Count - 1].Data;
                return;
            }

            for (int i = 0; i < frames.Count - 1; i++)
                if (time < frames[i + 1].Time)
                {
                    KeyFrame<float> cur = frames[i];
                    KeyFrame<float> nex = frames[i + 1];
                    value = blend.Invoke(ref cur.Data, ref nex.Data, (time - cur.Time) / (nex.Time - cur.Time));
                    return;
                }

            throw new ArgumentException();
        }

        public unsafe void GetValue(int frame, out float value)
        {
            if (frame >= precomputedTable.Capacity)
                frame = precomputedTable.Capacity - 1;
            value = *((Single*)precomputedTable.Pointer + frame);
        }

        public void Dispose()
        {
            if (precomputedTable != null)
                precomputedTable.Dispose();
        }
    }
}
