﻿using OpenTK;
using Penrose.Unsafe;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Penrose.Rendering.Effect.AnimationCurve
{
    public sealed class Vector3AnimationCurve : IDisposable
    {
        private BlendDelegate<Vector3> blend;
        private List<KeyFrame<Vector3>> frames;

        private UnmanagedArray<Vector3> precomputedTable;

        public Vector3AnimationCurve()
            : this(BlendFactory<Vector3>.LinearBlend)
        {

        }
        public Vector3AnimationCurve(BlendDelegate<Vector3> blend)
        {
            this.blend = blend;
            this.frames = new List<KeyFrame<Vector3>>();
        }

        public void Clear()
        {
            frames.Clear();
        }
        public void AddKeyFrame(KeyFrame<Vector3> keyFrame)
        {
            frames.Add(keyFrame);
            frames = frames.OrderBy(_ => _.Time).ToList();
        }

        public unsafe void PreCompute()
        {
            if (precomputedTable != null)
                precomputedTable.Dispose();
            precomputedTable = new UnmanagedArray<Vector3>(1 + (int)(GameSystem.TargetFPS * frames[frames.Count - 1].Time));

            float time = 0;
            Vector3* p = (Vector3*)precomputedTable.Pointer;
            for (int idx = 0; idx < precomputedTable.Capacity; idx++)
            {
                EvaluateValue(time, out *(p++));
                time += GameSystem.TargetDeltaTime;
            }
        }

        private void EvaluateValue(float time, out Vector3 value)
        {
            Debug.Assert(frames.Count != 0);

            if (time <= frames[0].Time)
            {
                value = frames[0].Data;
                return;
            }

            if (time >= frames[frames.Count - 1].Time)
            {
                value = frames[frames.Count - 1].Data;
                return;
            }

            for (int i = 0; i < frames.Count - 1; i++)
                if (time < frames[i + 1].Time)
                {
                    KeyFrame<Vector3> cur = frames[i];
                    KeyFrame<Vector3> nex = frames[i + 1];
                    value = blend.Invoke(ref cur.Data, ref nex.Data, (time - cur.Time) / (nex.Time - cur.Time));
                    return;
                }

            throw new ArgumentException();
        }

        public unsafe void GetValue(int frame, out Vector3 value)
        {
            if (frame >= precomputedTable.Capacity)
                frame = precomputedTable.Capacity - 1;
            value = *((Vector3*)precomputedTable.Pointer + frame);
        }

        public void Dispose()
        {
            if (precomputedTable != null)
                precomputedTable.Dispose();
        }
    }
}
