﻿namespace Penrose.Rendering.Effect.AnimationCurve
{
    public struct KeyFrame<T>
    {
        public T Data;
        public float Time;

        public KeyFrame(T data, float time)
        {
            Data = data;
            Time = time;
        }
    }
}
