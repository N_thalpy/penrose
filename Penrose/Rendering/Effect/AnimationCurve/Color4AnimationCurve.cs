﻿using OpenTK.Graphics;
using Penrose.Unsafe;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Penrose.Rendering.Effect.AnimationCurve
{
    public sealed class Color4AnimationCurve : IDisposable
    {
        private BlendDelegate<Color4> blend;
        private List<KeyFrame<Color4>> frames;

        private UnmanagedArray<Color4> precomputedTable;

        public Color4AnimationCurve()
            : this(BlendFactory<Color4>.LinearBlend)
        {

        }
        public Color4AnimationCurve(BlendDelegate<Color4> blend)
        {
            this.blend = blend;
            this.frames = new List<KeyFrame<Color4>>();
        }

        public void Clear()
        {
            frames.Clear();
        }
        public void AddKeyFrame(KeyFrame<Color4> keyFrame)
        {
            frames.Add(keyFrame);
            frames = frames.OrderBy(_ => _.Time).ToList();
        }

        public unsafe void PreCompute()
        {
            if (precomputedTable != null)
                precomputedTable.Dispose();
            precomputedTable = new UnmanagedArray<Color4>(1 + (int)(GameSystem.TargetFPS * frames[frames.Count - 1].Time));

            float time = 0;
            Color4* p = (Color4*)precomputedTable.Pointer;
            for (int idx = 0; idx < precomputedTable.Capacity; idx++)
            {
                EvaluateValue(time, out *(p++));
                time += GameSystem.TargetDeltaTime;
            }
        }

        private void EvaluateValue(float time, out Color4 value)
        {
            Debug.Assert(frames.Count != 0);

            if (time <= frames[0].Time)
            {
                value = frames[0].Data;
                return;
            }

            if (time >= frames[frames.Count - 1].Time)
            {
                value = frames[frames.Count - 1].Data;
                return;
            }

            for (int i = 0; i < frames.Count - 1; i++)
                if (time < frames[i + 1].Time)
                {
                    KeyFrame<Color4> cur = frames[i];
                    KeyFrame<Color4> nex = frames[i + 1];
                    value = blend.Invoke(ref cur.Data, ref nex.Data, (time - cur.Time) / (nex.Time - cur.Time));
                    return;
                }

            throw new ArgumentException();
        }

        public unsafe void GetValue(int frame, out Color4 value)
        {
            if (frame >= precomputedTable.Capacity)
                frame = precomputedTable.Capacity - 1;
            value = *((Color4*)precomputedTable.Pointer + frame);
        }

        public void Dispose()
        {
            if (precomputedTable != null)
                precomputedTable.Dispose();
        }
    }
}
