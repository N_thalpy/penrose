﻿using Penrose.Unsafe;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

namespace Penrose.Rendering.Effect.AnimationCurve
{
    // If have to optimize this, make FloatAnimationCurve, Vector3AnimationCurve, Color4AnimationCurve Instead of using C# generic
    // Will reduce function call overheads
    public sealed class GenericAnimationCurve<T> : IDisposable where T : struct
    {
        private BlendDelegate<T> blend;
        private List<KeyFrame<T>> frames;

        private UnmanagedArray<T> precomputedTable;
        private int singleBasedStride;

        public GenericAnimationCurve()
            : this(BlendFactory<T>.LinearBlend, new List<KeyFrame<T>>())
        {
        }
        public GenericAnimationCurve(BlendDelegate<T> blend)
            : this(blend, new List<KeyFrame<T>>())
        {
        }
        public GenericAnimationCurve(IEnumerable<KeyFrame<T>> keyframes)
            : this(BlendFactory<T>.LinearBlend, keyframes)
        {
        }
        public GenericAnimationCurve(BlendDelegate<T> blend, IEnumerable<KeyFrame<T>> frames)
        {
            Debug.Assert(Marshal.SizeOf<T>() % Marshal.SizeOf<Single>() == 0);

            this.blend = blend;
            this.frames = frames.OrderBy(_ => _.Time).ToList();

            singleBasedStride = Marshal.SizeOf<T>() / Marshal.SizeOf<Single>();
        }

        public void Clear()
        {
            frames.Clear();
        }
        public void AddKeyFrame(KeyFrame<T> keyFrame)
        {
            frames.Add(keyFrame);
            frames = frames.OrderBy(_ => _.Time).ToList();
        }

        public unsafe void PreCompute()
        {
            if (precomputedTable != null)
                precomputedTable.Dispose();
            precomputedTable = new UnmanagedArray<T>(1 + (int)(GameSystem.TargetFPS * frames[frames.Count - 1].Time));

            float time = 0;
            Single* p = (Single*)precomputedTable.Pointer;
            for (int idx = 0; idx < precomputedTable.Capacity; idx++)
            {
                T val = EvaluateValue(time);
                Marshal.StructureToPtr(val, new IntPtr(p), false);

                time += 1 * GameSystem.TargetDeltaTime;
                p += Marshal.SizeOf<T>() / Marshal.SizeOf<Single>();
            }
        }

        private T EvaluateValue(float time)
        {
            Debug.Assert(frames.Count != 0);

            if (time <= frames[0].Time)
                return frames[0].Data;

            if (time >= frames[frames.Count - 1].Time)
                return frames[frames.Count - 1].Data;

            for (int i = 0; i < frames.Count - 1; i++)
                if (time < frames[i + 1].Time)
                {
                    KeyFrame<T> cur = frames[i];
                    KeyFrame<T> nex = frames[i + 1];
                    return blend.Invoke(ref cur.Data, ref nex.Data, (time - cur.Time) / (nex.Time - cur.Time));
                }

            throw new ArgumentException();
        }

        public unsafe void GetValue(int frame, out T value)
        {
            if (frame >= precomputedTable.Capacity)
                frame = precomputedTable.Capacity - 1;
            value = Marshal.PtrToStructure<T>(new IntPtr((Single*)precomputedTable.Pointer + frame * singleBasedStride));
        }

        public void Dispose()
        {
            if (precomputedTable != null)
                precomputedTable.Dispose();
        }
    }
}
