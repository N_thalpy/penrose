﻿using OpenTK;
using OpenTK.Graphics;
using System;

namespace Penrose.Rendering.Effect.AnimationCurve
{
    public delegate T BlendDelegate<T>(ref T a, ref T b, float rate);
    public static class BlendFactory<U> where U : struct
    {
        public readonly static BlendDelegate<U> LinearBlend;

        static BlendFactory()
        {
            if (typeof(U) == typeof(Single))
                LinearBlend = (BlendDelegate<U>)(Object)(new BlendDelegate<Single>(
                    (ref Single a, ref Single b, float rate) =>
                    {
                        return a + rate * (b - a);
                    }));

            if (typeof(U) == typeof(Color4))
                LinearBlend = (BlendDelegate<U>)(Object)(new BlendDelegate<Color4>(
                    (ref Color4 a, ref Color4 b, float rate) =>
                    {
                        return new Color4(
                            a.R + rate * (b.R - a.R),
                            a.G + rate * (b.G - a.G),
                            a.B + rate * (b.B - a.B),
                            a.A + rate * (b.A - a.A));
                    }));

            if (typeof(U) == typeof(Vector3))
                LinearBlend = (BlendDelegate<U>)(Object)(new BlendDelegate<Vector3>(
                    (ref Vector3 a, ref Vector3 b, float rate) =>
                    {
                        return a + rate * (b - a);
                    }));
        }
    }
}
