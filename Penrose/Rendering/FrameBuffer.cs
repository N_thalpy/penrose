﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;

namespace Penrose.Rendering
{
    public sealed class FrameBuffer : IDisposable
    {
        private static Stack<int> frameBufferStack;
        private static Stack<Rectangle> viewportStack;

        public int Width
        {
            get;
            private set;
        }
        public int Height
        {
            get;
            private set;
        }

        private int fbo;
        public Texture DepthBuffer;
        public Texture ColorBuffer;

        static FrameBuffer()
        {
            frameBufferStack = new Stack<int>();
            viewportStack = new Stack<Rectangle>();
        }

        public static void PushFrameBuffer()
        {
            int fbo = GL.GetInteger(GetPName.FramebufferBinding);
            frameBufferStack.Push(fbo);

            int[] viewport = new int[4];
            GL.GetInteger(GetPName.Viewport, viewport);
            viewportStack.Push(new Rectangle(viewport[0], viewport[1], viewport[2], viewport[3]));
        }
        public static void PopFrameBuffer(out int fbo, out Rectangle viewport)
        {
            fbo = frameBufferStack.Pop();
            viewport = viewportStack.Pop();
        }

        [CanRunOn(ThreadType.All)]
        private FrameBuffer(int fbo, int depthBuffer, int colorBuffer, int width, int height)
        {
            this.fbo = fbo;

            Width = width;
            Height = height;

            DepthBuffer = Texture.CreateFromTextureHandle(depthBuffer, width, height, TextureMinFilter.Linear, TextureMagFilter.Linear, TextureWrapMode.Clamp, TextureWrapMode.Clamp);
            ColorBuffer = Texture.CreateFromTextureHandle(colorBuffer, width, height, TextureMinFilter.Linear, TextureMagFilter.Linear, TextureWrapMode.Clamp, TextureWrapMode.Clamp);
        }
        ~FrameBuffer()
        {
            Debug.Assert(fbo == 0);
        }

        public static FrameBuffer Create()
        {
            int[] viewport = new int[4];
            GL.GetInteger(GetPName.Viewport, viewport);
            return FrameBuffer.Create(viewport[2], viewport[3]);
        }
        [CanRunOn(ThreadType.All)]
#warning todo: creating framebuffer is very slow; maybe need scheduling?
        public static FrameBuffer Create(int width, int height)
        {
            int fbo = 0;
            int depthBuffer = 0;
            int colorBuffer = 0;

            GameSystem.RunOnRenderThread(() =>
            {
                GLHelper.EnterGLSection();
                
                fbo = GL.GenFramebuffer();
                depthBuffer = GL.GenTexture();
                colorBuffer = GL.GenTexture();

                FrameBuffer.PushFrameBuffer();
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo);
                GL.Viewport(0, 0, width, height);

                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, depthBuffer);
                GL.TexImage2D(
                    TextureTarget.Texture2D,
                    0,
                    PixelInternalFormat.DepthComponent32,
                    width,
                    height,
                    0,
                    PixelFormat.DepthComponent,
                    PixelType.UnsignedByte,
                    IntPtr.Zero);
                GL.FramebufferTexture2D(
                    FramebufferTarget.Framebuffer,
                    FramebufferAttachment.DepthAttachment,
                    TextureTarget.Texture2D,
                    depthBuffer,
                    0);

                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

                GL.BindTexture(TextureTarget.Texture2D, colorBuffer);
                GL.TexImage2D(
                    TextureTarget.Texture2D,
                    0,
                    PixelInternalFormat.Rgba,
                    width,
                    height,
                    0,
                    PixelFormat.Bgra,
                    PixelType.UnsignedByte,
                    IntPtr.Zero);
                GL.FramebufferTexture2D(
                    FramebufferTarget.Framebuffer,
                    FramebufferAttachment.ColorAttachment0,
                    TextureTarget.Texture2D,
                    colorBuffer,
                    0);

                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

                FramebufferErrorCode errorCode = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
                if (errorCode != FramebufferErrorCode.FramebufferComplete)
                    throw new InvalidOperationException();

                int prevFbo;
                Rectangle viewport;
                FrameBuffer.PopFrameBuffer(out prevFbo, out viewport);

                GL.BindFramebuffer(FramebufferTarget.Framebuffer, prevFbo);
                GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, 0);
                GL.BindTexture(TextureTarget.Texture2D, 0);
                GL.Viewport(viewport);

                GLHelper.ExitGLSection();
            }, "FBO Allocation");

            return new FrameBuffer(fbo, depthBuffer, colorBuffer, width, height);
        }
        
        [CanRunOn(ThreadType.MainThread)]
        public void BindFrameBuffer()
        {
            ThreadHelper.CheckCallerThread();

            Debug.Assert(fbo != 0);

            GLHelper.EnterGLSection();

            FrameBuffer.PushFrameBuffer();

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo);
            GL.Viewport(0, 0, Width, Height);

            GLHelper.ExitGLSection();
        }
        [CanRunOn(ThreadType.MainThread)]
        public void UnbindFrameBuffer()
        {
            ThreadHelper.CheckCallerThread();

            GLHelper.EnterGLSection();

            Debug.Assert(frameBufferStack.Count != 0);
            Debug.Assert(viewportStack.Count != 0);

            int prevFbo;
            Rectangle viewport;
            FrameBuffer.PopFrameBuffer(out prevFbo, out viewport);

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, prevFbo);
            GL.Viewport(viewport);

            GLHelper.ExitGLSection();
        }
        [CanRunOn(ThreadType.MainThread)]
        public void CreateMipMap()
        {
            ThreadHelper.CheckCallerThread();

            GLHelper.EnterGLSection();

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, ColorBuffer.TextureHandle);
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
            GL.BindTexture(TextureTarget.Texture2D, 0);

            GLHelper.ExitGLSection();
        }

        [CanRunOn(ThreadType.MainThread)]
        public void Dispose()
        {
            ThreadHelper.CheckCallerThread();

            Debug.Assert(fbo != 0);
            GLHelper.EnterGLSection();

            GL.DeleteFramebuffer(fbo);

            GLHelper.ExitGLSection();
            fbo = 0;

            ColorBuffer.Dispose();
            DepthBuffer.Dispose();
        }
    }
}
