﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Penrose.GameObject;
using Penrose.Rendering.Shader.ShaderVar;
using Penrose.Rendering.VertexType;
using System;

namespace Penrose.Rendering.Renderer
{
    public sealed class StaticObjectRenderer : IDisposable
    {
        public int MaxModel;
        public bool Loaded
        {
            get
            {
                if (vbo == null)
                    return false;
                return vbo.Loaded;
            }
        }

        private VertexBuffer vbo;
        private ElementBuffer[] eboList;

        private WavefrontObject model;

        private int offset;

        public StaticObjectRenderer(WavefrontObject model, int modelCount)
        {
            MaxModel = modelCount;
            offset = 0;

            this.model = model;
            eboList = new ElementBuffer[model.MaterialList.Count];

            GameSystem.RunOnRenderThread(() =>
            {
                vbo = VertexBuffer.Create<VertexPositionColorNormal>(model.Vertices.Length * modelCount);
            }, "StaticObjectRenderer: Generating VBO");

            unsafe
            {
                int eboDataLength = 10000;
                int* eboData = stackalloc int[eboDataLength];

                for (int matCount = 0; matCount < model.MaterialList.Count; matCount++)
                {
                    int eCount = 0;
                    int offset = 0;

                    GameSystem.RunOnRenderThread(() =>
                    {
                        eboList[matCount] = ElementBuffer.Create(model.MeshElement[matCount].Length * modelCount);
                    }, "StaticObjectRenderer: Generating EBO");

                    for (int objCount = 0; objCount < modelCount; objCount++)
                        foreach (int index in model.MeshElement[matCount])
                        {
                            eboData[eCount % eboDataLength] = index + objCount * model.Vertices.Length;
                            if ((eCount + 1) % eboDataLength == 0)
                            {
                                GameSystem.RunOnRenderThread(() =>
                                {
                                    eboList[matCount].Upload(eboData, offset, eboDataLength);
                                    offset += sizeof(int) * eboDataLength;
                                }, "StaticObjectRenderer: Uploading EBO");
                            }

                            eCount++;
                        }
                }
            }
        }

        public unsafe void PushObject(ObjectBase[] objList)
        {
            VertexPositionColorNormal* vptcArray = stackalloc VertexPositionColorNormal[objList.Length * model.Vertices.Length];
            for (int i = 0; i < objList.Length; i++)
                for (int vert = 0; vert < model.Vertices.Length; vert++)
                {
                    int index = i * model.Vertices.Length + vert;
                    vptcArray[index].Position = Vector3.TransformPerspective(model.Vertices[vert].Position, objList[i].Transform.TransformMatrix);
                    vptcArray[index].Color = objList[i].Color;
                    vptcArray[index].Normal = Vector3.Normalize(objList[i].Transform.Orientation * model.Vertices[vert].Normal);
                }

            vbo.Upload<VertexPositionColorNormal>(vptcArray, offset, objList.Length * model.Vertices.Length);
            offset += objList.Length * model.Vertices.Length * BlittableValueType<VertexPositionColorNormal>.Stride;
        }
        public void Seal()
        {
            vbo.Seal();
            for (int i = 0; i < eboList.Length; i++)
                eboList[i].Seal();
        }

        public void Render(Matrix4 mvp, ShaderBase shader)
        {
            for (int idx = 0; idx < model.MaterialList.Count; idx++)
            {
                RenderQueue.Instance.Enqueue(new RenderQueue.OpaqueRenderJob()
                {
                    VertexBuffer = vbo,
                    ElementBuffer = eboList[idx],
                    Shader = shader,

                    ElementCount = eboList[idx].Count,
                    PrimitiveType = PrimitiveType.Triangles,

                    VariableManager = new ShaderVariableManager()
                    {
                        UniformTransform = mvp,
                    }
                });
            }
        }

        public void Dispose()
        {
            if (vbo != null)
                vbo.Dispose();

            if (eboList != null)
                foreach (ElementBuffer ebo in eboList)
                    ebo.Dispose();
        }
    }
}
