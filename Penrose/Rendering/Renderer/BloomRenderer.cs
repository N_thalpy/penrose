﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Penrose.Rendering.Shader;
using Penrose.Rendering.Shader.ShaderVar;
using Penrose.Rendering.VertexType;
using System;

namespace Penrose.Rendering.Renderer
{
    public sealed class BloomRenderer : IDisposable
    {
        private VertexBuffer vbo;
        private ElementBuffer ebo;
        private FrameBuffer fbo;

        private ShaderBase shader;

        public BloomRenderer()
        {
            // VBO Order:
            // 0 <---- 3
            // |       ^
            // v       |
            // 1 ----> 2
            
            GameSystem.RunOnRenderThread(() =>
            {
                vbo = VertexBuffer.Create(new VertexPositionTexture[]
                {
                    new VertexPositionTexture(-1, 1, 0, GLHelper.UVLeftUpper),
                    new VertexPositionTexture(-1, -1, 0, GLHelper.UVLeftLower),
                    new VertexPositionTexture(1, -1, 0, GLHelper.UVRightLower),
                    new VertexPositionTexture(1, 1, 0, GLHelper.UVRightUpper),
                });
            }, "BloomRenderer: Generating VBO");

            GameSystem.RunOnRenderThread(() =>
            {
                ebo = ElementBuffer.Create(new int[] { 0, 1, 2, 3 });
            }, "BloomRenderer: Generating EBO");

            GameSystem.RunOnRenderThread(() =>
            {
                fbo = FrameBuffer.Create();
            }, "BloonRenderer: Generating FBO");

            shader = ShaderProvider.BloomShader;
        }

        public void Render(Action renderAction)
        {
            fbo.BindFrameBuffer();
            GL.ClearColor(new Color4(0, 0, 0, 0));
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            renderAction.Invoke();

            fbo.UnbindFrameBuffer();
            GL.ClearColor(new Color4(0, 0, 0, 0));
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            vbo.Bind(shader);
            ebo.Bind();

            Matrix4 mvp = Matrix4.Identity;
            
            shader.Bind();
            shader.BindUniformMatrix4(SV.UniformTransform, ref mvp);
            shader.BindTexture(SV.UniformTexture, fbo.ColorBuffer);

            GL.DrawElements(PrimitiveType.Quads, 4, DrawElementsType.UnsignedInt, 0);
        }

        public void Dispose()
        {
            fbo.Dispose();
            vbo.Dispose();
            ebo.Dispose();
        }
    }
}
