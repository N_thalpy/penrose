﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Penrose.GameObject;
using Penrose.Physics;
using Penrose.Rendering.Shader;
using Penrose.Rendering.Shader.ShaderVar;
using Penrose.Rendering.VertexType;
using Penrose.Unsafe;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Penrose.Rendering.Renderer
{
    public unsafe sealed class ColliderRenderer : IDisposable
    {
        public static ColliderRenderer Instance;

        static ColliderRenderer()
        {
        }
        public static void Initialize()
        {
            Instance = new ColliderRenderer(3);
            GameSystem.InvokeOnExit += Instance.Dispose;
        }

        private int detailLevel;
        private UnmanagedArray<VertexPositionColor> vertexArray;
        private UnmanagedArray<Int32> elementArray;

        private VertexBuffer vbo;
        private ElementBuffer ebo;

        private bool disposed;

        private List<SphericalCollider> colliderList;

        private ColliderRenderer(int detailLevel)
        {
            this.detailLevel = detailLevel;

            vertexArray = new UnmanagedArray<VertexPositionColor>(40000);
            elementArray = new UnmanagedArray<Int32>(40000);

            vbo = VertexBuffer.Create<VertexPositionColor>(40000);
            ebo = ElementBuffer.Create(40000);

            vbo.Seal();
            ebo.Seal();

            colliderList = new List<SphericalCollider>();

            disposed = false;
        }
        ~ColliderRenderer()
        {
            Debug.Assert(disposed == true);
        }

        public void Clear()
        {
            colliderList.Clear();
        }
        public void Enqueue(SphericalCollider c)
        {
            colliderList.Add(c);
        }
        public void Flush(Transform eyeTransform)
        {
            // Can optimize more
            vbo.UnSeal();
            ebo.UnSeal();

            int totalVertexCount = colliderList.Count * (4 * detailLevel * (2 * detailLevel + 1));
            int totalElementCount = colliderList.Count * (32 * detailLevel * detailLevel);

            vertexArray.CheckCapacity(totalVertexCount);
            elementArray.CheckCapacity(totalElementCount);
            vbo.Resize<VertexPositionColor>(vertexArray.Capacity);
            ebo.Resize(elementArray.Capacity);

            VertexPositionColor* vp = (VertexPositionColor*)vertexArray.Pointer;
            Int32* ep = (Int32*)elementArray.Pointer;

            int vertexCount = 0;
            int elementCount = 0;
            foreach (SphericalCollider collider in colliderList)
            {
                // phi : [-pi, pi]
                // theta: [0, 2pi)
                for (int phiIndex = -detailLevel; phiIndex <= detailLevel; phiIndex++)
                    for (int thetaIndex = 0; thetaIndex < 4 * detailLevel; thetaIndex++)
                    {
                        float phi = FastMath.PIOver2 * phiIndex / detailLevel;
                        float theta = FastMath.PIOver2 * thetaIndex / detailLevel;

                        float sinPhi, cosPhi;
                        float sinTheta, cosTheta;
                        FastMath.GetNPiOverM(phiIndex, 2 * detailLevel, out sinPhi, out cosPhi);
                        FastMath.GetNPiOverM(thetaIndex, 2 * detailLevel, out sinTheta, out cosTheta);

                        Vector3 pos = new Vector3(sinTheta * cosPhi, sinTheta * sinPhi, cosTheta) * collider.Radius;
                        vp->Position = collider.Transform.Position + collider.Transform.Orientation * pos;
                        vp->Color = Color4.Green;

                        vp++;
                    }

                for (int phiIndex = 0; phiIndex < 2 * detailLevel; phiIndex++)
                    for (int thetaIndex = 0; thetaIndex < 4 * detailLevel; thetaIndex++)
                    {
                        // a b
                        // c
                        // Winding: a->b a->c

                        // int a = vertexCount + (phiIndex + 0) * 4 * detailLevel + ((thetaIndex + 0) % (4 * detailLevel));
                        // int b = vertexCount + (phiIndex + 0) * 4 * detailLevel + ((thetaIndex + 1) % (4 * detailLevel));
                        // int c = vertexCount + (phiIndex + 1) * 4 * detailLevel + ((thetaIndex + 0) % (4 * detailLevel));

                        int vertexBase = vertexCount + phiIndex * 4 * detailLevel;
                        int a = vertexBase + thetaIndex;
                        int b = vertexBase + ((1 + thetaIndex) % (4 * detailLevel));
                        int c = a + 4 * detailLevel;

                        *(ep++) = a;
                        *(ep++) = b;
                        *(ep++) = a;
                        *(ep++) = c;
                    }

                vertexCount += 4 * detailLevel * (2 * detailLevel + 1);
                elementCount += 32 * detailLevel * detailLevel;
            }

            vbo.Upload<VertexPositionColor>(vertexArray.Pointer, 0, vertexCount);
            ebo.Upload(elementArray.Pointer, 0, elementCount);

            vbo.Seal();
            ebo.Seal();

            RenderQueue.Instance.Enqueue(new RenderQueue.DepthIndependentRenderJob()
            {
                VertexBuffer = vbo,
                ElementBuffer = ebo,
                Shader = ShaderProvider.ColorShader,

                ElementCount = elementCount,
                PrimitiveType = PrimitiveType.Lines,

                VariableManager = new ShaderVariableManager()
                {
                    UniformTransform = GLHelper.CreateTransformMatrix(eyeTransform),
                }
            });
        }

        public void Dispose()
        {
            disposed = true;

            vertexArray.Dispose();
            elementArray.Dispose();

            vbo.Dispose();
            ebo.Dispose();
        }
    }
}
