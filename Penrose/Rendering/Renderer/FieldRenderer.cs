﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Penrose.Field;
using Penrose.Rendering.Shader;
using Penrose.Rendering.Shader.ShaderVar;
using Penrose.Rendering.VertexType;
using System.Collections.Generic;

namespace Penrose.Rendering.Renderer
{
    public sealed class FieldRenderer
    {
        private VertexBuffer vbo;
        private ElementBuffer ebo;
        private FrameBuffer fbo;

        private ShaderBase shader;

        public FieldRenderer()
        {
            // VBO Order:
            // 0 <---- 3
            // |       ^
            // v       |
            // 1 ----> 2

            GameSystem.RunOnRenderThread(() =>
            {
                vbo = VertexBuffer.Create(new VertexPositionTexture[]
                {
                    new VertexPositionTexture(-1, 1, 0, GLHelper.UVLeftUpper),
                    new VertexPositionTexture(-1, -1, 0, GLHelper.UVLeftLower),
                    new VertexPositionTexture(1, -1, 0, GLHelper.UVRightLower),
                    new VertexPositionTexture(1, 1, 0, GLHelper.UVRightUpper),
                });
            }, "FieldRenderer: Generating VBO");

            GameSystem.RunOnRenderThread(() =>
            {
                ebo = ElementBuffer.Create(new int[] { 0, 1, 2, 3 });
            }, "FieldRenderer: Generating EBO");

            GameSystem.RunOnRenderThread(() =>
            {
                fbo = FrameBuffer.Create();
            }, "FieldRenderer: Generating FBO");

            shader = ShaderProvider.BloomShader;
        }

        public void Render(IEnumerable<FieldBase> fields, ref Matrix4 mat)
        {
            GLHelper.CheckGLError();
            GL.Enable(EnableCap.DepthTest);

            fbo.BindFrameBuffer();
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.ClearColor(new Color4(0, 0, 0, 0));
            
            fbo.UnbindFrameBuffer();

            vbo.Bind(shader);
            ebo.Bind();

            shader.Bind();
            Matrix4 m = Matrix4.Identity;
            shader.BindUniformMatrix4(SV.UniformTransform, ref m);
            shader.BindTexture(SV.UniformTexture, fbo.ColorBuffer);

            GL.DrawElements(PrimitiveType.Quads, 4, DrawElementsType.UnsignedInt, 0);

            vbo.Unbind();
            ebo.Unbind();
            shader.Unbind();

            GL.Disable(EnableCap.DepthTest);
            GLHelper.CheckGLError();
        }

        public void Dispose()
        {
            fbo.Dispose();
            vbo.Dispose();
            ebo.Dispose();
        }
    }
}
