﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Penrose.GameObject;
using Penrose.Rendering.Shader;
using Penrose.Rendering.Shader.ShaderVar;
using Penrose.Rendering.VertexType;
using Penrose.Unsafe;
using System;
using System.Collections.Generic;

namespace Penrose.Rendering.Renderer
{
    // Warning: this class is 'quite' slow
    public unsafe sealed class GenericObjectFlatRenderer
    {
        private struct GenericObjectData
        {
            public Transform Transform;
            public Vector3 Offset;
            public WavefrontObject Model;
            public Color4 Color;

            public GenericObjectData(Transform transform, Vector3 offset, WavefrontObject model, Color4 color)
            {
                Transform = transform;
                Offset = offset;
                Model = model;
                Color = color;
            }
        }

        public static GenericObjectFlatRenderer Instance;

        static GenericObjectFlatRenderer()
        {
        }
        public static void Initialize()
        {
            Instance = new GenericObjectFlatRenderer();
            GameSystem.InvokeOnExit += Instance.Dispose;
        }

        private List<GenericObjectData> dataList;

        private UnmanagedArray<VertexPositionColorNormal> vertexArray;
        private UnmanagedArray<Int32> elementArray;
        private VertexBuffer vbo;
        private ElementBuffer ebo;

        public GenericObjectFlatRenderer()
        {
            dataList = new List<GenericObjectData>();

            vertexArray = new UnmanagedArray<VertexPositionColorNormal>(10000);
            elementArray = new UnmanagedArray<Int32>(10000);
            vbo = VertexBuffer.Create<VertexPositionColorNormal>(vertexArray.Capacity);
            ebo = ElementBuffer.Create(elementArray.Capacity);

            vbo.Seal();
            ebo.Seal();
        }

        public void Clear()
        {
            dataList.Clear();
        }
        public void Enqueue(Transform transform, Vector3 offset, WavefrontObject model, Color4 color)
        {
            dataList.Add(new GenericObjectData(transform, offset, model, color));
        }
        public void Flush(Transform eyeTransform)
        {
            int totalVertexCount = 0;
            int totalElementCount = 0;

            foreach (GenericObjectData data in dataList)
            {
                totalVertexCount += data.Model.Vertices.Length;
                totalElementCount += data.Model.MeshElement[0].Length;
            }

            vbo.UnSeal();
            ebo.UnSeal();

            vertexArray.CheckCapacity(totalVertexCount);
            elementArray.CheckCapacity(totalElementCount);
            vbo.Resize<VertexPositionColorNormal>(vertexArray.Capacity);
            ebo.Resize(elementArray.Capacity);

            int vertexCount = 0;
            VertexPositionColorNormal* vp = (VertexPositionColorNormal*)vertexArray.Pointer;
            Int32* ep = (Int32*)elementArray.Pointer;
            foreach (GenericObjectData data in dataList)
            {
                foreach (VertexPositionTextureNormal vptn in data.Model.Vertices)
                {
                    vp->Position = Vector3.TransformPerspective(vptn.Position, data.Transform.TransformMatrix) + data.Transform.Orientation * data.Offset;
                    vp->Normal = vptn.Normal;
                    vp->Color = data.Color;

                    vp++;
                }
                foreach (Int32 index in data.Model.MeshElement[0])
                    *(ep++) = vertexCount + index;

                vertexCount += data.Model.Vertices.Length;
            }

            vbo.Upload<VertexPositionColorNormal>(vertexArray.Pointer, 0, totalVertexCount);
            ebo.Upload(elementArray.Pointer, 0, totalElementCount);
            vbo.Seal();
            ebo.Seal();

            RenderQueue.Instance.Enqueue(new RenderQueue.OpaqueRenderJob()
            {
                VertexBuffer = vbo,
                ElementBuffer = ebo,
                Shader = ShaderProvider.FlatColorShader,

                ElementCount = totalElementCount,
                PrimitiveType = PrimitiveType.Triangles,

                VariableManager = new ShaderVariableManager()
                {
                    UniformTransform = GLHelper.CreateTransformMatrix(eyeTransform),
                }
            });
        }

        private void Dispose()
        {
            vertexArray.Dispose();
            vbo.Dispose();

            elementArray.Dispose();
            ebo.Dispose();
        }
    }
}
