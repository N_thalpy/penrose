﻿using OpenTK;
using Penrose.GameObject;
using System;
using System.Threading;

namespace Penrose.Field
{
    public abstract class FieldBase
    {
        public Matrix4 InitialCameraMatrix;

        public Transform Transform;
        public bool Initialized
        {
            get;
            private set;
        }

        protected readonly Object lockObject;

        protected FieldBase()
        {
            InitialCameraMatrix = Matrix4.Identity;
            Transform = new Transform();
            lockObject = new Object();
            Initialized = false;
        }

        public void Initialize(bool async)
        {
            Initialized = false;

            if (async == true)
            {
                ThreadHelper.SetOffThreadJob(() =>
                {
                    Monitor.Enter(lockObject);

                    try
                    {
                        OnInitialize();
                    }
                    finally
                    {
                        Monitor.Exit(lockObject);
                    }

                    Initialized = true;
                }, "FieldBase.Initialize");
            }
            else
            {
                OnInitialize();
                Initialized = true;

                GLHelper.CheckGLError();
            }
        }
        public void Update(float deltaTime)
        {
            if (Initialized == false)
                return;
            if (Monitor.TryEnter(lockObject) == false)
            {
                if (Initialized == true)
                    Console.WriteLine("Update: Failed to synchronize with field");
                return;
            }

            try
            {
                OnUpdate(deltaTime);
            }
            finally
            {
                Monitor.Exit(lockObject);
            }
        }
        public void Render(Transform eyeTransform)
        {
            if (Initialized == false)
                return;
            if (Monitor.TryEnter(lockObject) == false)
            {
                if (Initialized == true)
                    Console.WriteLine("Render: Failed to synchronize with field");
                return;
            }

            try
            {
                OnRender(eyeTransform);
            }
            finally
            {
                Monitor.Exit(lockObject);
            }
        }

        protected abstract void OnInitialize();
        protected abstract void OnUpdate(float deltaTime);
        protected abstract void OnRender(Transform eyeTransform);
    }
}
