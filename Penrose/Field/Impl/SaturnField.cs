﻿using OpenTK;
using OpenTK.Graphics;
using Penrose.GameObject;
using Penrose.Rendering;
using Penrose.Rendering.Renderer;
using Penrose.Rendering.Shader;
using System;

namespace Penrose.Field.Impl
{
    public sealed class SaturnField : FieldBase, IDisposable
    {
        private BloomRenderer bloom;
        private StaticObjectRenderer[] rendererList;
        ObjectBase[] objects;

        public SaturnField()
            : base()
        {
            InitialCameraMatrix = new Matrix4(
                0.4834842f, -0.02300786f, 0.8750587f, 0f,
                0.8676377f, -0.1199368f, -0.4825379f, 0f,
                0.1160515f, 0.992538f, -0.03802159f, 0f,
                -255054.6f, -71451.23f, -559493.7f, 1f);
        }

        protected override void OnInitialize()
        {
            Random rd = new Random(314159265);
            WavefrontObject obj = WavefrontObjectProvider.Cube;
            rendererList = new StaticObjectRenderer[3];

            bloom = new BloomRenderer();

            rendererList[0] = new StaticObjectRenderer(obj, 50000);
            rendererList[1] = new StaticObjectRenderer(obj, 50000);
            rendererList[2] = new StaticObjectRenderer(obj, 100000);

            int range = 2500;
            objects = new ObjectBase[1000];
            for (int i = 0; i < objects.Length; i++)
                objects[i] = new ObjectBase(obj);

            for (int i = 0; i < rendererList[0].MaxModel; i++)
            {
                objects[i % objects.Length].Clear();
                Vector3 t = new Vector3(rd.Next(-range, range), rd.Next(-range, range), rd.Next(-range, range) / 100);
                t.Normalize();
                t *= (0.7f + 0.4f * rd.NextFloat()) * range;

                objects[i % objects.Length].Transform.Position += t;
                objects[i % objects.Length].Transform.Scale *= 15f * (1 + (rd.NextFloat() - 0.5f) * 0.1f);
                objects[i % objects.Length].Transform.Orientation *=
                    Quaternion.FromAxisAngle(new Vector3(rd.Next(-range, range), rd.Next(-range, range), rd.Next(-range, range)), MathHelper.Pi * rd.NextFloat());

                float v = rd.NextFloat();
                v = 0.2f + 0.6f * v * v;

                t.Normalize();
                objects[i % objects.Length].Color = new Color4(
                    t.X * (rd.NextFloat() * 0.5f + 0.25f),
                    t.Y * (rd.NextFloat() * 0.5f + 0.25f),
                    rd.NextFloat(),
                    1);

                if ((i + 1) % objects.Length == 0)
                    rendererList[0].PushObject(objects);
            }
            rendererList[0].Seal();

            for (int i = 0; i < rendererList[1].MaxModel; i++)
            {
                objects[i % objects.Length].Clear();

                Vector3 t = new Vector3(rd.Next(-range, range), rd.Next(-range, range), rd.Next(-range, range) / 100);
                t.Normalize();
                t = Vector3.TransformPerspective(t, Matrix4.CreateFromAxisAngle(Vector3.UnitX, MathHelper.Pi / 12));
                t *= (1.3f + 0.4f * rd.NextFloat()) * range;

                objects[i % objects.Length].Transform.Position += t;
                objects[i % objects.Length].Transform.Scale *= 15f * (1 + (rd.NextFloat() - 0.5f) * 0.1f);
                objects[i % objects.Length].Transform.Orientation *=
                    Quaternion.FromAxisAngle(new Vector3(rd.Next(-range, range), rd.Next(-range, range), rd.Next(-range, range)), MathHelper.Pi * rd.NextFloat());

                float v = rd.NextFloat();
                v = 0.2f + 0.6f * v * v;

                t.Normalize();
                objects[i % objects.Length].Color = new Color4(
                    (1 - t.X) * (rd.NextFloat() * 0.5f + 0.25f),
                    (1 - t.Y) * (rd.NextFloat() * 0.5f + 0.25f),
                    rd.NextFloat(),
                    1);

                if ((i + 1) % objects.Length == 0)
                    rendererList[1].PushObject(objects);
            }
            rendererList[1].Seal();

            for (int i = 0; i < rendererList[2].MaxModel; i++)
            {
                objects[i % objects.Length].Clear();
                Vector3 t = new Vector3(rd.Next(-range, range), rd.Next(-range, range), rd.Next(-range, range));
                t.Normalize();
                t *= range * (0.1f * rd.NextFloat() + 0.3f);

                objects[i % objects.Length].Transform.Position += t;
                objects[i % objects.Length].Transform.Scale *= 22.5f * (1 + (rd.NextFloat() - 0.5f) * 0.1f);
                objects[i % objects.Length].Transform.Orientation *=
                    Quaternion.FromAxisAngle(new Vector3(rd.Next(-range, range), rd.Next(-range, range), rd.Next(-range, range)), MathHelper.Pi * rd.NextFloat());

                float v = rd.NextFloat();
                v = 0.2f + 0.8f * v * v;

                t.Normalize();
                objects[i % objects.Length].Color = new Color4(1 - v / 8, v / 4, v / 4, 1);

                if ((i + 1) % objects.Length == 0)
                    rendererList[2].PushObject(objects);
            }
            rendererList[2].Seal();
        }

        protected override void OnUpdate(float deltaTime)
        {
        }
        protected override void OnRender(Transform eyeTransform)
        {
            rendererList[0].Render(GLHelper.CreateTransformMatrix(eyeTransform, Transform), ShaderProvider.ColorShader);
            rendererList[1].Render(GLHelper.CreateTransformMatrix(eyeTransform, Transform), ShaderProvider.ColorShader);
            rendererList[2].Render(GLHelper.CreateTransformMatrix(eyeTransform, Transform), ShaderProvider.ColorShader);
        }

        public void Dispose()
        {
            bloom.Dispose();
            foreach (StaticObjectRenderer r in rendererList)
                r.Dispose();
        }
    }
}
