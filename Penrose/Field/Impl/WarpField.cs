﻿using OpenTK;
using OpenTK.Graphics;
using Penrose.GameObject;
using Penrose.Rendering;
using Penrose.Rendering.Renderer;
using Penrose.Rendering.Shader;
using System;

namespace Penrose.Field.Impl
{
    public sealed class WarpField : FieldBase, IDisposable
    {
        private BloomRenderer bloom;
        private StaticObjectRenderer[] rendererList;

        private Matrix4[] rotationTransform;
        private Matrix4 positionTransform;
        private float currentZ;

        private float zWidth;
        private float zVelocity;
        private int repeat;

        private bool repeating;

        public WarpField()
            : base()
        {
            zWidth = 50000;
            zVelocity = 60000;
            repeat = 10;

            InitialCameraMatrix = new Matrix4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1);

            repeating = true;
        }

        protected override void OnInitialize()
        {
            ObjectBase[] objects;
            Random rd = new Random(314159265);
            WavefrontObject obj = WavefrontObjectProvider.Cube;

            bloom = new BloomRenderer();

            rendererList = new StaticObjectRenderer[5];
            for (int i = 0; i < rendererList.Length; i++)
                rendererList[i] = new StaticObjectRenderer(obj, 3000);

            positionTransform = Matrix4.Identity;
            rotationTransform = new Matrix4[rendererList.Length];
            for (int i = 0; i < rotationTransform.Length; i++)
                rotationTransform[i] = Matrix4.Identity;
            
            objects = new ObjectBase[1000];
            
            for (int i = 0; i < objects.Length; i++)
                objects[i] = new ObjectBase(obj);
            
            for (int rIndex = 0; rIndex < rendererList.Length; rIndex++)
            {
                for (int i = 0; i < rendererList[rIndex].MaxModel; i += repeat)
                {
                    float theta = 2 * rd.NextFloat() * MathHelper.Pi;
                    float r = 1000 + 3000 * rd.NextFloat();
                    float v = rd.NextFloat();
                    Color4 c = new Color4(rd.NextFloat(), rd.NextFloat(), rd.NextFloat(), 1);
                    float s = 50 + 30 * rd.NextFloat();

                    for (int j = 0; j < repeat && i + j < rendererList[rIndex].MaxModel; j++)
                    {
                        objects[(i + j) % objects.Length].Clear();
                        objects[(i + j) % objects.Length].Transform.Position +=
                            new Vector3((float)Math.Cos(theta) * r, (float)Math.Sin(theta) * r, -(v + j - repeat / 2) * zWidth);
                        objects[(i + j) % objects.Length].Transform.Scale *= s;
                        objects[(i + j) % objects.Length].Color = c;

                        if ((i + j + 1) % objects.Length == 0)
                            rendererList[rIndex].PushObject(objects);
                    }
                }
                
                rendererList[rIndex].Seal();
            }
        }

        protected override void OnUpdate(float deltaTime)
        {
            positionTransform = positionTransform * Matrix4.CreateTranslation(Vector3.UnitZ * zVelocity * deltaTime);
            currentZ += zVelocity * deltaTime;

            if (repeating == true && currentZ > zWidth)
            {
                currentZ -= zWidth;
                positionTransform = positionTransform * Matrix4.CreateTranslation(-Vector3.UnitZ * zWidth);
            }

            for (int i = 0; i < rotationTransform.Length; i++)
                rotationTransform[i] = rotationTransform[i] * Matrix4.CreateRotationZ((i - (float)rotationTransform.Length / 2) * MathHelper.Pi * deltaTime / 25);
        }
        protected override void OnRender(Transform eyeTransform)
        {
           /* for (int i = 0; i < rendererList.Length; i++)
                rendererList[i].Render(rotationTransform[i] * positionTransform * Transform * mat, ShaderProvider.ColorShader);*/
        }

        public void StopRepeat()
        {
            repeating = false;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
