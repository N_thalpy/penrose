﻿using Penrose.GameObject;
using Penrose.Pool;
using System;

namespace Penrose.Physics
{
    public delegate void CollisionDelegate(SphericalCollider opponent);

    public sealed class SphericalCollider : IPoolable
    {
        public Object Owner;
        public Transform Transform;
        public float Radius;

        public CollisionDelegate Callback;

        public SphericalCollider()
        {
        }
        public void Initialize(Transform transform, float radius, Object owner, CollisionDelegate callback)
        {
            Transform = transform;
            Radius = radius;
            Callback = callback;
            Owner = owner;
        }

        public void Clear()
        {
        }

        public void Activate()
        {
            PhysicsManager.Instance.AddCollider(this);
        }
        public void Deactivate()
        {
            PhysicsManager.Instance.RemoveCollider(this);
        }
    }
}
