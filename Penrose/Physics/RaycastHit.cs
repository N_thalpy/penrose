﻿namespace Penrose.Physics
{
    public struct RaycastHit
    {
        public SphericalCollider Collider;
        public float Distance;

        public RaycastHit(SphericalCollider collider, float distance)
        {
            Collider = collider;
            Distance = distance;
        }
    }
}
