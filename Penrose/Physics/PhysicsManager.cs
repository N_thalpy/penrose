﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Penrose.Physics
{
    public class PhysicsManager
    {
        public static PhysicsManager Instance;

        static PhysicsManager()
        {
        }
        public static void Initialize()
        {
            Instance = new PhysicsManager();
        }

        private List<SphericalCollider> colliders;
        private List<SphericalCollider> dummyColliders;

        private List<RaycastHit> hitList;

        private PhysicsManager()
        {
            colliders = new List<SphericalCollider>();
            dummyColliders = new List<SphericalCollider>();

            hitList = new List<RaycastHit>();
        }

        public void Fire()
        {
            dummyColliders.Clear();
            foreach (SphericalCollider sc in colliders)
                dummyColliders.Add(sc);

            foreach (SphericalCollider lhs in dummyColliders)
                if (lhs.Callback != null)
                    foreach (SphericalCollider rhs in dummyColliders)
                    {
                        float lengthSquared = (lhs.Transform.Position - rhs.Transform.Position).LengthSquared;
                        float radiusSquared = lhs.Radius + rhs.Radius;
                        radiusSquared *= radiusSquared;

                        if (lhs != rhs && lengthSquared <= radiusSquared)
                            lhs.Callback(rhs);
                    }
        }

        // TODO: Control memory allocation about array
        public RaycastHit[] Raycast(Ray r)
        {
            return Raycast(r, Single.PositiveInfinity);
        }
        public RaycastHit[] Raycast(Ray r, float maxDist)
        {
            hitList.Clear();

            float originSum = r.Origin.X + r.Origin.Y + r.Origin.Z;
            float directionSum = r.Direction.X + r.Direction.Y + r.Direction.Z;
            foreach (SphericalCollider c in colliders)
            {
                Vector3 targetPos = c.Transform.Position;
                float posSum = targetPos.X + targetPos.Y + targetPos.Z;

                float t = (posSum - originSum) / directionSum;
                t = MathHelper.Clamp(t, 0, maxDist);

                float dist = (r.Origin + r.Direction * t - targetPos).Length;
                if (dist < c.Radius)
                    hitList.Add(new RaycastHit(c, dist));
            }

            return hitList.OrderBy(_ => _.Distance).ToArray();
        }
        public RaycastHit[] CastSphere(Vector3 origin, float radius)
        {
            hitList.Clear();

            float r2 = radius * radius;
            foreach (SphericalCollider c in colliders)
            {
                Vector3 s = c.Transform.Position - origin;
                if (s.LengthSquared < r2)
                    hitList.Add(new RaycastHit(c, s.Length));
            }

            return hitList.OrderBy(_ => _.Distance).ToArray();
        }

        public void AddCollider(SphericalCollider c)
        {
            colliders.Add(c);
        }
        public void RemoveCollider(SphericalCollider c)
        {
            colliders.Remove(c);
        }
        public void Clear()
        {
            colliders.Clear();
        }
    }
}
