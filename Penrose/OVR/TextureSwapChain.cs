﻿using System;
using System.Runtime.InteropServices;

namespace Penrose.OVR
{
    [StructLayout(LayoutKind.Sequential)]
    public struct TextureSwapChain
    {
        public IntPtr TextureSwapChainData;
    }
}
