﻿using System;
using System.Runtime.InteropServices;

namespace Penrose.OVR
{
    public static class Oculus
    {
        private const String dllName = "LibOVRWrap";

        [DllImport(dllName, EntryPoint = "OVRInitialize")]
        public extern static void Initialize();

        [DllImport(dllName, EntryPoint = "OVRCreateSession")]
        private unsafe extern static void _CreateSession(Byte* rv);
        public unsafe static Session CreateSession()
        {
            Byte* rv = stackalloc Byte[Marshal.SizeOf<Session>()];
            _CreateSession(rv);
            return Marshal.PtrToStructure<Session>(new IntPtr(rv));
        }

        [DllImport(dllName, EntryPoint = "OVRGetHmdDesc")]
        private unsafe extern static void _GetHmdDesc(Byte* rv, Byte* sess);
        public unsafe static HmdDesc GetHmdDesc(Session sess)
        {
            Byte* rv = stackalloc Byte[Marshal.SizeOf<HmdDesc>()];
            Byte* sessPtr = stackalloc Byte[Marshal.SizeOf<Session>()];
            Marshal.StructureToPtr<Session>(sess, new IntPtr(sessPtr), false);

            _GetHmdDesc(rv, sessPtr);
            return Marshal.PtrToStructure<HmdDesc>(new IntPtr(rv));
        }

        [DllImport(dllName, EntryPoint = "OVRGetRenderDesc")]
        private unsafe extern static void _GetRenderDesc(Byte* rv, Byte* sess, int eyeType, Byte* fov);
        public unsafe static EyeRenderDesc GetRenderDesc(Session sess, EyeType eyeType, FovPort fov)
        {
            Byte* rv = stackalloc Byte[Marshal.SizeOf<EyeRenderDesc>()];
            Byte* sessPtr = stackalloc Byte[Marshal.SizeOf<Session>()];
            Byte* fovPtr = stackalloc Byte[Marshal.SizeOf<FovPort>()];
            Marshal.StructureToPtr<Session>(sess, new IntPtr(sessPtr), false);
            Marshal.StructureToPtr<FovPort>(fov, new IntPtr(fovPtr), false);

            _GetRenderDesc(rv, sessPtr, (int)eyeType, fovPtr);
            return Marshal.PtrToStructure<EyeRenderDesc>(new IntPtr(rv));
        }

        [DllImport(dllName, EntryPoint = "OVRGetSessionStatus")]
        private unsafe extern static void _GetSessionStatus(Byte* rv, Byte* sess);
        public unsafe static SessionStatus GetSessionStatus(Session sess)
        {
            Byte* rv = stackalloc Byte[Marshal.SizeOf<SessionStatus>()];
            Byte* sessPtr = stackalloc Byte[Marshal.SizeOf<Session>()];
            Marshal.StructureToPtr<Session>(sess, new IntPtr(sessPtr), false);

            _GetSessionStatus(rv, sessPtr);
            return Marshal.PtrToStructure<SessionStatus>(new IntPtr(rv));
        }

        [DllImport(dllName, EntryPoint = "OVRCreateTextureSwapChain")]
        private unsafe extern static void _CreateTextureSwapChain(Byte* rv, Byte* sess, int eye);
        public unsafe static TextureSwapChain CreateTextureSwapChain(Session sess, int eye)
        {
            Byte* rv = stackalloc Byte[Marshal.SizeOf<TextureSwapChain>()];
            Byte* sessPtr = stackalloc Byte[Marshal.SizeOf<Session>()];
            Marshal.StructureToPtr<Session>(sess, new IntPtr(sessPtr), false);

            _CreateTextureSwapChain(rv, sessPtr, eye);
            return Marshal.PtrToStructure<TextureSwapChain>(new IntPtr(rv));
        }

        [DllImport(dllName, EntryPoint = "OVRSubmitFrame")]
        private unsafe extern static void _SubmitFrame(Byte* sess, Int64 frameIndex, Byte* ld);
        public unsafe static void SubmitFrame(Session sess, Int64 frameIndex, LayerEyeFov ld)
        {
            Byte* sessPtr = stackalloc Byte[Marshal.SizeOf<Session>()];
            Byte* ldPtr = stackalloc Byte[Marshal.SizeOf<LayerEyeFov>()];
            Marshal.StructureToPtr<Session>(sess, new IntPtr(sessPtr), false);
            Marshal.StructureToPtr<LayerEyeFov>(ld, new IntPtr(ldPtr), false);

            _SubmitFrame(sessPtr, frameIndex, ldPtr);
        }

        [DllImport(dllName, EntryPoint = "OVRGetFovTextureSize")]
        private unsafe extern static void _GetFovTextureSize(Byte* rv, Byte* sess, int eye, Byte* fov, float pixelsPerDisplayPixel);
        public unsafe static SizeI GetFovTextureSize(Session sess, int eye, FovPort fov)
        {
            Byte* rv = stackalloc Byte[Marshal.SizeOf<SizeI>()];
            Byte* sessPtr = stackalloc Byte[Marshal.SizeOf<Session>()];
            Byte* fovPtr = stackalloc Byte[Marshal.SizeOf<FovPort>()];
            Marshal.StructureToPtr<Session>(sess, new IntPtr(sessPtr), false);
            Marshal.StructureToPtr<FovPort>(fov, new IntPtr(fovPtr), false);

            _GetFovTextureSize(rv, sessPtr, eye, fovPtr, 1);
            return Marshal.PtrToStructure<SizeI>(new IntPtr(rv));
        }

        [DllImport(dllName, EntryPoint = "OVRGetEyePoses")]
        private unsafe extern static void _GetEyePoses(Byte* rv, Byte* sess, Int64 frameIndex, Byte* eyeRenderDesc, Byte* sensorSampleTime);
        public unsafe static Posef[] GetEyePoses(Session sess, Int64 frameIndex, EyeRenderDesc[] eyeRenderDesc, out double sensorSampleTime)
        {
            Byte* rv = stackalloc Byte[Marshal.SizeOf<Posef>() * 2];
            Byte* sessPtr = stackalloc Byte[Marshal.SizeOf<Session>()];
            Byte* eyeRenderDescPtr = stackalloc Byte[Marshal.SizeOf<EyeRenderDesc>() * 2];
            Byte* sensorSampleTimePtr = stackalloc Byte[Marshal.SizeOf<double>()];
            Marshal.StructureToPtr<Session>(sess, new IntPtr(sessPtr), false);
            Marshal.StructureToPtr<EyeRenderDesc>(eyeRenderDesc[0], new IntPtr(eyeRenderDescPtr), false);
            Marshal.StructureToPtr<EyeRenderDesc>(eyeRenderDesc[1], new IntPtr(eyeRenderDescPtr) + Marshal.SizeOf<EyeRenderDesc>(), false);

            _GetEyePoses(rv, sessPtr, frameIndex, eyeRenderDescPtr, sensorSampleTimePtr);
            sensorSampleTime = Marshal.PtrToStructure<Double>(new IntPtr(sensorSampleTimePtr));
            return new Posef[]
            {
                Marshal.PtrToStructure<Posef>(new IntPtr(rv)),
                Marshal.PtrToStructure<Posef>(new IntPtr(rv) + Marshal.SizeOf<Posef>())
            };
        }

        [DllImport(dllName, EntryPoint = "OVRCommitTextureSwapChain")]
        private unsafe extern static void _CommitTextureSwapChain(Byte* sess, Byte* chain);
        public unsafe static void CommitTextureSwapChain(Session sess, TextureSwapChain chain)
        {
            Byte* sessPtr = stackalloc Byte[Marshal.SizeOf<Session>()];
            Byte* chainPtr = stackalloc Byte[Marshal.SizeOf<TextureSwapChain>()];
            Marshal.StructureToPtr<Session>(sess, new IntPtr(sessPtr), false);
            Marshal.StructureToPtr<TextureSwapChain>(chain, new IntPtr(chainPtr), false);

            _CommitTextureSwapChain(sessPtr, chainPtr);
        }

        [DllImport(dllName, EntryPoint = "OVRGetTextureSwapChainCurrentIndex")]
        private unsafe extern static void _GetTextureSwapChainCurrentIndex(Byte* rv, Byte* sess, Byte* chain);
        public unsafe static int GetTextureSwapChainCurrentIndex(Session sess, TextureSwapChain chain)
        {
            Byte* rv = stackalloc Byte[Marshal.SizeOf<Int32>()];
            Byte* sessPtr = stackalloc Byte[Marshal.SizeOf<Session>()];
            Byte* chainPtr = stackalloc Byte[Marshal.SizeOf<TextureSwapChain>()];
            Marshal.StructureToPtr<Session>(sess, new IntPtr(sessPtr), false);
            Marshal.StructureToPtr<TextureSwapChain>(chain, new IntPtr(chainPtr), false);

            _GetTextureSwapChainCurrentIndex(rv, sessPtr, chainPtr);
            return Marshal.PtrToStructure<Int32>(new IntPtr(rv));
        }

        [DllImport(dllName, EntryPoint = "OVRGetTextureSwapChainBufferGL")]
        private unsafe extern static void _GetTextureSwapChainBufferGL(Byte* rv, Byte* sess, Byte* chain, Int32 index);
        public unsafe static UInt32 GetTextureSwapChainBufferGL(Session sess, TextureSwapChain chain, Int32 index)
        {
            Byte* rv = stackalloc Byte[Marshal.SizeOf<UInt32>()];
            Byte* sessPtr = stackalloc Byte[Marshal.SizeOf<Session>()];
            Byte* chainPtr = stackalloc Byte[Marshal.SizeOf<TextureSwapChain>()];
            Marshal.StructureToPtr<Session>(sess, new IntPtr(sessPtr), false);
            Marshal.StructureToPtr<TextureSwapChain>(chain, new IntPtr(chainPtr), false);

            _GetTextureSwapChainBufferGL(rv, sessPtr, chainPtr, index);
            return Marshal.PtrToStructure<UInt32>(new IntPtr(rv));
        }

        [DllImport(dllName, EntryPoint = "OVRCreateMirrorTextureGL")]
        private unsafe extern static void _OVRCreateMirrorTextureGL(Byte* rv, Byte* sess, int width, int height);
        public unsafe static MirrorTexture OVRCreateMirrorTextureGL(Session sess, int width, int height)
        {
            Byte* rv = stackalloc Byte[Marshal.SizeOf<MirrorTexture>()];
            Byte* sessPtr = stackalloc Byte[Marshal.SizeOf<Session>()];
            Marshal.StructureToPtr<Session>(sess, new IntPtr(sessPtr), false);

            _OVRCreateMirrorTextureGL(rv, sessPtr, width, height);
            return Marshal.PtrToStructure<MirrorTexture>(new IntPtr(rv));
        }

        [DllImport(dllName, EntryPoint = "OVRGetMirrorTextureBufferGL")]
        private unsafe extern static void _OVRGetMirrorTextureBufferGL(Byte* rv, Byte* sess, Byte* mirrorTexture);
        public unsafe static int OVRGetMirrorTextureBufferGL(Session sess, MirrorTexture mirrorTexture)
        {
            Byte* rv = stackalloc Byte[Marshal.SizeOf<Int32>()];
            Byte* sessPtr = stackalloc Byte[Marshal.SizeOf<Session>()];
            Byte* mirrorTexturePtr = stackalloc Byte[Marshal.SizeOf<MirrorTexture>()];
            Marshal.StructureToPtr<Session>(sess, new IntPtr(sessPtr), false);
            Marshal.StructureToPtr<MirrorTexture>(mirrorTexture, new IntPtr(mirrorTexturePtr), false);

            _OVRGetMirrorTextureBufferGL(rv, sessPtr, mirrorTexturePtr);
            return Marshal.PtrToStructure<Int32>(new IntPtr(rv));
        }
    }
}
