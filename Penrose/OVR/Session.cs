﻿using System;
using System.Runtime.InteropServices;

namespace Penrose.OVR
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Session
    {
        public IntPtr HmdStruct;
    }
}
