﻿using System;
using System.Runtime.InteropServices;

namespace Penrose.OVR
{
    public enum HmdType
    {
        None = 0,
        DK1 = 3,
        DKHD = 4,
        DK2 = 6,
        CB = 8,
        Other = 9,
        E3_2015 = 10,
        ES06 = 11,
        ES09 = 12,
        ES11 = 13,
        CV = 14,

        EnumSize = 0x7FFFFFFF,
    }
    
    [StructLayout(LayoutKind.Sequential)]
    public struct HmdDesc
    {
        public HmdType Type;
        
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
        private String PAD1;
       
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public String ProductName;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public String Manufacturer;

        public short VendorId;
        public short ProductId;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 24)]
        public String SerialNumber;

        public short FirmwareMajor;
        public short FirmwareMinor;

        public uint AvailableHmdCaps;
        public uint DefaultHmdCaps;
        public uint AvailableTrackingCaps;
        public uint DefaultTrackingCaps;
        
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public FovPort[] DefaultEyeFov;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public FovPort[] MaxEyeFov;
        
        public SizeI Resolution;
        public float DisplayRefreshRate;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
        private String PAD2;
        
        public override string ToString()
        {
            return String.Format("{0}-Typed {1}, Ver{2}.{3}", Type, ProductName, FirmwareMajor, FirmwareMinor);
        }
    }
}
