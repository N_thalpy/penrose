﻿using System;
using System.Runtime.InteropServices;

namespace Penrose.OVR
{
    [StructLayout(LayoutKind.Sequential)]
    public struct SessionStatus
    {
        private Byte isVisible;
        private Byte hmdPresent;
        private Byte hmdMounted;
        private Byte displayLost;
        private Byte shouldQuit;
        private Byte shouldRecenter;

        public Boolean IsVisible
        {
            get
            {
                return isVisible == 1;
            }
        }
        public Boolean HmdPresent
        {
            get
            {
                return hmdPresent == 1;
            }
        }
        public Boolean HmdMounted
        {
            get
            {
                return hmdMounted == 1;
            }
        }
        public Boolean DisplayLost
        {
            get
            {
                return displayLost == 1;
            }
        }
        public Boolean ShouldQuit
        {
            get
            {
                return shouldQuit == 1;
            }
        }
        public Boolean ShouldRecenter
        {
            get
            {
                return shouldRecenter == 1;
            }
        }

        public override string ToString()
        {
            return String.Format("Visible: {0}, HMD[Present: {1}, Mounted: {2}, Lost: {3}], Should[Quit: {4}, Recenter: {5}]",
                IsVisible, HmdPresent, HmdMounted, DisplayLost, ShouldQuit, ShouldRecenter);
        }
    }
}
