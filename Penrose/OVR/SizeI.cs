﻿using System.Runtime.InteropServices;

namespace Penrose.OVR
{
    [StructLayout(LayoutKind.Sequential)]
    public struct SizeI
    {
        public int W;
        public int H;
    }
}
