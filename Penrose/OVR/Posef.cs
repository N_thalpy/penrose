﻿using OpenTK;
using System.Runtime.InteropServices;

namespace Penrose.OVR
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Posef
    {
        private Vector4 orientation;
        public Vector3 Position;

        public Quaternion Orientation
        {
            get
            {
                return new Quaternion(orientation.X, orientation.Y, orientation.Z, orientation.W);
            }
        }
    }
}
