﻿using System;
using System.Runtime.InteropServices;

namespace Penrose.OVR
{
    [StructLayout(LayoutKind.Sequential)]
    public struct MirrorTexture
    {
        public IntPtr MirrorTextureData;
    }
}
