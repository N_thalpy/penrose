﻿using OpenTK;
using System.Runtime.InteropServices;

namespace Penrose.OVR
{
    public enum EyeType
    {
        Left = 0,
        Right = 1,
        Count = 2,
        EnumSize = 0x7FFFFFFF,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct EyeRenderDesc
    {
        public EyeType Eye;
        public FovPort Fov;
        public RectI DistortedViewport;
        public Vector2 PixelsPerTanAngleAtCenter;
        public Vector3 HmdToEyeOffset;
    }
}
