﻿using OpenTK;
using Penrose.GameObject.Ship;

namespace Penrose.AI
{
    public static class AIHelper
    {
        public static void MoveToward(float deltaTime, ShipBase owner, Vector3 target)
        {
            // I don't know why it works
            Matrix4 lookAt = Matrix4.LookAt(owner.Transform.Position, target, Vector3.UnitY);
            Quaternion targetQuat = owner.Transform.Orientation.Inverted() * lookAt.ExtractRotation().Inverted();

            targetQuat.ToEulerAngle(out float pitch, out float yaw, out float roll);

            owner.PrimaryEngine.Update(
                deltaTime,
                true,
                false,
                pitch,
                yaw,
                roll);
        }
    }
}
