﻿using Penrose.GameObject.Ship;
using Penrose.GameObject.Ship.Impl;

namespace Penrose.AI.Impl
{
    public sealed class SimpleOrionNebulaAI : AIBase
    {
        private bool spotted;

        public SimpleOrionNebulaAI(ShipBase target)
            : base()
        {
            Target = target;
        }
        public void Initialize(OrionNebula target)
        {
            spotted = false;
        }

        public override void Update(float deltaTime)
        {
            if (Owner.Alive == true)
            {
                AIHelper.MoveToward(deltaTime, Owner, Target.Transform.Position);
                
            }
            else
            {
            }
        }
    }
}
