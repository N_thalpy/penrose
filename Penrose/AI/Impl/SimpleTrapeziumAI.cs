﻿using OpenTK;
using Penrose.GameObject.Ship;
using Penrose.GameObject.Ship.Impl;
using Penrose.Physics;

namespace Penrose.AI.Impl
{
    public sealed class SimpleTrapeziumAI : AIBase
    {
        public SimpleTrapeziumAI(ShipBase target)
            : base()
        {
            Target = target;
        }
        public void Initialize(Trapzeium target)
        {

        }

        public override void Update(float deltaTime)
        {
            if (Owner.Alive == true)
            {
                AIHelper.MoveToward(deltaTime, Owner, Target.Transform.Position);

                RaycastHit[] hits = PhysicsManager.Instance.Raycast(new Ray(Owner.Transform.Position, Owner.Transform.Orientation * (-Vector3.UnitZ)));
                bool firePrimary = false;
                foreach (RaycastHit hit in hits)
                {
                    if (hit.Collider.Owner is ShipBase && hit.Collider.Owner == Target)
                    {
                        firePrimary = true;
                        break;
                    }
                }

                Owner.PrimaryWeapon?.Update(deltaTime, firePrimary);
            }
            else
            {
                Owner.PrimaryWeapon?.Update(deltaTime, false);
            }
        }
    }
}
