﻿using Penrose.GameObject.Ship;

namespace Penrose.AI
{
    public abstract class AIBase
    {
        public ShipBase Owner;
        public ShipBase Target;

        protected AIBase()
        {
            
        }

        public abstract void Update(float deltaTime);
    }
}
