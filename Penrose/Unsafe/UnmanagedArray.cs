﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Penrose.Unsafe
{
    public unsafe class UnmanagedArray<T>
    {
        private void* pointer;
        public void* Pointer
        {
            get
            {
                if (pointer == null)
                    throw new ObjectDisposedException(this.ToString());
                return pointer;
            }
        }
        public Int32 Capacity
        {
            get;
            private set;
        }

        public UnmanagedArray(Int32 initialCapacity)
        {
            pointer = Marshal.AllocHGlobal(initialCapacity * Marshal.SizeOf<T>()).ToPointer();
            Capacity = initialCapacity;
        }
        ~UnmanagedArray()
        {
            Debug.Assert(pointer == null);
        }

        // Warning: Address can change; MUST EVICT CACHED POINTER ADDRESS before calling this method
        // Warning: This is NOT realloc; WILL NOT COPY DATAS
        public bool CheckCapacity(Int32 targetCapacity)
        {
            if (targetCapacity <= Capacity)
                return false;

            while (targetCapacity > Capacity)
                Capacity *= 2;
            Marshal.FreeHGlobal(new IntPtr(pointer));
            pointer = Marshal.AllocHGlobal(Capacity * Marshal.SizeOf<T>()).ToPointer();
            return true;
        }
        public void Dispose()
        {
            Marshal.FreeHGlobal(new IntPtr(pointer));
            pointer = null;
        }

        public override String ToString()
        {
            return String.Format("UnmanagedArray<{0}>, addr {1}, cap {2}", typeof(T).FullName, new IntPtr(pointer).ToInt64(), Capacity);
        }
        public override bool Equals(Object obj)
        {
            if (obj is UnmanagedArray<T>)
                return ((UnmanagedArray<T>)obj).pointer == pointer;
            return false;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
