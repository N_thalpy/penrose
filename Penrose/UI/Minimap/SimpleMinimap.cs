﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Penrose.GameObject;
using Penrose.GameObject.Ship;
using Penrose.GameObject.Ship.Impl;
using Penrose.Rendering;
using Penrose.Rendering.Shader;
using Penrose.Rendering.Shader.ShaderVar;
using Penrose.Rendering.VertexType;
using Penrose.Unsafe;
using System;
using System.Collections.Generic;

namespace Penrose.UI.Minimap
{
    public sealed class SimpleMinimap : MinimapBase
    {
        private enum InnerMinimapObjectData
        {
            Player,

            EnemyStingray,
            EnemyOrionNebula,
            EnemyTrapezium,
        }
        private struct MinimapData
        {
            public Transform Transform;
            public InnerMinimapObjectData Type;

            public MinimapData(Transform transform, InnerMinimapObjectData type)
            {
                Transform = transform;
                Type = type;
            }
        }

        private List<MinimapData> objectList;

        private UnmanagedArray<VertexPositionTextureColor> vertexArray;
        private UnmanagedArray<Int32> elementArray;

        private VertexBuffer vbo;
        private ElementBuffer ebo;
        private ShaderBase shader;

        public SimpleMinimap(int width, int height, ShipBase owner)
            : base(width, height, owner)
        {
            objectList = new List<MinimapData>();

            int objectCount = 100;
            vertexArray = new UnmanagedArray<VertexPositionTextureColor>(objectCount * 4);
            elementArray = new UnmanagedArray<Int32>(objectCount * 4);

            vbo = VertexBuffer.Create<VertexPositionTextureColor>(vertexArray.Capacity);
            ebo = ElementBuffer.Create(elementArray.Capacity);
            shader = ShaderProvider.TexturedShader;

            vbo.Seal();
            ebo.Seal();
        }

        public override void Clear()
        {
            objectList.Clear();
        }
        public override void Enqueue(ShipBase ship, MinimapObjectType type)
        {
            switch (type)
            {
                case MinimapObjectType.Player:
                    objectList.Add(new MinimapData(ship.Transform, InnerMinimapObjectData.Player));
                    break;

                case MinimapObjectType.Enemy:
                    if (ship is Stingray || ship is StingrayMkII)
                    {
                        objectList.Add(new MinimapData(ship.Transform, InnerMinimapObjectData.EnemyStingray));
                        break;
                    }
                    if (ship is OrionNebula)
                    {
                        objectList.Add(new MinimapData(ship.Transform, InnerMinimapObjectData.EnemyOrionNebula));
                        break;
                    }
                    if (ship is Trapzeium)
                    {
                        objectList.Add(new MinimapData(ship.Transform, InnerMinimapObjectData.EnemyTrapezium));
                        break;
                    }
                    throw new ArgumentException();
            }
        }
        public unsafe override void Flush()
        {
            // create vbo/ebo data
            int objectCount = objectList.Count;

            vbo.UnSeal();
            ebo.UnSeal();

            vertexArray.CheckCapacity(objectCount * 4);
            vbo.Resize<VertexPositionTextureColor>(vertexArray.Capacity);
            elementArray.CheckCapacity(objectCount * 4);
            ebo.Resize(elementArray.Capacity);

            VertexPositionTextureColor* vp = (VertexPositionTextureColor*)vertexArray.Pointer;
            Int32* ep = (Int32*)elementArray.Pointer;

            int vertexCount = 0;

            foreach (MinimapData data in objectList)
            {
                float texelSize;
                Quaternion direction = Owner.Transform.Orientation.Inverted();

                Vector3 dpos = data.Transform.Position - Owner.Transform.Position;
                dpos = direction * dpos;
                dpos = dpos.SwizzleXZ0();
                if (dpos.Length != 0)
                    dpos.Normalize();
                dpos *= (data.Transform.Position - Owner.Transform.Position).Length / 20;

                Vector3 pos;
                Color4 c;

                switch (data.Type)
                {
                    case InnerMinimapObjectData.Player:
                        c = Color4.Green;
                        texelSize = 8;
                        break;

                    case InnerMinimapObjectData.EnemyStingray:
                        c = Color4.Red;
                        texelSize = 8;
                        break;

                    case InnerMinimapObjectData.EnemyOrionNebula:
                        c = Color4.Turquoise;
                        texelSize = 12;
                        break;

                    case InnerMinimapObjectData.EnemyTrapezium:
                        c = Color4.Violet;
                        texelSize = 7;
                        break;

                    default:
                        throw new ArgumentException();
                }

                // 0 3
                // 1 2
                pos = (texelSize * new Vector3(-1, 1, 0)) + dpos;
                pos = pos.ElementwiseProduct(2f / Width, 2f / Height, 0);
                *(vp++) = new VertexPositionTextureColor(pos, GLHelper.UVLeftUpper, c);

                pos = (texelSize * new Vector3(-1, -1, 0)) + dpos;
                pos = pos.ElementwiseProduct(2f / Width, 2f / Height, 0);
                *(vp++) = new VertexPositionTextureColor(pos, GLHelper.UVLeftLower, c);

                pos = (texelSize * new Vector3(1, -1, 0)) + dpos;
                pos = pos.ElementwiseProduct(2f / Width, 2f / Height, 0);
                *(vp++) = new VertexPositionTextureColor(pos, GLHelper.UVRightLower, c);

                pos = (texelSize * new Vector3(1, 1, 0)) + dpos;
                pos = pos.ElementwiseProduct(2f / Width, 2f / Height, 0);
                *(vp++) = new VertexPositionTextureColor(pos, GLHelper.UVRightUpper, c);

                *(ep++) = vertexCount + 0;
                *(ep++) = vertexCount + 1;
                *(ep++) = vertexCount + 2;
                *(ep++) = vertexCount + 3;

                vertexCount += 4;
            }

            vbo.Upload<VertexPositionTextureColor>(vertexArray.Pointer, 0, objectCount * 4);
            ebo.Upload(elementArray.Pointer, 0, objectCount * 4);

            vbo.Seal();
            ebo.Seal();

            // render to frame buffer
            GLHelper.PushCap(EnableCap.Blend, true);
            FrameBuffer.BindFrameBuffer();

            GL.ClearColor(1.0f, 1.0f, 1.0f, 0.8f);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            vbo.Bind(shader);
            ebo.Bind();
            shader.Bind();

            // Temporary code; will render only enemy stingray
            Matrix4 id = Matrix4.Identity;
            shader.BindUniformMatrix4(SV.UniformTransform, ref id);
            shader.BindTexture(SV.UniformTexture, TextureProvider.MinimapPlayerIcon);

            GLHelper.DrawPrimitive(PrimitiveType.Quads, objectCount * 4);

            vbo.Unbind();
            ebo.Unbind();
            shader.Unbind();

            FrameBuffer.UnbindFrameBuffer();
            GLHelper.PopCap(EnableCap.Blend);
        }

        public override void Dispose()
        {
            base.Dispose();

            vertexArray.Dispose();
            elementArray.Dispose();

            vbo.Dispose();
            ebo.Dispose();
        }
    }
}
