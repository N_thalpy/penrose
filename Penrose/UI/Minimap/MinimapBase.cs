﻿using Penrose.GameObject.Ship;
using Penrose.Rendering;
using System;

namespace Penrose.UI.Minimap
{
    public enum MinimapObjectType
    {
        Player,
        Enemy,
    }

    public abstract class MinimapBase : IDisposable
    {
        public ShipBase Owner;
        public FrameBuffer FrameBuffer;

        public readonly int Width;
        public readonly int Height;

        protected MinimapBase(int width, int height, ShipBase owner)
        {
            Owner = owner;
            FrameBuffer = FrameBuffer.Create(width, height);

            Width = width;
            Height = height;
        }

        public abstract void Clear();
        public abstract void Enqueue(ShipBase ship, MinimapObjectType type);
        public abstract void Flush();

        public virtual void Dispose()
        {
            FrameBuffer.Dispose();
        }
    }
}
