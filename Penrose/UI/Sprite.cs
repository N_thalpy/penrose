﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Penrose.GameObject;
using Penrose.Rendering;
using Penrose.Rendering.Shader;
using Penrose.Rendering.Shader.ShaderVar;
using Penrose.Rendering.VertexType;
using System;

namespace Penrose.UI
{
    public sealed class Sprite : IDisposable
    {
        public Transform Transform;
        public bool DoubleSided;

        private VertexBuffer vbo;
        private ElementBuffer ebo;
        private ShaderBase shader;
        private Texture texture;

        public Sprite(Texture tex, bool doubleSided)
        {
            DoubleSided = doubleSided;
            texture = tex;

            vbo = VertexBuffer.Create(new VertexPositionTexture[4]
            {
                new VertexPositionTexture(-tex.Width, tex.Height, 0, GLHelper.UVLeftLower),
                new VertexPositionTexture(-tex.Width, -tex.Height, 0, GLHelper.UVLeftUpper),
                new VertexPositionTexture(tex.Width, -tex.Height, 0, GLHelper.UVRightUpper),
                new VertexPositionTexture(tex.Width, tex.Height, 0, GLHelper.UVRightLower),
            });
            ebo = ElementBuffer.Create(new int[]
            {
                0, 1, 2, 3,
                3, 2, 1, 0,
            });

            shader = ShaderProvider.TexturedShader;

            Transform = new Transform();
        }
        public void Render(Transform eyeTransform)
        {
            RenderQueue.Instance.Enqueue(new RenderQueue.DepthIndependentRenderJob()
            {
                VertexBuffer = vbo,
                ElementBuffer = ebo,
                Shader = ShaderProvider.TexturedShader,

                ElementCount = DoubleSided == true ? 8 : 4,
                PrimitiveType = PrimitiveType.Quads,

                VariableManager = new ShaderVariableManager()
                {
                    UniformTransform = GLHelper.CreateTransformMatrix(eyeTransform, Transform),
                    UniformColor = Color4.White,
                    UniformTexture = texture.TextureHandle,
                }
            });
        }

        public void Dispose()
        {
            vbo.Dispose();
            ebo.Dispose();
        }
    }
}
