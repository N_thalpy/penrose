﻿using Penrose.Rendering;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Penrose.UI
{
    public sealed class BitmapFont : IDisposable
    {
        // Format Example:
        //
        // info face="Arial" size=32 bold=0 italic=0 charset="" unicode=1 stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=1,1 outline=0
        // common lineHeight=32 base=26 scaleW=256 scaleH=256 pages=1 packed=0 alphaChnl=0 redChnl=4 greenChnl=4 blueChnl=4
        // page id=0 file="a_0.png"
        // chars count=95
        // char id=32 x=110 y=22 width=3 height=1 xoffset=-1 yoffset=31 xadvance=8 page=0 chnl=15
        // kernings count=91
        // kerning first=32 second=65 amount=-2       

        public struct Character
        {
            public int Id;
            public int X;
            public int Y;
            public int Width;
            public int Height;
            public int XOffset;
            public int YOffset;
            public int XAdvance;
            public int Page;
            public int Channel;
        }
        private struct Kerning
        {
            public int First;
            public int Second;
            public int Amount;
        }

        public String Face;
        public int Size;
        public int LineHeight;
        public int ScaleW;
        public int ScaleH;

        private bool disposed;

        private List<Character> characterList;
        private List<Kerning> kerningList;
        private List<Texture> textureList;

        public static BitmapFont CreateFromPath(String path)
        {
            using (StreamReader sr = new StreamReader(path))
                return new BitmapFont(sr.ReadToEnd(), path);
        }

        private BitmapFont(String data, String path)
        {
            characterList = new List<Character>();
            kerningList = new List<Kerning>();
            textureList = new List<Texture>();

            String[] splitted;
            String[] param;

            Character c = new Character();
            Kerning k = new Kerning();

            foreach (String line in data.Split(new Char[] { '\r', '\n' }))
            {
                if (String.IsNullOrWhiteSpace(line) == true)
                    continue;

                splitted = line.Split(' ').Where(_ => String.IsNullOrWhiteSpace(_) == false).ToArray();
                switch (splitted[0])
                {
                    case "info":
                        for (int idx = 1; idx < splitted.Length; idx++)
                        {
                            param = splitted[idx].Split('=');
                            switch (param[0])
                            {
                                case "face":
                                    Face = Unquote(param[1]);
                                    break;

                                case "size":
                                    Size = Int32.Parse(param[1]);
                                    break;

                                case "bold":
                                case "italic":
                                case "charset":
                                case "unicode":
                                case "stretchH":
                                case "smooth":
                                case "aa":
                                case "padding":
                                case "spacing":
                                case "outline":
                                    break;

                                default:
                                    throw new ArgumentException();
                            }
                        }
                        break;

                    case "common":
                        for (int idx = 1; idx < splitted.Length; idx++)
                        {
                            param = splitted[idx].Split('=');
                            switch (param[0])
                            {
                                case "lineHeight":
                                    LineHeight = Int32.Parse(param[1]);
                                    break;
                                    
                                case "scaleW":
                                    ScaleW = Int32.Parse(param[1]);
                                    break;

                                case "scaleH":
                                    ScaleH = Int32.Parse(param[1]);
                                    break;

                                case "base":
                                case "pages":
                                case "packed":
                                case "alphaChnl":
                                case "redChnl":
                                case "greenChnl":
                                case "blueChnl":
                                    break;

                                default:
                                    throw new ArgumentException();
                            }
                        }
                        break;

                    case "page":
                        for (int idx = 1; idx < splitted.Length; idx++)
                        {
                            param = splitted[idx].Split('=');
                            switch (param[0])
                            {
                                case "file":
                                    textureList.Add(
                                        Texture.CreateFromBitmapPath(Path.Combine(
                                            Path.GetDirectoryName(path),
                                            Unquote(param[1]))));
                                    break;

                                case "id":
                                    break;

                                default:
                                    throw new ArgumentException();
                            }
                        }
                        break;

                    case "char":
                        for (int idx = 1; idx < splitted.Length; idx++)
                        {
                            param = splitted[idx].Split('=');
                            switch (param[0])
                            {
                                case "id":
                                    c.Id = Int32.Parse(param[1]);
                                    break;

                                case "x":
                                    c.X = Int32.Parse(param[1]);
                                    break;

                                case "y":
                                    c.Y = Int32.Parse(param[1]);
                                    break;

                                case "width":
                                    c.Width = Int32.Parse(param[1]);
                                    break;

                                case "height":
                                    c.Height = Int32.Parse(param[1]);
                                    break;

                                case "xoffset":
                                    c.XOffset = Int32.Parse(param[1]);
                                    break;

                                case "yoffset":
                                    c.YOffset = Int32.Parse(param[1]);
                                    break;

                                case "xadvance":
                                    c.XAdvance = Int32.Parse(param[1]);
                                    break;

                                case "page":
                                    c.Page = Int32.Parse(param[1]);
                                    break;

                                case "chnl":
                                    c.Channel = Int32.Parse(param[1]);
                                    break;

                                default:
                                    throw new ArgumentException();
                            }
                        }
                        characterList.Add(c);
                        break;

                    case "kerning":
                        for (int idx = 1; idx < splitted.Length; idx++)
                        {
                            param = splitted[idx].Split('=');
                            switch (param[0])
                            {
                                case "first":
                                    k.First = Int32.Parse(param[1]);
                                    break;

                                case "second":
                                    k.Second = Int32.Parse(param[1]);
                                    break;

                                case "amount":
                                    k.Amount = Int32.Parse(param[1]);
                                    break;

                                default:
                                    throw new ArgumentException();
                            }
                        }
                        kerningList.Add(k);
                        break;

                    case "chars":
                    case "kernings":
                        break;

                    default:
                        throw new ArgumentException();
                }
            }
        }
        ~BitmapFont()
        {
            Debug.Assert(disposed == true);
        }

        public Character GetCharacter(char c)
        {
            return characterList.Find(_ => _.Id == c);
        }
        public int GetKerning(char prev, char curr)
        {
            return kerningList.FindAll(_ => _.First == prev && _.Second == curr).FirstOrDefault().Amount;
        }
        public Texture GetTexture(int page)
        {
            return textureList[page];
        }

        private String Unquote(String s)
        {
            return s.Substring(1, s.Length - 2);
        }

        public void Dispose()
        {
            Debug.Assert(disposed == false);
            foreach (Texture t in textureList)
                t.Dispose();

            disposed = true;
        }
    }
}
