﻿namespace Penrose.UI
{
    public static class BitmapFontProvider
    {
        public static BitmapFont Consolas;

        static BitmapFontProvider()
        {
        }

        public static void Initialize()
        {
            Consolas = BitmapFont.CreateFromPath("Resource/Font/consolas.fnt");

            GameSystem.InvokeOnExit += Dispose;
        }
        public static void Dispose()
        {
            Consolas.Dispose();
        }
    }
}
