﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Penrose.GameObject;
using Penrose.Rendering;
using Penrose.Rendering.Shader;
using Penrose.Rendering.Shader.ShaderVar;
using Penrose.Rendering.VertexType;
using System;
using System.Linq;
using static Penrose.UI.BitmapFont;

namespace Penrose.UI
{
    public sealed class BitmapLabel : IDisposable
    {
        [Flags]
        public enum Dock
        {
            Center = 0x0,
            Left = 0x1,
            Right = 0x2,
            Top = 0x4,
            Bottom = 0x8,

            XAlign = Left | Right,
            YAlign = Top | Bottom,
        }
        public enum TextAlign
        {
            Left = 0x0,
            Right = 0x1,
        }

        public Transform Transform;
        public bool AutoRefresh;

        private Dock docking;
        public Dock Docking
        {
            get
            {
                return docking;
            }
            set
            {
                if (docking == value)
                    return;

                docking = value;
                if (AutoRefresh == true)
                    Refresh();
            }
        }

        private TextAlign align;
        public TextAlign Align
        {
            get
            {
                return align;
            }
            set
            {
                if (align == value)
                    return;

                align = value;
                if (AutoRefresh == true)
                    Refresh();
            }
        }

        private String text;
        public String Text
        {
            get
            {
                return text;
            }
            set
            {
                if (text == value)
                    return;

                text = value;
                if (AutoRefresh == true)
                    Refresh();
            }
        }

        public Color4 Color;

        private VertexBuffer vbo;
        private ElementBuffer ebo;
        private FrameBuffer fbo;
        private ShaderBase shader;

        private VertexBuffer tempVbo;
        private ElementBuffer tempEbo;

        private int resolution;

        private int xSize;
        private int ySize;

        private BitmapFont font;
        public BitmapFont Font
        {
            get
            {
                return font;
            }
            set
            {
                if (font == value)
                    return;

                font = value;
                if (AutoRefresh == true)
                    Refresh();
            }
        }

        public BitmapLabel(BitmapFont font, int initWidth, int initHeight)
        {
            AutoRefresh = true;

            Transform = new Transform();
            text = String.Empty;
            this.font = font;

            resolution = 1;

            // 0   3
            // 
            // 1   2

            ebo = ElementBuffer.Create(new int[]
            {
                0, 1, 2, 0, 2, 3
            });
            shader = ShaderProvider.BitmapFontShader;
            Color = Color4.White;

            fbo = FrameBuffer.Create(initWidth, initHeight);

            Refresh();
        }

        public unsafe void Refresh()
        {
            if (vbo != null)
            {
                vbo.Dispose();
                vbo = null;
            }

            if (text == null || font == null || String.IsNullOrWhiteSpace(text))
                return;

            // evaluate size
            xSize = 0;
            ySize = font.LineHeight * resolution;
            int currentX = 0;
            int charCount = 0;

            if (fbo.Width < xSize || fbo.Height < ySize)
            {
                fbo.Dispose();
                fbo = FrameBuffer.Create(fbo.Width, fbo.Height);
            }

            Char prevChar = (Char)0;
            foreach (Char ch in text)
            {
                switch (ch)
                {
                    case '\r':
                        break;

                    case '\n':
                        currentX = 0;
                        ySize += font.LineHeight * resolution;
                        break;

                    default:
                        Character chData = Font.GetCharacter(ch);
                        currentX += (chData.XAdvance + font.GetKerning(prevChar, ch)) * resolution;
                        xSize = Math.Max(xSize, currentX);
                        charCount++;
                        break;
                }

                prevChar = ch;
            }

            float left = -xSize / resolution;
            float right = xSize / resolution;
            float top = ySize / resolution;
            float bottom = -ySize / resolution;

            switch (docking & Dock.XAlign)
            {
                case Dock.Center:
                    break;

                case Dock.Left:
                    right -= left;
                    left = 0;
                    break;

                case Dock.Right:
                    left -= right;
                    right = 0;
                    break;

                default:
                    throw new ArgumentException();
            }
            switch (docking & Dock.YAlign)
            {
                case Dock.Center:
                    break;

                case Dock.Top:
                    bottom -= top;
                    top = 0;
                    break;

                case Dock.Bottom:
                    top -= bottom;
                    bottom = 0;
                    break;

                default:
                    throw new ArgumentException();
            }

            vbo = VertexBuffer.Create(new VertexPositionTexture[]
            {
                new VertexPositionTexture(left, top, 0, GLHelper.UVLeftLower),
                new VertexPositionTexture(left, bottom, 0, GLHelper.UVLeftUpper),
                new VertexPositionTexture(right, bottom, 0, GLHelper.UVRightUpper),
                new VertexPositionTexture(right, top, 0, GLHelper.UVRightLower),
            });

            // make fbo, bind, draw, unbind
            int x = 0;
            int y = 0;

            switch (align)
            {
                case TextAlign.Left:
                    x = 0;
                    break;

                case TextAlign.Right:
                    x = xSize;
                    for (int jdx = 0; jdx < text.Length && text[jdx] != '\n'; jdx++)
                        switch (text[jdx])
                        {
                            case '\r':
                                break;

                            default:
                                x -= font.GetCharacter(text[jdx]).XAdvance * resolution;
                                break;
                        }

                    break;
            }

            tempVbo = VertexBuffer.Create<VertexPositionTexture>(charCount * 4);
            tempEbo = ElementBuffer.Create(Enumerable.Range(0, charCount * 4).ToArray());

            VertexPositionTexture* vptArray = stackalloc VertexPositionTexture[charCount * 4];
            int vp = 0;

            prevChar = (Char)0;
            for (int idx = 0; idx < text.Length; idx++)
            {
                Char ch = text[idx];
                x -= font.GetKerning(prevChar, ch) * resolution;

                switch (ch)
                {
                    case '\r':
                        break;

                    case '\n':
                        switch (align)
                        {
                            case TextAlign.Left:
                                x = 0;
                                break;

                            case TextAlign.Right:
                                x = xSize;
                                for (int jdx = idx + 1; jdx < text.Length && text[jdx] != '\n'; jdx++)
                                    switch (text[jdx])
                                    {
                                        case '\r':
                                            break;

                                        default:
                                            x -= font.GetCharacter(text[jdx]).XAdvance * resolution;
                                            break;
                                    }

                                break;
                        }
                        y += font.LineHeight * resolution;
                        break;

                    default:
                        Character chData = Font.GetCharacter(ch);

                        // 3   2
                        //
                        // 0   1
                        vptArray[vp++] = new VertexPositionTexture(
                            x + chData.XOffset * resolution,
                            y + (chData.Height + chData.YOffset) * resolution,
                            0,
                            chData.X,
                            chData.Y + chData.Height);
                        vptArray[vp++] = new VertexPositionTexture(
                            x + (chData.Width + chData.XOffset) * resolution,
                            y + (chData.Height + chData.YOffset) * resolution,
                            0,
                            chData.X + chData.Width,
                            chData.Y + chData.Height);
                        vptArray[vp++] = new VertexPositionTexture(
                            x + (chData.Width + chData.XOffset) * resolution,
                            y + chData.YOffset * resolution,
                            0,
                            chData.X + chData.Width,
                            chData.Y);
                        vptArray[vp++] = new VertexPositionTexture(
                            x + chData.XOffset * resolution,
                            y + chData.YOffset * resolution,
                            0,
                            chData.X,
                            chData.Y);

                        x += chData.XAdvance * resolution;
                        break;
                }
            }

            for (int idx = 0; idx < charCount * 4; idx++)
            {
                Vector3 p = vptArray[idx].Position;
                vptArray[idx].Position.X = 2 * p.X / xSize - 1;
                vptArray[idx].Position.Y = 2 * p.Y / ySize - 1;

                Vector2 tc = vptArray[idx].TexCoord;
                vptArray[idx].TexCoord.X = tc.X / font.ScaleW;
                vptArray[idx].TexCoord.Y = tc.Y / font.ScaleH;
            }

            tempVbo.Upload<VertexPositionTexture>(vptArray, 0, charCount * 4);
            tempVbo.Seal();

            Matrix4 id = Matrix4.Identity;
            fbo.BindFrameBuffer();

            GL.ClearColor(new Color4(0, 0, 0, 0));
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            tempVbo.Bind(shader);
            tempEbo.Bind();

            shader.Bind();
            shader.BindUniformMatrix4(SV.UniformTransform, ref id);
            shader.BindUniform4(SV.UniformColor, Color4.White);
            shader.BindTexture(SV.UniformTexture, font.GetTexture(0));

            GLHelper.PushCap(EnableCap.Blend, true);
            GLHelper.DrawPrimitive(PrimitiveType.Quads, tempEbo.Count);
            GLHelper.PopCap(EnableCap.Blend);

            tempVbo.Unbind();
            tempEbo.Unbind();
            shader.Unbind();

            tempVbo.Dispose();
            tempEbo.Dispose();

            fbo.UnbindFrameBuffer();
        }

        public void Render(Transform eyeTransform)
        {
            if (text == null || font == null || String.IsNullOrWhiteSpace(text))
                return;

            RenderQueue.Instance.Enqueue(new RenderQueue.DepthIndependentRenderJob()
            {
                VertexBuffer = vbo,
                ElementBuffer = ebo,
                Shader = ShaderProvider.BitmapFontShader,

                ElementCount = ebo.Count,
                PrimitiveType = PrimitiveType.Triangles,

                VariableManager = new ShaderVariableManager()
                {
                    UniformTransform = GLHelper.CreateTransformMatrix(eyeTransform, Transform),
                    UniformColor = Color,
                    UniformTexture = fbo.ColorBuffer.TextureHandle,
                }
            });
        }

        public void Dispose()
        {
            if (vbo != null)
                vbo.Dispose();
            if (ebo != null)
                ebo.Dispose();
            if (fbo != null)
                fbo.Dispose();
        }
    }
}
