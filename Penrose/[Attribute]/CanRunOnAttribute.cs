﻿using System;
using System.Diagnostics;

namespace Penrose
{
    public enum ThreadType
    {
        MainThread = 1,
        SubThread = 2, 

        All = 3,
    }

    [Conditional("DEBUG")]
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor, AllowMultiple = false, Inherited = false)]
    public sealed class CanRunOnAttribute : Attribute
    {
        public ThreadType ThreadType;
        public CanRunOnAttribute(ThreadType threadType)
        {
            ThreadType = threadType;
        }
    }
}
