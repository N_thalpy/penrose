﻿using Penrose.GameObject;
using Penrose.GameObject.Ship;
using Penrose.UI.Minimap;
using System;

namespace Penrose.HUD
{
    public abstract class HUDBase : IDisposable
    {
        public abstract void ClearMinimapObject();
        public abstract void EnqueueMinimapObject(ShipBase ship, MinimapObjectType type);

        public abstract void Update(float deltaTime);
        public abstract void Render(Transform eyeTransform);

        public abstract void Dispose();
    }
}
