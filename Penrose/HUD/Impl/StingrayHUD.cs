﻿using OpenTK;
using OpenTK.Graphics;
using Penrose.GameObject;
using Penrose.GameObject.Ship;
using Penrose.GameObject.Ship.Impl;
using Penrose.Rendering;
using Penrose.UI;
using Penrose.UI.Minimap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Penrose.HUD.Impl
{
    public sealed class StingrayHUD : HUDBase
    {
        ShipBase Owner;

        BitmapLabel center;
        BitmapLabel velDisplay;
        BitmapLabel fpsDisplay;
        Sprite frontHUD;

        BitmapLabel diagDisplay;
        Sprite rightHUD;

        SimpleMinimap minimap;
        Sprite minimapSprite;
        Sprite leftHUD;

        Texture hudTexture;

        List<float> timeTracker;
        int skipped;

        StringBuilder sb;

        public StingrayHUD(ShipBase owner)
        {
            Owner = owner;
            sb = new StringBuilder();

            hudTexture = Texture.CreateFromBitmapPath("Resource/HUD/TempHUDFrame.png");

            sb.Clear();
            sb.Append("    |    \n");
            sb.Append("==< + >==\n");
            sb.Append("    |    ");

            center = new BitmapLabel(BitmapFontProvider.Consolas, 1000, 1000)
            {
                Color = Color4.Black,
                Text = sb.ToString()
            };
            velDisplay = new BitmapLabel(BitmapFontProvider.Consolas, 1000, 1000)
            {
                Color = Color4.Green,
                Text = String.Empty,
                Docking = BitmapLabel.Dock.Left
            };
            fpsDisplay = new BitmapLabel(BitmapFontProvider.Consolas, 1000, 1000)
            {
                Color = Color4.Green,
                Text = String.Empty,
                Docking = BitmapLabel.Dock.Right,
                Align = BitmapLabel.TextAlign.Left
            };
            frontHUD = new Sprite(hudTexture, false);

            diagDisplay = new BitmapLabel(BitmapFontProvider.Consolas, 1000, 1000)
            {
                Color = Color4.Green,
                Text = String.Empty,
                Docking = BitmapLabel.Dock.Left | BitmapLabel.Dock.Top
            };
            rightHUD = new Sprite(hudTexture, false);

            minimap = new SimpleMinimap(hudTexture.Width, hudTexture.Height, owner);
            minimapSprite = new Sprite(minimap.FrameBuffer.ColorBuffer, false);
            leftHUD = new Sprite(hudTexture, false);

            timeTracker = new List<float>();
        }

        public override void ClearMinimapObject()
        {
            minimap.Clear();
        }
        public override void EnqueueMinimapObject(ShipBase ship, MinimapObjectType type)
        {
            minimap.Enqueue(ship, type);
        }
        public override void Update(float deltaTime)
        {
            if (deltaTime != 0)
                timeTracker.Add(deltaTime);

            if (GameSystem.IsVR == true && 1 / deltaTime < 60)
                skipped++;
            while (timeTracker.Sum() > 1)
                timeTracker.RemoveAt(0);

            float v = Owner.CurrentWorldVelocity.Length;
            velDisplay.Text = String.Format("VEL: {0:000.00} m/s", v);

            if (timeTracker.Count != 0)
                fpsDisplay.Text = String.Format("FPS: AVG {0:000} {1:000} ~ {2:000}\n{3} SKIPPED",
                    1 / timeTracker.Average(),
                    1 / timeTracker.Max(),
                    1 / timeTracker.Min(),
                    skipped);

            sb.Clear();
            sb.AppendLine(String.Format("Input"));
            sb.AppendLine(String.Format(" Forw   : {0}", InputHelper.GetForwardBackward()));
            sb.AppendLine(String.Format(" Left   : {0}", InputHelper.GetLeftRight()));
            sb.AppendLine(String.Format(" Twist  : {0}", InputHelper.GetTwist()));
            sb.AppendLine(String.Format(" Accel. : {0}", InputHelper.IsAccelerationPushed()));
            sb.AppendLine(String.Format(" Break  : {0}", InputHelper.IsBreakPushed()));
            sb.AppendLine(String.Format(" Prim   : {0}", InputHelper.IsPrimaryFired()));
            sb.AppendLine(String.Format(" Scndry : {0}", InputHelper.IsSecondaryFired()));

            diagDisplay.Text = sb.ToString();
        }
        public override void Render(Transform eyeTransform)
        {
            minimap.Flush();

            frontHUD.Transform.Position =
                Owner.Transform.Position +
                Owner.Transform.Orientation * (-Vector3.UnitZ * 1000);
            frontHUD.Transform.Orientation = Owner.Transform.Orientation;
            frontHUD.Transform.Scale = Vector3.One * 0.6f;

            center.Transform.Position = frontHUD.Transform.Position;
            center.Transform.Orientation = frontHUD.Transform.Orientation;

            velDisplay.Transform.Position = frontHUD.Transform.Position + frontHUD.Transform.Orientation * new Vector3(-5, 3, 0) * 80;
            velDisplay.Transform.Orientation = frontHUD.Transform.Orientation;

            fpsDisplay.Transform.Position = frontHUD.Transform.Position + frontHUD.Transform.Orientation * new Vector3(5, 3, 0) * 80;
            fpsDisplay.Transform.Orientation = frontHUD.Transform.Orientation;

            rightHUD.Transform.Position =
                Owner.Transform.Position +
                Owner.Transform.Orientation * Quaternion.FromEulerAngles(0, -MathHelper.PiOver4, 0) * new Vector3(300, 0, -1100);
            rightHUD.Transform.Orientation = Owner.Transform.Orientation * Quaternion.FromEulerAngles(0, -MathHelper.PiOver4, 0);
            rightHUD.Transform.Scale = Vector3.One * 0.6f;

            diagDisplay.Transform.Position = rightHUD.Transform.Position + rightHUD.Transform.Orientation * new Vector3(-5, 3, 0) * 80;
            diagDisplay.Transform.Orientation = rightHUD.Transform.Orientation;

            leftHUD.Transform.Position =
                Owner.Transform.Position +
                Owner.Transform.Orientation * Quaternion.FromEulerAngles(0, MathHelper.PiOver4, 0) * new Vector3(-300, 0, -1100);
            leftHUD.Transform.Orientation = Owner.Transform.Orientation * Quaternion.FromEulerAngles(0, MathHelper.PiOver4, 0);
            leftHUD.Transform.Scale = Vector3.One * 0.6f;

            minimapSprite.Transform.Position =
                frontHUD.Transform.Position +
                frontHUD.Transform.Orientation * new Vector3(-hudTexture.Width * 0.33f, -hudTexture.Height * 0.33f, 0);
            minimapSprite.Transform.Orientation = frontHUD.Transform.Orientation;
            minimapSprite.Transform.Scale = Vector3.One * 0.6f / 4;

            center.Render(eyeTransform);
            velDisplay.Render(eyeTransform);
            fpsDisplay.Render(eyeTransform);

            diagDisplay.Render(eyeTransform);

            minimapSprite.Render(eyeTransform);

            frontHUD.Render(eyeTransform);
            rightHUD.Render(eyeTransform);
            leftHUD.Render(eyeTransform);
        }

        public override void Dispose()
        {
            hudTexture.Dispose();

            center.Dispose();
            velDisplay.Dispose();
            fpsDisplay.Dispose();
            frontHUD.Dispose();

            diagDisplay.Dispose();
            rightHUD.Dispose();

            minimap.Dispose();
            minimapSprite.Dispose();
            leftHUD.Dispose();
        }
    }
}
