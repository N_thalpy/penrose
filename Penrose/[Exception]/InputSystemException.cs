﻿using System;

namespace Penrose
{
    public enum InputSystemExceptionType
    {
        RequireMouseOrJoystick,
        RequireKeyboardOrJoystick,
    }
    public sealed class InputSystemException : Exception
    {
        private InputSystemExceptionType type;

        public InputSystemException(InputSystemExceptionType type)
            : base()
        {
            this.type = type;
        }

        public override string ToString()
        {
            switch (type)
            {
                case InputSystemExceptionType.RequireKeyboardOrJoystick:
                    return "Program reqires either Keyboard or Joystick";

                case InputSystemExceptionType.RequireMouseOrJoystick:
                    return "Program requires either Mouse or Joystick";

                default:
                    throw new ArgumentException();
            }
        }
    }
}
