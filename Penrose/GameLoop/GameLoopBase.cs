﻿using Penrose.SystemComponent;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Penrose.GameLoop
{
    public abstract class GameLoopBase : IDisposable
    {
        public bool IsInitialized
        {
            get;
            private set;
        }

        private Queue<ThreadJob> RenderJobQueue;
        private Queue<ThreadJob> UpdateJobQueue;

        protected abstract void OnInitialize();
        protected abstract void OnRender(float deltaTime);
        protected abstract void OnUpdate(float deltaTime);

        protected GameLoopBase()
        {
            IsInitialized = false;
            RenderJobQueue = new Queue<ThreadJob>();
            UpdateJobQueue = new Queue<ThreadJob>();
        }

        public void Initialize()
        {
            if (IsInitialized == false)
            {
                IsInitialized = true;                
                OnInitialize();
            }
        }
        public void Render(float deltaTime)
        {
            RenderingDebugger.VertexPerFrame = 0;
            RenderingDebugger.EdgePerFrame = 0;

            TimeHelper.Tic("MainLoop.InnerRender");
            while (RenderJobQueue.Count != 0)
            {
                ThreadJob job = RenderJobQueue.Dequeue();

                Debug.Assert(job.Executed == false);
                job.Action.Invoke();
                job.Executed = true;
            }

            OnRender(deltaTime);
            TimeHelper.Toc("MainLoop.InnerRender");
        }
        public void Update(float deltaTime)
        {
            while (UpdateJobQueue.Count != 0)
            {
                ThreadJob job = UpdateJobQueue.Dequeue();
                Debug.Assert(job.Executed == false);
                job.Action.Invoke();
                job.Executed = true;
            }

            OnUpdate(deltaTime);
        }

        public void ReserveUpdate(Action action, String debugName)
        {
            throw new NotImplementedException();
        }
        public void ReserveRender(Action action, String debugName)
        {
            if (Thread.CurrentThread == ThreadHelper.MainThread)
                action.Invoke();
            else
            {
                ThreadJob job = new ThreadJob()
                {
                    Action = action,
                    DebugName = debugName,
                };

                RenderJobQueue.Enqueue(job);
                while (job.Executed == false)
                    Thread.Yield();
            }
        }

        public abstract void Dispose();
    }
}
