﻿using System;

namespace Penrose.GameLoop
{
    public sealed class ThreadJob
    {
        public bool Executed;
        public Action Action;
        public String DebugName;

        public override string ToString()
        {
            return DebugName;
        }
    }
}
