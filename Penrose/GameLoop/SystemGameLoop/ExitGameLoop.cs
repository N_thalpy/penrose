﻿using System;

namespace Penrose.GameLoop.SystemGameLoop
{
    public sealed class ExitGameLoop : GameLoopBase
    {
        protected override void OnInitialize()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            
            GameSystem.Instance.Exit();
        }

        protected override void OnRender(float deltaTime)
        {
        }
        protected override void OnUpdate(float deltaTime)
        {
        }

        public override void Dispose()
        {
        }
    }
}
