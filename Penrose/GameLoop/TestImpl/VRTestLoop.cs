﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Penrose.AI.Impl;
using Penrose.Field.Impl;
using Penrose.GameObject.Projectile.Renderer;
using Penrose.GameObject.Ship;
using Penrose.GameObject.Ship.Impl;
using Penrose.HUD;
using Penrose.HUD.Impl;
using Penrose.Physics;
using Penrose.Rendering.Effect;
using Penrose.Rendering.Renderer;
using Penrose.Rendering.VertexType;
using Penrose.UI.Minimap;
using System;
using System.Collections.Generic;

namespace Penrose.GameLoop.TestImpl
{
    public sealed class VRTestLoop : GameLoopBase
    {
        Random rd;

        ShipBase player;
        SaturnField sf;
        HUDBase hud;

        List<ShipBase> enemyList;

        protected override void OnInitialize()
        {
            rd = new Random(1024728);
            PhysicsManager.Instance.Clear();

            player = new Stingray(null);
            //(1086948, -868145.6, 68032.9) / V: (-0.6475965, -0.1816233, -0.4504104), W: -0.5871646
            player.Transform.Position = new Vector3(1086948f, -868145.6f, 68032.9f) * 2;
            player.Transform.Orientation = new Quaternion(-0.6475965f, -0.1816233f, -0.4504104f, -0.5871646f);

            sf = new SaturnField();
            sf.Initialize(false);

            hud = new StingrayHUD(player as Stingray);

            enemyList = new List<ShipBase>();
            for (int idx = 0; idx < 16; idx++)
            {
                Stingray stingray = new Stingray(new SimpleStingrayAI(player));
                stingray.Transform.Position = 10000000f * (new Vector3(rd.NextFloat(), rd.NextFloat(), rd.NextFloat()) - 0.5f * Vector3.One).Normalized();

                enemyList.Add(stingray);
            }

            GLHelper.CheckGLError();
        }

        protected override void OnRender(float deltaTime)
        {
            ParticleRenderer<VertexPositionColor>.Instance.Clear();
            ColliderRenderer.Instance.Clear();
            TrailEffectRenderer.Instance.Clear();
            TurretProjectileRenderer.Instance.Clear();
            MissileProjectileRenderer.Instance.Clear();
            GenericObjectFlatRenderer.Instance.Clear();

            GLHelper.PushCap(EnableCap.DepthTest, true);
            sf.Render(player.Transform);
            player.Render(player.Transform);

            foreach (ShipBase s in enemyList)
                s.Render(player.Transform);
            GLHelper.PopCap(EnableCap.DepthTest);

            GLHelper.PushCap(EnableCap.DepthTest, false);
            hud.Render(player.Transform);
            GLHelper.PopCap(EnableCap.DepthTest);

            ParticleRenderer<VertexPositionColor>.Instance.Flush(player.Transform);
            ColliderRenderer.Instance.Flush(player.Transform);
            TrailEffectRenderer.Instance.Flush(player.Transform);
            TurretProjectileRenderer.Instance.Flush(player.Transform);
            MissileProjectileRenderer.Instance.Flush(player.Transform);
            GenericObjectFlatRenderer.Instance.Flush(player.Transform);
        }

        protected override void OnUpdate(float deltaTime)
        {
            hud.ClearMinimapObject();

            sf.Update(deltaTime);
            player.Update(deltaTime);
            hud.Update(deltaTime);

            hud.EnqueueMinimapObject(player, MinimapObjectType.Player);
            foreach (ShipBase s in enemyList)
            {
                s.Update(deltaTime);
                if (s.CanDispose == true)
                    s.Dispose();

                if (s.Alive == true)
                    hud.EnqueueMinimapObject(s, MinimapObjectType.Enemy);
            }
            enemyList.RemoveAll(_ => _.CanDispose == true);

            PhysicsManager.Instance.Fire();
        }

        public override void Dispose()
        {
            player.Dispose();
            sf.Dispose();
            hud.Dispose();
            foreach (ShipBase s in enemyList)
                s.Dispose();
        }
    }
}
