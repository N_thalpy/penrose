﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using Penrose.GameObject;
using Penrose.Rendering;
using Penrose.Rendering.Shader;
using Penrose.Rendering.Shader.ShaderVar;
using Penrose.Rendering.VertexType;
using System;
using System.Runtime.InteropServices;

namespace Penrose.GameLoop.TestImpl
{
    public sealed class DepthPeelingTest : GameLoopBase
    {
        const int frameBufferCount = 20;

        const int opaqueCubeCount = 100000;
        const int transparentCubeCount = 100000;

        FrameBuffer opaqueFrameBuffer;
        FrameBuffer[] frameBuffers;

        ShaderBase colorShader;
        ShaderBase textureShader;
        ShaderBase primaryPeelingShader;
        ShaderBase secondaryPeelingShader;

        WavefrontObject model;

        VertexBuffer transparentCubeVertexBuffer;
        VertexBuffer opaqueCubeVertexBuffer;
        VertexBuffer frameVertexBuffer;

        ElementBuffer transparentCubeElementBuffer;
        ElementBuffer opaqueCubeElementBuffer;
        ElementBuffer frameElementBuffer;

        Transform eyeTransform;
        float rotX;
        float rotY;

        MouseState prevMouse;

        protected override unsafe void OnInitialize()
        {
            opaqueFrameBuffer = FrameBuffer.Create();
            frameBuffers = new FrameBuffer[frameBufferCount];
            for (int idx = 0; idx < frameBufferCount; idx++)
                frameBuffers[idx] = FrameBuffer.Create();

            model = WavefrontObjectProvider.Cube;
            colorShader = ShaderProvider.ColorShader;
            textureShader = ShaderProvider.TexturedShader;

            primaryPeelingShader = new ColorPrimaryPeelingShader();
            secondaryPeelingShader = new ColorSecondaryPeelingShader();

            transparentCubeVertexBuffer = VertexBuffer.Create<VertexPositionColor>(transparentCubeCount * model.Vertices.Length);
            transparentCubeElementBuffer = ElementBuffer.Create(transparentCubeCount * model.MeshElement[0].Length);

            opaqueCubeVertexBuffer = VertexBuffer.Create<VertexPositionColor>(opaqueCubeCount * model.Vertices.Length);
            opaqueCubeElementBuffer = ElementBuffer.Create(opaqueCubeCount * model.MeshElement[0].Length);

            eyeTransform = new Transform();
            eyeTransform.Position = new Vector3(0, 0, 700);
            rotX = 0;
            rotY = 0;

            prevMouse = Mouse.GetState();

            Random rd = new Random(3141592);

            VertexPositionColor* vpcTransparent = (VertexPositionColor*)Marshal.AllocHGlobal(transparentCubeCount * model.Vertices.Length * Marshal.SizeOf<VertexPositionColor>()).ToPointer();
            Int32* indTransparent = (Int32*)Marshal.AllocHGlobal(transparentCubeCount * model.MeshElement[0].Length * Marshal.SizeOf<Int32>()).ToPointer();

            for (int idx = 0; idx < transparentCubeCount; idx++)
            {
                Transform tf = new Transform();
                tf.Position = new Vector3(rd.NextFloat() - 0.5f, rd.NextFloat() - 0.5f, rd.NextFloat() - 0.5f) * 300;
                tf.Scale = Vector3.One * (50 + 15 * (rd.NextFloat() - 0.5f));

                Color4 c = new Color4(rd.NextFloat(), rd.NextFloat(), rd.NextFloat(), 0.3f);

                for (int v = 0; v < model.Vertices.Length; v++)
                {
                    vpcTransparent[idx * model.Vertices.Length + v].Position = Vector3.TransformPerspective(model.Vertices[v].Position, tf.TransformMatrix);
                    vpcTransparent[idx * model.Vertices.Length + v].Color = c;
                }
                for (int e = 0; e < model.MeshElement[0].Length; e++)
                    indTransparent[idx * model.MeshElement[0].Length + e] = model.MeshElement[0][e] + idx * model.Vertices.Length;
            }

            transparentCubeVertexBuffer.Upload<VertexPositionColor>(vpcTransparent, 0, transparentCubeCount * model.Vertices.Length);
            transparentCubeVertexBuffer.Seal();

            transparentCubeElementBuffer.Upload(indTransparent, 0, transparentCubeCount * model.MeshElement[0].Length);
            transparentCubeElementBuffer.Seal();

            VertexPositionColor* vpcOpaque = (VertexPositionColor*)Marshal.AllocHGlobal(opaqueCubeCount * model.Vertices.Length * Marshal.SizeOf<VertexPositionColor>()).ToPointer();
            Int32* indOpaque = (Int32*)Marshal.AllocHGlobal(opaqueCubeCount * model.MeshElement[0].Length * Marshal.SizeOf<Int32>()).ToPointer();

            for (int idx = 0; idx < opaqueCubeCount; idx++)
            {
                Transform tf = new Transform();
                tf.Position = new Vector3(rd.NextFloat() - 0.5f, rd.NextFloat() - 0.5f, rd.NextFloat() - 0.5f) * 300;
                tf.Scale = Vector3.One * (50 + 15 * (rd.NextFloat() - 0.5f));

                for (int v = 0; v < model.Vertices.Length; v++)
                {
                    Color4 c = new Color4(rd.NextFloat(), rd.NextFloat(), rd.NextFloat(), 1);
                    vpcOpaque[idx * model.Vertices.Length + v].Position = Vector3.TransformPerspective(model.Vertices[v].Position, tf.TransformMatrix);
                    vpcOpaque[idx * model.Vertices.Length + v].Color = c;
                }
                for (int e = 0; e < model.MeshElement[0].Length; e++)
                    indOpaque[idx * model.MeshElement[0].Length + e] = model.MeshElement[0][e] + idx * model.Vertices.Length;
            }

            opaqueCubeVertexBuffer.Upload<VertexPositionColor>(vpcOpaque, 0, opaqueCubeCount * model.Vertices.Length);
            opaqueCubeVertexBuffer.Seal();

            opaqueCubeElementBuffer.Upload(indOpaque, 0, opaqueCubeCount * model.MeshElement[0].Length);
            opaqueCubeElementBuffer.Seal();

            frameVertexBuffer = VertexBuffer.Create(new VertexPositionTextureColor[4]
            {
                new VertexPositionTextureColor(-1, 1, 0, 0, 0),
                new VertexPositionTextureColor(-1, -1, 0, 0, 1),
                new VertexPositionTextureColor(1, -1, 0, 1, 1),
                new VertexPositionTextureColor(1, 1, 0, 1, 0),
            });
            frameElementBuffer = ElementBuffer.Create(new int[4]
            {
                0, 1, 2, 3,
            });

            Marshal.FreeHGlobal(new IntPtr(vpcOpaque));
            Marshal.FreeHGlobal(new IntPtr(vpcTransparent));
            Marshal.FreeHGlobal(new IntPtr(indOpaque));
            Marshal.FreeHGlobal(new IntPtr(indTransparent));
        }

        protected override void OnUpdate(float deltaTime)
        {
            Console.WriteLine(1 / deltaTime);

            MouseState currMouse = Mouse.GetState();
            if (currMouse.IsButtonDown(MouseButton.Left))
            {
                rotX += (currMouse.X - prevMouse.X) * 0.01f;
                rotY -= (currMouse.Y - prevMouse.Y) * 0.01f;

                if (rotY > MathHelper.PiOver2)
                    rotY = MathHelper.PiOver2;
                if (rotY < -MathHelper.PiOver2)
                    rotY = -MathHelper.PiOver2;
            }
            prevMouse = currMouse;

            eyeTransform.Orientation = Quaternion.FromEulerAngles(0, -rotX, rotY);
            eyeTransform.Position = eyeTransform.Orientation * new Vector3(0, 0, 700);
        }
        protected override void OnRender(float deltaTime)
        {
            RenderQueue.Instance.Enqueue(new RenderQueue.OpaqueRenderJob()
            {
                VertexBuffer = opaqueCubeVertexBuffer,
                ElementBuffer = opaqueCubeElementBuffer,
                Shader = colorShader,

                ElementCount = opaqueCubeElementBuffer.Count,
                PrimitiveType = PrimitiveType.Triangles,

                VariableManager = new ShaderVariableManager()
                {
                    UniformTransform = GLHelper.CreateTransformMatrix(eyeTransform),
                }
            });
            RenderQueue.Instance.Enqueue(new RenderQueue.TransparentRenderJob()
            {
                VertexBuffer = transparentCubeVertexBuffer,
                ElementBuffer = transparentCubeElementBuffer,
                PrimaryPeelingShader = primaryPeelingShader,
                SecondaryPeelingShader = secondaryPeelingShader,

                ElementCount = transparentCubeElementBuffer.Count,
                PrimitiveType = PrimitiveType.Triangles,

                VariableManager = new ShaderVariableManager()
                {
                    UniformTransform = GLHelper.CreateTransformMatrix(eyeTransform),
                }
            });
        }

        public override void Dispose()
        {
            opaqueFrameBuffer.Dispose();
            for (int idx = 0; idx < frameBufferCount; idx++)
                frameBuffers[idx].Dispose();

            transparentCubeVertexBuffer.Dispose();
            opaqueCubeVertexBuffer.Dispose();
            frameVertexBuffer.Dispose();

            transparentCubeElementBuffer.Dispose();
            opaqueCubeElementBuffer.Dispose();
            frameElementBuffer.Dispose();

            primaryPeelingShader.Dispose();
            secondaryPeelingShader.Dispose();
        }
    }
}
