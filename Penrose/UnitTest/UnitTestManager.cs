﻿using System;
using System.Reflection;

namespace Penrose.UnitTest
{
    public static class UnitTestManager
    {
        public static void InvokeUnitTest()
        {
            Type[] typeList = typeof(UnitTestManager).Assembly.GetTypes();
            foreach (Type t in typeList)
            {
                MethodInfo[] infoList = t.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                foreach (MethodInfo m in infoList)
                    if (m.GetCustomAttribute<UnitTestAttribute>() != null)
                        m.Invoke(null, null);
            }
        }
    }
}
