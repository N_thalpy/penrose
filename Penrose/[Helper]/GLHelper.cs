﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Penrose.GameObject;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Penrose
{
    public static class GLHelper
    {
        public static Matrix4 CurrentProjection;
        public static Color4 BackgroundColor;
        public readonly static float NearPlane;
        public readonly static float FarPlane;

        public readonly static Vector2 UVLeftUpper;
        public readonly static Vector2 UVLeftLower;
        public readonly static Vector2 UVRightLower;
        public readonly static Vector2 UVRightUpper;

        private static Object glLock;
        private static Dictionary<EnableCap, Stack<bool>> capDict;

        static GLHelper()
        {
            glLock = new Object();
            capDict = new Dictionary<EnableCap, Stack<bool>>();

            UVLeftUpper = new Vector2(0, 1);
            UVLeftLower = new Vector2(0, 0);
            UVRightLower = new Vector2(1, 0);
            UVRightUpper = new Vector2(1, 1);

            BackgroundColor = new Color4(0xFF, 0xFF, 0xFF, 0xFF);
            NearPlane = 1;
            FarPlane = 100000;
        }

        [Conditional("DEBUG")]
        public static void EnterGLSection()
        {
            GLHelper.CheckGLError();
            Debug.Assert(Monitor.TryEnter(glLock) == true);
        }
        [Conditional("DEBUG")]
        public static void ExitGLSection()
        {
            Debug.Assert(Monitor.IsEntered(glLock) == true);
            Monitor.Exit(glLock);
            GLHelper.CheckGLError();
        }

        [Conditional("DEBUG")]
        public static void CheckGLError()
        {
            ErrorCode errorCode = GL.GetError();
            if (errorCode != ErrorCode.NoError)
                throw new Exception(errorCode.ToString());
        }

        [CanRunOn(ThreadType.MainThread)]
        public static void Initialize()
        {
            ThreadHelper.CheckCallerThread();
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
        }

        [CanRunOn(ThreadType.MainThread)]
        public static void DrawPrimitive(PrimitiveType primitiveType, Int64 count)
        {
            ThreadHelper.CheckCallerThread();
            GL.DrawElements(primitiveType, (int)count, DrawElementsType.UnsignedInt, 0);
        }

        [CanRunOn(ThreadType.MainThread)]
        public static void PushCap(EnableCap cap, bool enable)
        {
            ThreadHelper.CheckCallerThread();

            if (capDict.ContainsKey(cap) == false)
                capDict.Add(cap, new Stack<bool>());

            capDict[cap].Push(GL.IsEnabled(cap));

            if (enable == true)
                GL.Enable(cap);
            else
                GL.Disable(cap);
        }
        [CanRunOn(ThreadType.MainThread)]
        public static void PopCap(EnableCap cap)
        {
            ThreadHelper.CheckCallerThread();

            bool enable = capDict[cap].Pop();

            if (enable == true)
                GL.Enable(cap);
            else
                GL.Disable(cap);
        }
        
        public static Matrix4 CreateTransformMatrix(Transform eyeTransform, Transform modelTransform)
        {
            return modelTransform.TransformMatrix * eyeTransform.InvertedTransformMatrix;
        }
        public static Matrix4 CreateTransformMatrix(Transform eyeTransform)
        {
            return eyeTransform.InvertedTransformMatrix;
        }
    }
}
