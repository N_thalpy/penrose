﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Penrose
{
    public static class ThreadHelper
    {
        private class LogData
        {
            public String Log;
            public ConsoleColor Foreground;
            public ConsoleColor Background;

            public LogData()
            {
                Console.ResetColor();
                Foreground = Console.ForegroundColor;
                Background = Console.BackgroundColor;
            }
        }
        private class SubThreadJob
        {
            public Action Action;
            public String DebugName;
        }

        public static Thread MainThread;
        public static Thread SubThread;

        public static bool DebugThread;

        [Flags]
        private enum Affinity
        {
            Core1 = 1 << 0,
            Core2 = 1 << 1,
            Core3 = 1 << 2,
            Core4 = 1 << 3,
        };

        private static int MainThreadAffinity;
        private static int SubThreadAffinity;

        private static List<LogData> logDataList;

        private static Queue<SubThreadJob> subThreadRenderJob;

        static ThreadHelper()
        {
            DebugThread = false;

            subThreadRenderJob = new Queue<SubThreadJob>();
            logDataList = new List<LogData>();

            MainThreadAffinity = (int)(Affinity.Core1 | Affinity.Core2 | Affinity.Core3);
            SubThreadAffinity = (int)(Affinity.Core4);
        }

        private static void SubThreadEntryPoint()
        {
            try
            {
#pragma warning disable CS0618
                Process.GetCurrentProcess().Threads.Cast<ProcessThread>().Single(_ => _.Id == AppDomain.GetCurrentThreadId()).ProcessorAffinity = new IntPtr(SubThreadAffinity);
#pragma warning restore CS0618

                while (true)
                {
                    while (Monitor.TryEnter(subThreadRenderJob) == false)
                        Thread.Yield();

                    if (subThreadRenderJob.Count == 0)
                    {
                        Monitor.Exit(subThreadRenderJob);
                        SubThread.Suspend();
                        continue;
                    }
                    else
                    {
                        SubThreadJob job = subThreadRenderJob.Dequeue();
                        Monitor.Exit(subThreadRenderJob);

                        job.Action.Invoke();
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void SetOffThreadJob(Action action, String debugName)
        {
            lock (subThreadRenderJob)
                subThreadRenderJob.Enqueue(new SubThreadJob()
                {
                    Action = action,
                    DebugName = debugName
                });

#warning Thread.Resume and Thread.Suspend is very dangerous; need another blocking methods
            SubThread.Resume();
        }

        public static void Initialize()
        {
            MainThread = Thread.CurrentThread;
            SubThread = new Thread(SubThreadEntryPoint);

#pragma warning disable CS0618
            Process.GetCurrentProcess().Threads.Cast<ProcessThread>().Single(_ => _.Id == AppDomain.GetCurrentThreadId()).ProcessorAffinity = new IntPtr(MainThreadAffinity);
#pragma warning restore CS0618

            MainThread.Priority = ThreadPriority.Highest;
            SubThread.Priority = ThreadPriority.Highest;

            SubThread.Start();
        }

        [Conditional("DEBUG")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void CheckCallerThread()
        {
            if (DebugThread == false)
                return;

            MethodBase current = new StackTrace(1, false).GetFrame(0).GetMethod();
            CanRunOnAttribute attr = current.GetCustomAttribute<CanRunOnAttribute>();

            Debug.Assert(attr != null);
            switch (attr.ThreadType)
            {
                case ThreadType.MainThread:
                    Debug.Assert(Thread.CurrentThread == ThreadHelper.MainThread);
                    return;

                case ThreadType.SubThread:
                    Debug.Assert(Thread.CurrentThread == ThreadHelper.SubThread);
                    return;

                case ThreadType.All:
                    return;

                default:
                    throw new ArgumentException();
            }
        }
    }
}
