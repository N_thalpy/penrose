﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Penrose
{
    public static class TimeHelper
    {
        private static Dictionary<String, Stopwatch> swDict;
        private static StreamWriter csv;

        private static int frame;
        private static bool csvInitialized;

        static TimeHelper()
        {
            swDict = new Dictionary<String, Stopwatch>();
            frame = 0;

            csv = new StreamWriter(String.Format("Profiled-{0}.csv", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss")));
            csvInitialized = false;

            GameSystem.InvokeOnExit += () =>
            {
                csv.Close();
                csv.Dispose();
            };
        }

        public static void Tic(String name)
        {
            if (swDict.ContainsKey(name) == false)
                swDict.Add(name, new Stopwatch());

            swDict[name].Restart();
        }
        public static void Toc(String name)
        {
            swDict[name].Stop();
        }

        public static void Flush()
        {
            if (csvInitialized == false)
            {
                csv.Write(",GC 0,GC 1,GC 2,");
                csv.WriteLine(String.Join(",", swDict.Keys));
                csvInitialized = true;
            }

            if (csv.BaseStream != null)
            {
                csv.Write("Frame,{0},{1},{2},", GC.CollectionCount(0), GC.CollectionCount(1), GC.CollectionCount(2));
                csv.WriteLine(String.Join(",", swDict.Values.Select(_ => 1000f * _.ElapsedTicks / Stopwatch.Frequency)));
            }

            frame++;
        }
    }
}
