﻿using OpenTK.Input;
using System;

namespace Penrose
{
    public static class InputHelper
    {
        private const JoystickAxis LeftRight = JoystickAxis.Axis0;
        private const JoystickAxis ForwardBackward = JoystickAxis.Axis1;
        private const JoystickAxis Throttle = JoystickAxis.Axis2;
        private const JoystickAxis Twist = JoystickAxis.Axis3;

        private const int JoystickDepth = 32;

        private static int prevMouseX;
        private static int prevMouseY;

        static InputHelper()
        {
            MouseState mState = GetMouseState();
            if (mState.IsConnected == true)
            {
                prevMouseX = mState.X;
                prevMouseY = mState.Y;
            }
        }

        private static JoystickState GetJoystickState()
        {
            return Joystick.GetState(0);
        }
        private static KeyboardState GetKeyboardState()
        {
            return Keyboard.GetState();
        }
        private static MouseState GetMouseState()
        {
            return Mouse.GetState();
        }

        public static bool IsAccelerationPushed()
        {
            KeyboardState kState = GetKeyboardState();
            if (kState.IsConnected == false)
                throw new InputSystemException(InputSystemExceptionType.RequireKeyboardOrJoystick);

            return kState.IsKeyDown(Key.W);
        }
        public static bool IsBreakPushed()
        {
            KeyboardState kState = GetKeyboardState();
            if (kState.IsConnected == false)
                throw new InputSystemException(InputSystemExceptionType.RequireKeyboardOrJoystick);

            return kState.IsKeyDown(Key.S);
        }

        public static bool IsPrimaryFired()
        {
            JoystickState jState = GetJoystickState();
            if (jState.IsConnected == false)
            {
                MouseState mState = GetMouseState();
                if (mState.IsConnected == false)
                    throw new InputSystemException(InputSystemExceptionType.RequireMouseOrJoystick);

                return mState.IsButtonDown(MouseButton.Left);
            }
            else
            {
                return jState.IsButtonDown(JoystickButton.Button0);
            }
        }

        public static bool IsSecondaryFired()
        {
            JoystickState jState = GetJoystickState();
            if (jState.IsConnected == false)
            {
                MouseState mState = GetMouseState();
                if (mState.IsConnected == false)
                    throw new InputSystemException(InputSystemExceptionType.RequireMouseOrJoystick);

                return mState.IsButtonDown(MouseButton.Right);
            }
            else
            {
                return jState.IsButtonDown(JoystickButton.Button1);
            }
        }

        public static float GetLeftRight()
        {
            JoystickState jState = GetJoystickState();
            MouseState mState = GetMouseState();

            if (jState.IsConnected == true)
                return (float)1 / JoystickDepth * (int)(JoystickDepth * -jState.GetAxis(InputHelper.LeftRight));
            if (mState.IsConnected == true)
                return 0;

            throw new InputSystemException(InputSystemExceptionType.RequireMouseOrJoystick);
        }
        public static float GetForwardBackward()
        {
            JoystickState jState = GetJoystickState();
            MouseState mState = GetMouseState();

            if (jState.IsConnected == true)
                return (float)1 / JoystickDepth * (int)(JoystickDepth * jState.GetAxis(InputHelper.ForwardBackward));
            if (mState.IsConnected == true)
                return -(mState.Y - prevMouseY);

            throw new InputSystemException(InputSystemExceptionType.RequireMouseOrJoystick);
        }
        public static float GetTwist()
        {
            JoystickState jState = GetJoystickState();
            MouseState mState = GetMouseState();

            if (jState.IsConnected == true)
                return (float)1 / JoystickDepth * (int)(JoystickDepth * -jState.GetAxis(InputHelper.Twist));
            if (mState.IsConnected == true)
                return -(mState.X - prevMouseX);

            throw new InputSystemException(InputSystemExceptionType.RequireMouseOrJoystick);
        }

        public static void Update()
        {
            MouseState mState = GetMouseState();
            if (mState.IsConnected == true)
            {
                prevMouseX = mState.X;
                prevMouseY = mState.Y;
            }
        }

        [Obsolete("DO NOT USE GetThrottle", false)]
        public static float GetThrottle()
        {
            float rv = GetJoystickState().GetAxis(InputHelper.Throttle);

            if (Math.Abs(rv) < 1.0 / 255)
                rv = 0;
            return rv;
        }
    }
}
