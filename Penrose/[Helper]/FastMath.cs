﻿using System;

namespace Penrose
{
    public static class FastMath
    {
        public const float PI = 3.1415926535f;
        public const float PIOver2 = 1.57079632675f;

        private static int maxTableSize;

        private static float[][] sinTable;
        private static float[][] cosTable;

        static FastMath()
        {
            maxTableSize = 30;
        }
        public static void Initialize()
        {
            // sinTable[n][m] = sin(n * pi / 2m - 2pi)
            // n/m is in [-1, 1]
            sinTable = new float[maxTableSize][];
            cosTable = new float[maxTableSize][];

            for (int n = 0; n < maxTableSize; n++)
            {
                sinTable[n] = new float[4 * n + 1];
                cosTable[n] = new float[4 * n + 1];
                for (int m = 0; m <= 4 * n; m++)
                {
                    sinTable[n][m] = (float)Math.Sin(Math.PI * n / m - Math.PI * 2);
                    cosTable[n][m] = (float)Math.Cos(Math.PI * n / m - Math.PI * 2);
                }
            }
        }

        public static void GetNPiOverM(int n, int m, out float sin, out float cos)
        {
            sin = sinTable[n + m][m];
            cos = cosTable[n + m][m];
        }
    }
}
